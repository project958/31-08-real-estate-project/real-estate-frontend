# Dự án

Real Estate v1.5

[Home page](https://real-estate-app-front-end.herokuapp.com/customer/)

[Admin page](https://real-estate-app-front-end.herokuapp.com/admin/)

[Demo api](https://real-estate-app-devcamp.herokuapp.com/projectsAll)

* Admin account: admin/1234

* Customer account: linhtn/1234

## Mô tả

Hệ thống API cho phép thao tác quản lý với dữ liệu.

Giao diện cho khách hàng truy cập và sử dụng.

Hệ thống adminLTE cho phép thao tác quản lý với dữ liệu.

## Công nghệ

* [Bootstrap](bootstrap.com)
* [JQuery](jquery.com)
* [Java](java.com)
* [Spring Boot](spring.io)
* [MySQL](mysql.com)

## Tác giả

linhtn

## Version

- 1.5
  - Initial Release

<p align="right">(<a href="#readme-top">back to top</a>)</p>

### Chi tiết

* [Slide](https://docs.google.com/presentation/d/1pyrtsqKEmlnEKQTpzqTZ5pK0Ma0YUQiO0giZinn6sMk/edit#slide=id.p)
