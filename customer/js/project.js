$(document).ready(function () {
  /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
  const gAPI_URL = 'http://localhost:8080/'
  
  let gProject = []
  let gProjectFound = []
  // get param url
  let gUrlString = new URL(window.location.href)
  let gProvinceId = gUrlString.searchParams.get('provinceid')
  let gProjectId = gUrlString.searchParams.get('projectid')

  /*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
  onPageLoading()
  // gán sự kiện menu khi cuộn trang
  $(window).scroll(navbarTranform)
  // gán sự kiện tìm khi nhập
  $(document).on('keyup', '#searh-input', onInputChangeSearch)
  // gán sự kiện chọn sắp xếp
  $(document).on('change', '#sort-estate', onSelectSortChange)

  /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */

  function onPageLoading() {
    navbarTranform()
    getAllProject()
  }

  /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/

  // xử lý sự kiện menu khi cuộn trang
  function navbarTranform() {
    if ($(document).scrollTop() > 30) {
      $('#navbar-main').addClass('transform')
      $('#navbar-main').addClass('navbar-dark bg-dark')
      $('#navbar-main').removeClass('navbar-light  bg-light')
    } else {
      $('#navbar-main').removeClass('transform')
      $('#navbar-main').removeClass('navbar-dark bg-dark')
      $('#navbar-main').addClass('navbar-light bg-light')
    }
  }

  // get all project
  function getAllProject() {
    if (gProvinceId) {
      $.ajax({
        type: 'get',
        url: gAPI_URL + 'province/' + gProvinceId,
        dataType: 'json',
        async: false,
        success: function (response) {
          // console.log(response)
          $('#content-title').html('Dự án tại thành phố: ' + response.name)
        },
      })
      $.ajax({
        type: 'get',
        url: gAPI_URL + 'projects/province/' + gProvinceId + '/page/0/limit/50/sort/id',
        dataType: 'json',
        async: false,
        success: function (response) {
          // console.log(response)
          gProject = response
          addProjectToHtml(response)
          pageEstate()
        },
      })
    } else if (gProjectId) {
      $.ajax({
        type: 'get',
        url: gAPI_URL + 'project/' + gProjectId,
        dataType: 'json',
        success: function (response) {
          // console.log(response)
          gProject = response
          $('#content-title').html('BĐS trong dự án: ' + response.name)
          $('#project-detail').append(`<h2 class="my-4 content-title">Tổng quan dự án: ${response.name}</h2>
        <div class="container-fluid p-4 bg-light">
        <div class="row px-2">
          <div class="col-lg-12 p-1 py-2"><strong>Địa chỉ: </strong>  ${response.address} </div>
          <div class="col-lg-12 p-1 py-2"><strong>Mô tả: </strong> ${response.description} </div>
          <div class="col-lg-6 p-1 py-2"><strong>Số tòa nhà:  </strong>  ${response.numBlock}</div>
          <div class="col-lg-6 p-1 py-2"><strong>Số tầng: </strong> ${response.numFloors}</div>
          <div class="col-lg-6 p-1 py-2"><strong>Số phòng: </strong> ${response.numApartment}</div>
          <div class="col-lg-6 p-1 py-2"><strong> Tổng diện tích: </strong> ${response.acreage} m2</div>          
          </div>
        </div>`)
        },
      })
      $.ajax({
        type: 'get',
        url: gAPI_URL + 'real-estate/project/' + gProjectId,
        dataType: 'json',
        success: function (response) {
          // console.log(response)
          addRealEstateToHtml(response)
          pageEstate()
        },
      })
    } else {
      $.ajax({
        type: 'get',
        url: gAPI_URL + 'projects/page/0/limit/50/sort/id',
        dataType: 'json',
        success: function (response) {
          // console.log(response)
          gProject = response
          $('#content-title').html('Tất cả dự án')
          addProjectToHtml(response)
          pageEstate()
        },
      })
    }
  }

  // add project to html
  function addProjectToHtml(paramProject) {
    $('.new-project-list').html('')

    if (paramProject) {
      for (const project of paramProject) {
        $('.new-project-list').append(`     
      <div class="col-lg-4 col-md-6 mt-4 all-project-item"> 
          <div class="card card-list mx-2">
            <a href="estatelist.html?projectid=${project.id}">
              <img
                class="card-img-top"
                src="./images/project/${project.photo ? project.photo : '5.jpg'}"
                alt="Card image cap"
              />
              <div class="card-body">
                <h5 class="card-title">${project.name}</h5>
                <h6 class="card-subtitle mb-2 text-muted">
                  <i class="mdi mdi-home-map-marker"></i> ${project.address}
                </h6>
                <h6 class="text-success mb-0 mt-3">
                  <small>Tổng diện tích: </small> ${project.acreage} m2
                </h6>
              </div>
              <div class="card-footer">
                <span>
                  <i class="mdi mdi-sofa"></i> Số tầng: <strong>${project.numFloors}</strong>
                </span>
                <span>
                  <i class="mdi mdi-scale-bathroom"></i> | Số Căn: <strong>${project.numApartment}</strong>
                </span>
                <span>
                  <i class="mdi mdi-move-resize-variant"></i> | DT căn hộ: <strong>${
                    project.apartmenttArea ? project.apartmenttArea : ' '
                  }</strong>
                </span>
              </div>
            </a>
          </div>     
          </div>   
      `)
      }
    }
  }

  // add Estate to html
  function addRealEstateToHtml(paramEstate) {
    $('.new-project-list').html('')

    if (paramEstate) {
      for (const estate of paramEstate) {
        $('.new-project-list').append(`     
        <div class="col-lg-4 col-md-6 mt-4 all-project-item"> 
        <div class="card card-list">
          <a href="estate.html?estateid=${estate.id}">
            <img
              class="card-img-top"
              src="./images/bds/${estate.photo ? estate.photo : '11.jpg'}"
              alt="Card image cap"
            />
            <div class="card-body">
              <h5 class="card-title">${estate.title ? estate.title : 'Đang cập nhật'}</h5>
              <h6 class="card-subtitle mb-2 text-muted">
                <i class="mdi mdi-home-map-marker"></i> ${estate.address}
              </h6>
              <h2 class="text-success mb-0 mt-3"><small>Giá: </small>${
                estate.price ? estate.price : ' '
              } <small>tỷ </small></h2>
                <small>Tổng diện tích: </small> ${estate.acreage ? estate.acreage : ' '} m2
              </h4>
            </div>
            <div class="card-footer">
              <span>
                <i class="mdi mdi-sofa"></i> PN: <strong>${estate.bedroom ? estate.bedroom : ' '}</strong>
              </span>
              <span>
                <i class="mdi mdi-scale-bathroom"></i> | PT: <strong>${estate.bath ? estate.bath : ' '}</strong>
              </span>
              <span>
                <i class="mdi mdi-move-resize-variant"></i> | Ban công: <strong>${
                  estate.balcony ? estate.balcony : ' '
                }</strong>
              </span>
            </div>
          </a>
        </div>     
      </div>    
      `)
      }
    }
  }

  // tìm khi nhập
  function onInputChangeSearch(e) {
    gProjectFound = filterNameEstate(this.value)
    // console.log(this.value)
    addProjectToHtml(gProjectFound)
    pageEstate()
  }

  // tìm kiếm bđs
  function filterNameEstate(paramName) {
    let result = gProject.filter(function (project) {
      // console.log(project)
      // console.log(project.address)
      return (
        removeAccents(project.name.toLowerCase()).includes(removeAccents(paramName.toLowerCase())) || paramName == ''
      )
    })
    // console.log(gProject)
    // console.log(result)
    return result
  }

  // sắp sếp khi chọn
  function onSelectSortChange(e) {
    // console.log(gProjectFound)
    if (gProjectFound.length > 0) {
      sortEstate(this.value, gProjectFound)
      addProjectToHtml(gProjectFound)
      pageEstate()
    } else {
      sortEstate(this.value, gProject)
      addProjectToHtml(gProject)
      pageEstate()
    }
  }

  // paging
  function pageEstate() {
    var items = $('.all-project-item')
    var numItems = items.length
    var perPage = 9

    items.slice(perPage).hide()

    $('#pagination-container').pagination({
      items: numItems,
      itemsOnPage: perPage,
      prevText: '&laquo;',
      nextText: '&raquo;',
      onPageClick: function (pageNumber) {
        var showFrom = perPage * (pageNumber - 1)
        var showTo = showFrom + perPage
        items.hide().slice(showFrom, showTo).show()
      },
    })
  }

  // sắp xếp
  function sortEstate(paramValue, paramObj) {
    switch (paramValue) {
      case 'moi':
        paramObj.sort(function (a, b) {
          return b.id - a.id
        })
        break
      case 'cu':
        paramObj.sort(function (a, b) {
          return a.id - b.id
        })
        break

      default:
        paramObj.sort(function (a, b) {
          return b.id - a.id
        })
        break
    }
  }

  // bỏ dâu tiếng việt
  function removeAccents(str) {
    var AccentsMap = [
      'aàảãáạăằẳẵắặâầẩẫấậ',
      'AÀẢÃÁẠĂẰẲẴẮẶÂẦẨẪẤẬ',
      'dđ',
      'DĐ',
      'eèẻẽéẹêềểễếệ',
      'EÈẺẼÉẸÊỀỂỄẾỆ',
      'iìỉĩíị',
      'IÌỈĨÍỊ',
      'oòỏõóọôồổỗốộơờởỡớợ',
      'OÒỎÕÓỌÔỒỔỖỐỘƠỜỞỠỚỢ',
      'uùủũúụưừửữứự',
      'UÙỦŨÚỤƯỪỬỮỨỰ',
      'yỳỷỹýỵ',
      'YỲỶỸÝỴ',
    ]
    for (var i = 0; i < AccentsMap.length; i++) {
      var re = new RegExp('[' + AccentsMap[i].substr(1) + ']', 'g')
      var char = AccentsMap[i][0]
      str = str.replace(re, char)
    }
    return str
  }
})
