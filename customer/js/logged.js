$(document).ready(function () {
  const gAPI_URL = 'http://localhost:8080/'

  const token = getCookie('token')
  if (token) {
    callApiGetUserIdByToken(token)
    // window.location.href = 'addressMap.html'
    // console.log(window.location.href)
    handleLogged()
  } else {
    // window.location.href = 'index.html'
  }

  function handleLogged() {
    $('.list-inline.main-nav-right.my-2').html(` <li class="list-inline-item">
    <a id="btn-logout" class="btn btn-danger btn-sm" href="">Đăng xuất</a>
  </li>`)
    $('#register-hidden-form').hide()
  }

  function callApiGetUserIdByToken(paramToken) {
    $.ajax({
      url: gAPI_URL + 'findUserId/Token',
      type: 'GET',
      dataType: 'json',
      contentType: 'application/json; charset=utf-8',
      headers: {
        Authorization: 'Token ' + paramToken,
      },
      success: function (responseObject) {
        // console.log('user id: ', responseObject)
        getUserInfo(responseObject)
      },
      error: function (pAjaxContext) {
        //redirectToLogin();
      },
    })
  }

  // get user info to redirect
  function getUserInfo(paramId) {
    $.ajax({
      type: 'get',
      url: gAPI_URL + 'user/' + paramId,
      dataType: 'json',
      async: false,
      success: function (response) {
        // console.log(response)
        let userRole = response.roles[0].roleKey
        // console.log(userRole)
        handleUserAuth(response, userRole)
      },
    })
  }

  function handleUserAuth(paramUser, paramRole) {
    $('.info .d-block').html(paramUser.username)
    switch (paramRole) {
      //   // case 'ROLE_ADMIN':
      //   //   break

      //   case 'ROLE_CUSTOMER':
      //     // window.location = "realestateList.html"
      //     $('.menu-open .nav-treeview').html(`<li class="nav-item">
      //    <a href="./realestateCList.html" class="nav-link active">
      //      <p><i class="fas fa-city"></i> Thông tin BĐS</p>
      //    </a>
      //  </li>`)
      //     break

      case 'ROLE_HOMESELLER':
        // window.location = "realestateHList.html"
        //   $('.menu-open .nav-treeview').html(`
        //   <li class="nav-item">
        //     <a href="./customerList.html" class="nav-link active">
        //       <p><i class="fas fa-city"></i> Khách hàng </p>
        //     </a>
        //   </li>
        //   <li class="nav-item">
        //     <a href="./realestateList.html" class="nav-link">
        //       <p><i class="fas fa-city"></i> Thông tin BĐS</p>
        //     </a>
        //  </li>`)
        break
    }
  }

  function getCookie(cname) {
    var name = cname + '='
    var decodedCookie = decodeURIComponent(document.cookie)
    var ca = decodedCookie.split(';')
    for (var i = 0; i < ca.length; i++) {
      var c = ca[i]
      while (c.charAt(0) == ' ') {
        c = c.substring(1)
      }
      if (c.indexOf(name) == 0) {
        return c.substring(name.length, c.length)
      }
    }
    return ''
  }
})
