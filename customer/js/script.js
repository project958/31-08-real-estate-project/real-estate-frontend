$(document).ready(function () {
  /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
  const gAPI_URL = 'http://localhost:8080/'
    
  const numberOption = {
    currencySymbol: ' VNĐ',
    currencySymbolPlacement: 's',
    decimalCharacter: ',',
    decimalPlaces: 0,
    digitGroupSeparator: '.',
  }
  // new AutoNumeric('#pricefrom', numberOption)
  // new AutoNumeric('#priceto', numberOption)

  /*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */

  onPageLoading()
  // gán sự kiện menu khi cuộn trang
  $(window).scroll(navbarTranform)

  /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */

  function onPageLoading() {
    navbarTranform()
    getNewProject()
    getNewEstate()
  }

  /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/

  // xử lý khi cuộn trang
  function navbarTranform() {
    if ($(document).scrollTop() > 30) {
      $('#navbar-main').addClass('transform')
      $('#navbar-main').addClass('navbar-dark bg-dark')
      $('#navbar-main').removeClass('navbar-light  bg-light')
    } else {
      $('#navbar-main').removeClass('transform')
      $('#navbar-main').removeClass('navbar-dark bg-dark')
      $('#navbar-main').addClass('navbar-light bg-light')
    }
  }

  // get new project
  function getNewProject() {
    $.ajax({
      type: 'get',
      url: gAPI_URL + 'projects/page/0/limit/6/sort/id',
      dataType: 'json',
      success: function (response) {
        // console.log(response)
        addProjectToHtml(response)
        addToSlideProject()
      },
    })
  }

  // add new project to slide
  function addToSlideProject() {
    $('.new-project-list').slick({
      infinite: true,
      slidesToShow: 3,
      slidesToScroll: 1,
      autoplay: true,
      autoplaySpeed: 3000,
      centerPadding: '40px',
      arrows: true,
      dots: true,
      responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 3,
            infinite: true,
            dots: true,
          },
        },
        {
          breakpoint: 800,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 1,
            arrows: false,
          },
        },
        {
          breakpoint: 700,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
          },
        },
      ],
    })
  }

  // add project to html
  function addProjectToHtml(paramProject) {
    $('.new-project-list').html('')

    for (const project of paramProject) {
      $('.new-project-list').append(`       
          <div class="card card-list mx-2">
            <a href="estatelist.html?projectid=${project.id}">
              <img
                class="card-img-top"
                src="./images/project/${project.photo ? project.photo : '5.jpg'}"
                alt="Card image cap"
              />
              <div class="card-body">
                <h5 class="card-title">${project.name}</h5>
                <h6 class="card-subtitle mb-2 text-muted">
                  <i class="mdi mdi-home-map-marker"></i> ${project.address}
                </h6>
                <h6 class="text-success mb-0 mt-3">
                  <small>Tổng diện tích: </small> ${project.acreage} m2
                </h6>
              </div>
              <div class="card-footer">
                <span>
                  <i class="mdi mdi-sofa"></i> Số tầng: <strong>${project.numFloors}</strong>
                </span>
                <span>
                  <i class="mdi mdi-scale-bathroom"></i> | Số Căn: <strong>${project.numApartment}</strong>
                </span>
                <span>
                  <i class="mdi mdi-move-resize-variant"></i> | DT căn hộ: <strong>${
                    project.apartmenttArea ? project.apartmenttArea : 'updating'
                  }</strong>
                </span>
              </div>
            </a>
          </div>     
      `)
    }
  }

  // get new estate approved
  function getNewEstate() {
    $.ajax({
      type: 'get',
      url: gAPI_URL + 'real-estate/status/1/page/0/limit/6/sort/id',
      dataType: 'json',
      success: function (response) {
        // console.log(response)
        addEstateToHtml(response)
        // gEstate = response
        // pageEstate()
      },
    })
  }

  // add Estate to html
  function addEstateToHtml(paramProject) {
    $('.all-estate').html('')
    // console.log(paramProject)

    for (const estate of paramProject) {
      $('.all-estate').append(`      
      <div class="col-lg-4 col-md-6 mt-4 all-estate-item"> 
        <div class="card card-list">
          <a href="estate.html?estateid=${estate.id}">
            <img
              class="card-img-top"
              src="./images/bds/${estate.photo ? estate.photo : '11.jpg'}"
              alt="Card image cap"
            />
            <div class="card-body">
              <h5 class="card-title">${estate.title ? estate.title : 'Đang cập nhật'}</h5>
              <h6 class="card-subtitle mb-2 text-muted">
                <i class="mdi mdi-home-map-marker"></i> ${estate.address ? estate.address : ' '}
              </h6>
              <h2 class="text-success mb-0 mt-3"><small>Giá: </small>${
                estate.price ? estate.price : ' '
              } <small>tỷ </small></h2>
                <small>Tổng diện tích: </small> ${estate.acreage ? estate.acreage : ' '} m2
              </h4>
            </div>
            <div class="card-footer">
              <span>
                <i class="mdi mdi-sofa"></i> PN: <strong>${estate.bedroom ? estate.bedroom : ' '}</strong>
              </span>
              <span>
                <i class="mdi mdi-scale-bathroom"></i> | PT: <strong>${estate.bath ? estate.bath : ' '}</strong>
              </span>
              <span>
                <i class="mdi mdi-move-resize-variant"></i> | Ban công: <strong>${
                  estate.balcony ? estate.balcony : ' '
                }</strong>
              </span>
            </div>
          </a>
        </div>     
      </div>     
    `)
    }
  }

  // paging
  function pageEstate() {
    var items = $('.all-estate-item')
    var numItems = items.length
    var perPage = 9
    items.slice(perPage).hide()

    $('#pagination-container').pagination({
      items: numItems,
      itemsOnPage: perPage,
      hrefTextPrefix: '#',
      prevText: '&laquo;',
      edges: 1,
      displayedPages: 3,
      nextText: '&raquo;',
      onPageClick: function (pageNumber) {
        var showFrom = perPage * (pageNumber - 1)
        var showTo = showFrom + perPage
        items.hide().slice(showFrom, showTo).show()
      },
    })
    $('#pagination-container').pagination(
      'selectPage',
      window.location.hash.substring(1) ? window.location.hash.substring(1) : 1
    )
  }
})
