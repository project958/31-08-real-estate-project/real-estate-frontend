$(document).ready(function () {
  /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
  const gAPI_URL = 'http://localhost:8080/'

  let gEstate = []
  let gEstateFound = []
  let vUrlString = new URL(window.location.href)
  let gProjectId = vUrlString.searchParams.get('projectid')
  // const numberOption = {
  //   currencySymbol: ' VNĐ',
  //   currencySymbolPlacement: 's',
  //   decimalCharacter: ',',
  //   decimalPlaces: 0,
  //   digitGroupSeparator: '.',
  // }
  // new AutoNumeric('#pricefrom', numberOption)
  // new AutoNumeric('#priceto', numberOption)

  /*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */

  onPageLoading()
  // gán sự kiện menu khi cuộn
  $(window).scroll(navbarTranform)
  // gán sự kiện nút tìm kiếm
  $(document).on('click', '#search-btn', onSearchBtnClick)
  // gán sự kiện chọn sắp xếp
  $(document).on('change', '#sort-estate', onSelectSortChange)

  /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */

  function onPageLoading() {
    navbarTranform()
    getAllEstate()
  }

  /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/

  // xử lý menu khi cuộn trang
  function navbarTranform() {
    if ($(document).scrollTop() > 30) {
      $('#navbar-main').addClass('transform')
      $('#navbar-main').addClass('navbar-dark bg-dark')
      $('#navbar-main').removeClass('navbar-light  bg-light')
    } else {
      $('#navbar-main').removeClass('transform')
      $('#navbar-main').removeClass('navbar-dark bg-dark')
      $('#navbar-main').addClass('navbar-light bg-light')
    }
  }

  // get all estate
  function getAllEstate() {
    if (gProjectId) {
      $.ajax({
        type: 'get',
        url: gAPI_URL + 'project/' + gProjectId,
        dataType: 'json',
        success: function (response) {
          // console.log(response)
          $('#content-title').html('BĐS trong dự án: ' + response.name)
          $('#project-detail').append(`<h2 class="my-4 content-title">Tổng quan dự án: ${
            response.name ? response.name : ' '
          }</h2>
        <div class="container-fluid p-4 bg-light">
        <div class="row px-2">
          <div class="col-lg-12 p-1 py-2"><strong>Địa chỉ: </strong>  ${
            response.address ? response.address : ' '
          } </div>
          <div class="col-lg-12 p-1 py-2"><strong>Mô tả: </strong> ${
            response.description ? response.description : ' '
          } </div>
          <div class="col-lg-6 p-1 py-2"><strong>Số tòa nhà: </strong>  ${
            response.numBlock ? response.numBlock : ' '
          }</div>
          <div class="col-lg-6 p-1 py-2"><strong>Số tầng: </strong> ${
            response.numFloors ? response.numFloors : ' '
          }</div>
          <div class="col-lg-6 p-1 py-2"><strong>Số phòng: </strong> ${
            response.numApartment ? response.numApartment : ''
          }</div>
          <div class="col-lg-6 p-1 py-2"><strong> Tổng diện tích: </strong> ${
            response.acreage ? response.acreage : ' '
          } m2</div>          
          </div>
        </div>`)
        },
      })
      $.ajax({
        type: 'get',
        url: gAPI_URL + 'real-estate/project/' + gProjectId + '/page/0/limit/50/sort/id',
        dataType: 'json',
        success: function (response) {
          // console.log(response)
          gEstate = response
          addEstateToHtml(response)
          pageEstate()
        },
      })
    } else {
      $.ajax({
        type: 'get',
        url: gAPI_URL + 'real-estate/status/1/page/0/limit/100/sort/id',
        dataType: 'json',
        success: function (response) {
          // console.log(response)
          addEstateToHtml(response)
          gEstate = response
          pageEstate()
        },
      })
    }
  }

  // add Estate to html
  function addEstateToHtml(paramProject) {
    $('.all-estate').html('')

    for (const estate of paramProject) {
      $('.all-estate').append(`      
      <div class="col-lg-4 col-md-6 mt-4 all-estate-item"> 
        <div class="card card-list">
          <a href="estate.html?estateid=${estate.id}">
            <img
              class="card-img-top"
              src="./images/bds/${estate.photo ? estate.photo : '11.jpg'}"
              alt="Card image cap"
            />
            <div class="card-body">
              <h5 class="card-title">${estate.title ? estate.title : 'Đang cập nhật'}</h5>
              <h6 class="card-subtitle mb-2 text-muted">
                <i class="mdi mdi-home-map-marker"></i> ${estate.address ? estate.address : ' '}
              </h6>
              <h2 class="text-success mb-0 mt-3"><small>Giá: </small>${
                estate.price ? estate.price : ' '
              } <small>tỷ </small></h2>
                <small>Tổng diện tích: </small> ${estate.acreage ? estate.acreage : ' '} m2
              </h4>
            </div>
            <div class="card-footer">
              <span>
                <i class="mdi mdi-sofa"></i> PN: <strong>${estate.bedroom ? estate.bedroom : ' '}</strong>
              </span>
              <span>
                <i class="mdi mdi-scale-bathroom"></i> | PT: <strong>${estate.bath ? estate.bath : ' '}</strong>
              </span>
              <span>
                <i class="mdi mdi-move-resize-variant"></i> | Ban công: <strong>${
                  estate.balcony ? estate.balcony : ' '
                }</strong>
              </span>
            </div>
          </a>
        </div>     
      </div>     
    `)
    }
  }

  function onSearchBtnClick() {
    let objFind = {
      address: $('#searh-input').val(),
      type: $('#type-estate').val(),
      request: $('#request-estate').val(),
    }
    // console.log(objFind)
    gEstateFound = gEstate.filter(function (estate) {
      return (
        (removeAccents(estate.address.toLowerCase()).includes(removeAccents(objFind.address.toLowerCase())) ||
          objFind.address == '') &&
        (estate.type == objFind.type || objFind.type == '') &&
        (estate.request == objFind.request || objFind.request == '')
      )
    })
    // console.log(gEstateFound)
    addEstateToHtml(gEstateFound)
    pageEstate()
  }

  // sắp sếp khi chọn
  function onSelectSortChange(e) {
    // console.log(gEstateFound)
    if (gEstateFound.length > 0) {
      sortEstate(this.value, gEstateFound)
      addEstateToHtml(gEstateFound)
      pageEstate()
    } else {
      sortEstate(this.value, gEstate)
      addEstateToHtml(gEstate)
      pageEstate()
    }
  }

  // sắp xếp
  function sortEstate(paramValue, paramObj) {
    switch (paramValue) {
      case 'giatang':
        paramObj.sort(function (a, b) {
          return a.price - b.price
        })
        break
      case 'giagiam':
        paramObj.sort(function (a, b) {
          return b.price - a.price
        })
        break
      case 'moi':
        paramObj.sort(function (a, b) {
          return b.id - a.id
        })
        break
      case 'cu':
        paramObj.sort(function (a, b) {
          return a.id - b.id
        })
        break

      default:
        paramObj.sort(function (a, b) {
          return b.id - a.id
        })
        break
    }
  }

  // bỏ dâu tiếng việt
  function removeAccents(str) {
    var AccentsMap = [
      'aàảãáạăằẳẵắặâầẩẫấậ',
      'AÀẢÃÁẠĂẰẲẴẮẶÂẦẨẪẤẬ',
      'dđ',
      'DĐ',
      'eèẻẽéẹêềểễếệ',
      'EÈẺẼÉẸÊỀỂỄẾỆ',
      'iìỉĩíị',
      'IÌỈĨÍỊ',
      'oòỏõóọôồổỗốộơờởỡớợ',
      'OÒỎÕÓỌÔỒỔỖỐỘƠỜỞỠỚỢ',
      'uùủũúụưừửữứự',
      'UÙỦŨÚỤƯỪỬỮỨỰ',
      'yỳỷỹýỵ',
      'YỲỶỸÝỴ',
    ]
    for (var i = 0; i < AccentsMap.length; i++) {
      var re = new RegExp('[' + AccentsMap[i].substr(1) + ']', 'g')
      var char = AccentsMap[i][0]
      str = str.replace(re, char)
    }
    return str
  }

  // paging
  function pageEstate() {
    var items = $('.all-estate-item')
    var numItems = items.length
    var perPage = 9
    items.slice(perPage).hide()

    $('#pagination-container').pagination({
      items: numItems,
      itemsOnPage: perPage,
      hrefTextPrefix: '#',
      prevText: '&laquo;',
      edges: 1,
      displayedPages: 3,
      nextText: '&raquo;',
      onPageClick: function (pageNumber) {
        var showFrom = perPage * (pageNumber - 1)
        var showTo = showFrom + perPage
        items.hide().slice(showFrom, showTo).show()
      },
    })
    // $('#pagination-container').pagination(
    // 'selectPage',
    // window.location.hash.substring(1) ? window.location.hash.substring(1) : 1
    // )
  }
})
