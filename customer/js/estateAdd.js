$(document).ready(function () {
  'use strict'

  /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
  const gAPI_URL = 'http://localhost:8080/'

  let gDistrictId = ''
  let gProvinceId = ''
  let gWardId = ''
  let gProvinceIdUpdate = ''
  let gUserId
  let gCustomerId
  let gProvince = []
  let gDistrict = []
  let gWard = []
  let gStreet = []

  // crud data
  let gData = {
    title: '',
    type: '',
    request: '',
    provinceId: '',
    districtId: '',
    wardsId: '',
    streetId: '',
    projectId: '',
    address: '',
    customerId: '',
    price: '',
    priceMin: '',
    priceTime: '',
    // dateCreate: "",
    acreage: '',
    direction: '',
    totalFloors: '',
    numberFloors: '',
    bath: '',
    apartCode: "'",
    wallArea: '',
    bedroom: '',
    balcony: '',
    landscapeView: '',
    apartLoca: '',
    apartType: '',
    furnitureType: '',
    priceRent: '',
    returnRate: '',
    legalDoc: '',
    description: '',
    widthY: '',
    streetHouse: 0,
    viewNum: '',
    createBy: '',
    updateBy: '',
    shape: '',
    distance2facade: '',
    adjacentFacadeNum: '',
    adjacentRoad: '',
    alleyMinWidth: '',
    adjacentAlleyMinWidth: '',
    factor: '',
    structure: '',
    // photo: '',
    lat: '',
    lng: '',
    ctxdprice: '',
    ctxdvalue: '',
    dtsxd: '',
    clcl: '',
    fsbo: 0,
    longX: '',
  }

  let gUrlString = new URL(window.location.href)
  let gEstateId = gUrlString.searchParams.get('estateid')

  const token = getCookie('token')
  // console.log('token', token)
  // Khai báo xác thực ở headers
  var headers = {
    Authorization: 'Token ' + token,
  }

  /*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */

  onPageLoading()
  // gán sự kiện menu khi cuộn trang
  $(window).scroll(navbarTranform)
  // gán sự kiện change province select
  $('#select-province').change(changeProvince)
  // gán sự kiện change district select
  $('#select-district').change(changeDistrict)
  // gán sự kiện change ward select
  $('#select-ward').change(changeWard)
  // gán sự kiện nút create
  $('#create-data').click(function (e) {
    e.preventDefault()
    createNewEstate()
  })
  // gán sự kiện nút update
  $('#btn-save').click(function (e) {
    e.preventDefault()
    updateNewEstate()
  })

  /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */

  function onPageLoading() {
    navbarTranform()
    getUserAuthorize()
    getAllProvince()
    loadAllEstateToSelect()
    loadDataToUpdateForm()
    loadDistrictToUpdateForm()
    loadWardToUpdateForm()
    loadStreetToUpdateForm()
  }

  // province change
  function changeProvince() {
    gProvinceId = $('#select-province').val()
    $('#select-district').html('')
    $('#select-district').append(
      $('<option>', {
        value: '',
        text: 'Quận huyện',
      })
    )
    $('#select-ward').html('')
    $('#select-ward').append(
      $('<option>', {
        value: '',
        text: 'Phường xã',
      })
    )
    $('#select-street').html('')
    $('#select-street').append(
      $('<option>', {
        value: '',
        text: 'Đường phố',
      })
    )
    getDistrictOfProvince(gProvinceId)
  }

  // district change
  function changeDistrict() {
    gDistrictId = $('#select-district').val()
    $('#select-ward').html('')
    $('#select-ward').append(
      $('<option>', {
        value: '',
        text: 'Phường xã',
      })
    )
    $('#select-street').html('')
    $('#select-street').append(
      $('<option>', {
        value: '',
        text: 'Đường phố',
      })
    )
    getWardOfDistrict(gDistrictId)
  }

  // ward change
  function changeWard() {
    gWardId = $('#select-ward').val()
    $('#select-street').html('')
    $('#select-street').append(
      $('<option>', {
        value: '',
        text: 'Đường phố',
      })
    )
    getStreetOfWard(gDistrictId)
  }

  /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
  // xử lý khi cuộn trang
  function navbarTranform() {
    if ($(document).scrollTop() > 30) {
      $('#navbar-main').addClass('transform')
      $('#navbar-main').addClass('navbar-dark bg-dark')
      $('#navbar-main').removeClass('navbar-light  bg-light')
    } else {
      $('#navbar-main').removeClass('transform')
      $('#navbar-main').removeClass('navbar-dark bg-dark')
      $('#navbar-main').addClass('navbar-light bg-light')
    }
  }

  // Authorization
  function getUserAuthorize() {
    if (token) {
      callApiGetUserIdByToken(token)
      handleLogged()
    } else {
      window.location.href = 'login.html'
    }
  }

  // get user id
  function callApiGetUserIdByToken(paramToken) {
    $.ajax({
      url: gAPI_URL + 'findUserId/Token',
      type: 'GET',
      dataType: 'json',
      contentType: 'application/json; charset=utf-8',
      headers: {
        Authorization: 'Token ' + paramToken,
      },
      success: function (responseObject) {
        // console.log('user id: ', responseObject)
        gUserId = responseObject
        getUserInfo(responseObject)
      },
      error: function (pAjaxContext) {
        //redirectToLogin();
      },
    })
  }

  // get user info to redirect
  function getUserInfo(paramId) {
    $.ajax({
      type: 'get',
      url: gAPI_URL + 'customer/user/' + paramId,
      dataType: 'json',
      async: false,
      success: function (response) {
        gCustomerId = response.id
        // console.log('gCustomerId', response.id)
      },
    })
  }

  // handle logged menu
  function handleLogged() {
    $('.list-inline.main-nav-right.my-2').html(` <li class="list-inline-item">
    <a id="btn-logout" class="btn btn-danger btn-sm" href="">Đăng xuất</a>
  </li>`)
    $('#register-hidden-form').hide()
  }

  //Hàm get Cookie
  function getCookie(cname) {
    var name = cname + '='
    var decodedCookie = decodeURIComponent(document.cookie)
    var ca = decodedCookie.split(';')
    for (var i = 0; i < ca.length; i++) {
      var c = ca[i]
      while (c.charAt(0) == ' ') {
        c = c.substring(1)
      }
      if (c.indexOf(name) == 0) {
        return c.substring(name.length, c.length)
      }
    }
    return ''
  }

  // load all province to select
  function getAllProvince() {
    $.ajax({
      type: 'get',
      url: gAPI_URL + 'provinces',
      headers: headers,
      dataType: 'json',
      async: false,
      success: function (response) {
        // gProvince = response
        for (const province of response) {
          $('#select-province').append(
            $('<option>', {
              value: province.id,
              text: province.name,
            })
          )
          $('#select-province-update').append(
            $('<option>', {
              value: province.id,
              text: province.name,
            })
          )
        }
      },
    })
  }

  // load district to update form
  function loadDistrictToUpdateForm() {
    if (gEstateId) {
      $.ajax({
        type: 'get',
        url: gAPI_URL + 'districts',
        dataType: 'json',
        headers: headers,
        async: false,
        success: function (response) {
          for (const district of response) {
            $('#select-district-update').append(
              $('<option>', {
                value: district.id,
                text: district.name,
              })
            )
          }
        },
      })
    }
  }

  // load ward to update form
  function loadWardToUpdateForm() {
    if (gEstateId) {
      $.ajax({
        type: 'get',
        url: gAPI_URL + 'wards',
        dataType: 'json',
        headers: headers,
        async: false,
        success: function (response) {
          for (const ward of response) {
            $('#select-ward-update').append(
              $('<option>', {
                value: ward.id,
                text: ward.name,
              })
            )
          }
        },
      })
    }
  }
  // load street to update form
  function loadStreetToUpdateForm() {
    if (gEstateId) {
      $.ajax({
        type: 'get',
        url: gAPI_URL + 'streets',
        dataType: 'json',
        headers: headers,
        async: false,
        success: function (response) {
          for (const street of response) {
            $('#select-street-update').append(
              $('<option>', {
                value: street.id,
                text: street.name,
              })
            )
          }
        },
      })
    }
  }

  // get district of province
  function getDistrictOfProvince(paramId) {
    if (paramId) {
      $.ajax({
        headers: headers,
        type: 'get',
        url: gAPI_URL + 'province/' + paramId + '/districts',
        dataType: 'json',
        success: function (response) {
          // console.log('district', response)
          for (const district of response) {
            $('#select-district').append(
              $('<option>', {
                value: district.id,
                text: district.name,
              })
            )
          }
        },
      })
    }
  }

  // get district of province
  function getWardOfDistrict(paramId) {
    if (paramId) {
      $.ajax({
        headers: headers,
        type: 'get',
        url: gAPI_URL + 'district/' + paramId + '/wards',
        dataType: 'json',
        success: function (response) {
          // console.log('ward', response)
          for (const ward of response) {
            $('#select-ward').append(
              $('<option>', {
                value: ward.id,
                text: ward.name,
              })
            )
          }
        },
      })
    }
  }

  // get street of ward
  function getStreetOfWard(paramId) {
    if (paramId) {
      $.ajax({
        headers: headers,
        type: 'get',
        url: gAPI_URL + 'district/' + paramId + '/street',
        dataType: 'json',
        success: function (response) {
          // console.log('street', response)
          for (const street of response) {
            $('#select-street').append(
              $('<option>', {
                value: street.id,
                text: street.prefix + ' ' + street.name,
              })
            )
          }
        },
      })
    }
  }

  // load project to select
  function loadAllEstateToSelect() {
    $.ajax({
      type: 'get',
      url: gAPI_URL + 'projectsAll',
      dataType: 'json',
      headers: headers,
      async: false,
      success: function (response) {
        // console.log('project', response)
        $('#select-project').html('')
        $('#select-project').append(
          $('<option>', {
            value: '',
            text: 'Chọn dự án',
          })
        )
        $('#select-project-update').html('')
        $('#select-project-update').append(
          $('<option>', {
            value: '',
            text: 'Chọn dự án',
          })
        )
        for (const data of response) {
          $('#select-project').append(
            $('<option>', {
              value: data.id,
              text: data.name,
            })
          )
          $('#select-project-update').append(
            $('<option>', {
              value: data.id,
              text: data.name,
            })
          )
        }
      },
    })
  }

  // load data to update form if gEstateId exist
  function loadDataToUpdateForm() {
    if (gEstateId) {
      // console.log(gEstateId)
      $.ajax({
        type: 'get',
        url: gAPI_URL + 'real-estate/' + gEstateId,
        dataType: 'json',
        success: function (response) {
          // console.log(response)
          // gData = response
          loadDataToInput(response)
        },
      })
    }
  }

  // create
  function createNewEstate() {
    //get data
    getDataInput(gData)
    // console.log(gData)
    if ($('#create-form').valid()) {
      Swal.fire({
        title: 'Vui lòng xác nhận?',
        position: 'top',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Xác nhận',
      }).then((result) => {
        if (result.isConfirmed) {
          $.ajax({
            type: 'post',
            headers: headers,
            url: gAPI_URL + 'real-estate',
            data: JSON.stringify(gData),
            contentType: 'application/json; charset=utf-8',
            success: function (respond) {
              // Swal.fire({
              //   position: 'top',
              //   icon: 'success',
              //   title: 'Tạo mới thành công',
              //   showConfirmButton: false,
              //   timer: 1500,
              // })
              // console.log('respond', respond)
              resetDataForm()
              window.location.href = 'manageList.html'
            },
            error: function (error) {
              // alert(error.statusText)
              // console.log(error.responseText)
              Swal.fire({
                position: 'top',
                icon: 'error',
                title: error.responseText,
                showConfirmButton: false,
                timer: 2000,
              })
            },
          })
        }
      })
    }
  }

  // call api update
  function updateNewEstate() {
    getDataInputUpdate(gData)
    // console.log(gData)
    if ($('#update-form').valid()) {
      Swal.fire({
        title: 'Vui lòng xác nhận?',
        position: 'top',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Xác nhận',
      }).then((result) => {
        if (result.isConfirmed) {
          // console.log('gData', gData)
          // call api update
          $.ajax({
            type: 'put',
            url: gAPI_URL + 'real-estate/' + gEstateId,
            headers: headers,
            data: JSON.stringify(gData),
            async: false,
            contentType: 'application/json; charset=utf-8',
            success: function (response) {
              // console.log(response)
              // Swal.fire({
              //   position: 'top',
              //   icon: 'success',
              //   title: 'Cập nhật thành công',
              //   showConfirmButton: false,
              //   timer: 1500,
              // })
              window.location.href = 'manageList.html'
            },
            error: function (error) {
              // console.log(error.responseText)
              Swal.fire({
                position: 'top',
                icon: 'error',
                title: error.responseText,
                showConfirmButton: false,
                timer: 20000,
              })
            },
          })
        }
      })
    }
  }

  // get data input
  function getDataInput(paramObj) {
    paramObj.title = $('#input-title').val().trim()
    paramObj.type = $('#select-type').val().trim()
    paramObj.request = $('#select-request').val().trim()
    paramObj.provinceId = $('#select-province').val()
    paramObj.districtId = $('#select-district').val()
    paramObj.streetId = $('#select-street').val()
    paramObj.wardsId = $('#select-ward').val()
    paramObj.projectId = $('#select-project').val()
    paramObj.address = $('#input-address').val().trim()
    paramObj.customerId = gCustomerId
    paramObj.createBy = gUserId
    paramObj.price = $('#input-price').val().trim()
    paramObj.priceMin = $('#input-priceMin').val().trim()
    paramObj.priceTime = $('#select-priceTime').val()
    paramObj.acreage = $('#input-acreage').val().trim()
    paramObj.direction = $('#input-direction').val().trim()
    paramObj.totalFloors = $('#input-totalFloors').val().trim()
    paramObj.numberFloors = $('#input-numberFloors').val().trim()
    paramObj.bath = $('#input-bath').val().trim()
    paramObj.apartCode = $('#input-apartCode').val().trim()
    paramObj.wallArea = $('#input-wallArea').val().trim()
    paramObj.bedroom = $('#input-bedroom').val().trim()
    paramObj.balcony = $('#select-balcony').val().trim()
    paramObj.landscapeView = $('#input-landscapeView').val()
    paramObj.apartLoca = $('#select-apartLoca').val().trim()
    paramObj.apartType = $('#select-apartType').val()
    paramObj.furnitureType = $('#select-furnitureType').val()
    paramObj.priceRent = $('#input-priceRent').val().trim()
    paramObj.returnRate = $('#input-returnRate').val().trim()
    paramObj.legalDoc = $('#input-legalDoc').val().trim()
    paramObj.description = $('#input-description').val().trim()
    paramObj.widthY = $('#input-widthY').val().trim()
    paramObj.streetHouse = $("input[name='streetHouse']").is(':checked') ? 1 : 0
    paramObj.viewNum = $('#input-viewNum').val().trim()
    paramObj.shape = $('#input-shape').val().trim()
    paramObj.distance2facade = $('#input-distance2facade').val().trim()
    paramObj.adjacentFacadeNum = $('#input-adjacentFacadeNum').val().trim()
    paramObj.adjacentRoad = $('#input-adjacentRoad').val().trim()
    paramObj.alleyMinWidth = $('#input-alleyMinWidth').val().trim()
    paramObj.adjacentAlleyMinWidth = $('#input-adjacentAlleyMinWidth').val().trim()
    paramObj.factor = $('#input-factor').val().trim()
    paramObj.structure = $('#input-structure').val().trim()
    paramObj.photo = $('#input-photo').val()
    paramObj.lat = $('#input-lat').val().trim()
    paramObj.lng = $('#input-lng').val().trim()
    paramObj.ctxdprice = $('#input-ctxdprice').val().trim()
    paramObj.ctxdvalue = $('#input-ctxdvalue').val().trim()
    paramObj.dtsxd = $('#input-dtsxd').val().trim()
    paramObj.clcl = $('#input-clcl').val().trim()
    paramObj.fsbo = $("input[name='fsbo']").is(':checked') ? 1 : 0
    paramObj.longX = $('#input-longX').val().trim()
  }
  // get data input update
  function getDataInputUpdate(paramObj) {
    paramObj.title = $('#input-title-update').val().trim()
    paramObj.type = $('#select-type-update').val().trim()
    paramObj.request = $('#select-request-update').val().trim()
    paramObj.provinceId = $('#select-province-update').val()
    paramObj.districtId = $('#select-district-update').val()
    paramObj.streetId = $('#select-street-update').val()
    paramObj.wardsId = $('#select-ward-update').val()
    paramObj.projectId = $('#select-project-update').val()
    paramObj.address = $('#input-address-update').val().trim()
    paramObj.customerId = gCustomerId
    paramObj.updateBy = gUserId
    paramObj.price = $('#input-price-update').val().trim()
    paramObj.priceMin = $('#input-priceMin-update').val().trim()
    paramObj.priceTime = $('#select-priceTime-update').val()
    paramObj.acreage = $('#input-acreage-update').val().trim()
    paramObj.direction = $('#input-direction-update').val().trim()
    paramObj.totalFloors = $('#input-totalFloors-update').val().trim()
    paramObj.numberFloors = $('#input-numberFloors-update').val().trim()
    paramObj.bath = $('#input-bath-update').val().trim()
    paramObj.apartCode = $('#input-apartCode-update').val().trim()
    paramObj.wallArea = $('#input-wallArea-update').val().trim()
    paramObj.bedroom = $('#input-bedroom-update').val().trim()
    paramObj.balcony = $('#select-balcony-update').val().trim()
    paramObj.landscapeView = $('#input-landscapeView-update').val()
    paramObj.apartLoca = $('#select-apartLoca-update').val().trim()
    paramObj.apartType = $('#select-apartType-update').val()
    paramObj.furnitureType = $('#select-furnitureType-update').val()
    paramObj.priceRent = $('#input-priceRent-update').val().trim()
    paramObj.returnRate = $('#input-returnRate-update').val().trim()
    paramObj.legalDoc = $('#input-legalDoc-update').val().trim()
    paramObj.description = $('#input-description-update').val().trim()
    paramObj.widthY = $('#input-widthY-update').val().trim()
    paramObj.streetHouse = $("input[name='streetHouse']").is(':checked') ? 1 : 0
    paramObj.viewNum = $('#input-viewNum-update').val().trim()
    paramObj.shape = $('#input-shape-update').val().trim()
    paramObj.distance2facade = $('#input-distance2facade-update').val().trim()
    paramObj.adjacentFacadeNum = $('#input-adjacentFacadeNum-update').val().trim()
    paramObj.adjacentRoad = $('#input-adjacentRoad-update').val().trim()
    paramObj.alleyMinWidth = $('#input-alleyMinWidth-update').val().trim()
    paramObj.adjacentAlleyMinWidth = $('#input-adjacentAlleyMinWidth-update').val().trim()
    paramObj.factor = $('#input-factor-update').val().trim()
    paramObj.structure = $('#input-structure-update').val().trim()
    paramObj.photo = $('#input-photo-update').val()
    paramObj.lat = $('#input-lat-update').val().trim()
    paramObj.lng = $('#input-lng-update').val().trim()
    paramObj.ctxdprice = $('#input-ctxdprice-update').val().trim()
    paramObj.ctxdvalue = $('#input-ctxdvalue-update').val().trim()
    paramObj.dtsxd = $('#input-dtsxd-update').val().trim()
    paramObj.clcl = $('#input-clcl-update').val().trim()
    paramObj.fsbo = $("input[name='fsbo']").is(':checked') ? 1 : 0
    paramObj.longX = $('#input-longX-update').val().trim()
    paramObj.status = $('#select-status-update').val()
  }

  // load data input update
  function loadDataToInput(paramObj) {
    $('#input-title-update').val(paramObj.title)
    $('#select-type-update').val(paramObj.type)
    $('#select-request-update').val(paramObj.request)
    $('#select-province-update').val(paramObj.provinceId)
    $('#select-district-update').val(paramObj.districtId)
    $('#select-street-update').val(paramObj.streetId)
    $('#select-ward-update').val(paramObj.wardsId)
    $('#select-project-update').val(paramObj.projectId)
    $('#input-address-update').val(paramObj.address)
    $('#input-price-update').val(paramObj.price)
    $('#input-priceMin-update').val(paramObj.priceMin)
    $('#select-priceTime-update').val(paramObj.priceTime)
    $('#input-acreage-update').val(paramObj.acreage)
    $('#input-direction-update').val(paramObj.direction)
    $('#input-totalFloors-update').val(paramObj.totalFloors)
    $('#input-numberFloors-update').val(paramObj.numberFloors)
    $('#input-bath-update').val(paramObj.bath)
    $('#input-apartCode-update').val(paramObj.apartCode)
    $('#input-wallArea-update').val(paramObj.wallArea)
    $('#input-bedroom-update').val(paramObj.bedroom)
    $('#select-balcony-update').val(paramObj.balcony)
    $('#input-landscapeView-update').val(paramObj.landscapeView)
    $('#select-apartLoca-update').val(paramObj.apartLoca)
    $('#select-apartType-update').val(paramObj.apartType)
    $('#select-furnitureType-update').val(paramObj.furnitureType)
    $('#input-priceRent-update').val(paramObj.priceRent)
    $('#input-returnRate-update').val(paramObj.returnRate)
    $('#input-legalDoc-update').val(paramObj.legalDoc)
    $('#input-description-update').val(paramObj.description)
    $('#input-widthY-update').val(paramObj.widthY)
    1 ? $('#input-streetHouse-update').prop('checked', true) : $('#input-streetHouse-update').prop('checked', false)
    $('#input-viewNum-update').val(paramObj.viewNum)
    $('#input-shape-update').val(paramObj.shape)
    $('#input-distance2facade-update').val(paramObj.distance2facade)
    $('#input-adjacentFacadeNum-update').val(paramObj.adjacentFacadeNum)
    $('#input-adjacentRoad-update').val(paramObj.adjacentRoad)
    $('#input-alleyMinWidth-update').val(paramObj.alleyMinWidth)
    $('#input-adjacentAlleyMinWidth-update').val(paramObj.adjacentAlleyMinWidth)
    $('#input-factor-update').val(paramObj.factor)
    $('#input-structure-update').val(paramObj.structure)
    // $('#input-photo-update').val(paramObj.photo)
    $('#input-lat-update').val(paramObj.lat)
    $('#input-lng-update').val(paramObj.lng)
    $('#input-ctxdprice-update').val(paramObj.ctxdprice)
    $('#input-ctxdvalue-update').val(paramObj.ctxdvalue)
    $('#input-dtsxd-update').val(paramObj.dtsxd)
    $('#input-clcl-update').val(paramObj.clcl)
    1 ? $('#input-fsbo-update').prop('checked', true) : $('#input-fsbo-update').prop('checked', false)
    $('#input-longX-update').val(paramObj.longX)
    $('#select-status-update').val(paramObj.status)
  }

  // reset form
  function resetDataForm() {
    $('#input-title').val('')
    $('#select-type').val('')
    $('#select-request').val('')
    $('#select-province').val('')
    $('#select-district').val('')
    $('#select-street').val('')
    $('#select-ward').val('')
    $('#select-project').val('')
    $('#input-address').val('')
    $('#select-customer').val('')
    $('#input-price').val('')
    $('#input-priceMin').val('')
    $('#select-priceTime').val('')
    $('#input-acreage').val('')
    $('#input-direction').val('')
    $('#input-totalFloors').val('')
    $('#input-numberFloors').val('')
    $('#input-bath').val('')
    $('#input-apartCode').val('')
    $('#input-wallArea').val('')
    $('#input-bedroom').val('')
    $('#select-balcony').val('')
    $('#input-landscapeView').val('')
    $('#select-apartLoca').val('')
    $('#select-apartType').val('')
    $('#select-furnitureType').val('')
    $('#input-priceRent').val('')
    $('#input-returnRate').val('')
    $('#input-legalDoc').val('')
    $('#input-description').val('')
    $('#input-widthY').val('')
    $('#input-streetHouse').val('')
    $('#input-viewNum').val('')
    $('#input-shape').val('')
    $('#input-distance2facade').val('')
    $('#input-adjacentFacadeNum').val('')
    $('#input-adjacentRoad').val('')
    $('#input-alleyMinWidth').val('')
    $('#input-adjacentAlleyMinWidth').val('')
    $('#input-factor').val('')
    $('#input-structure').val('')
    $('#input-photo').val('')
    $('#input-lat').val('')
    $('#input-lng').val('')
    $('#input-ctxdprice').val('')
    $('#input-ctxdvalue').val('')
    $('#input-dtsxd').val('')
    $('#input-clcl').val('')
    $('#input-fsbo').val('')
    $('#input-longX').val('')
  }

  // validate data
  $('#create-form').validate({
    rules: {
      name: 'required',
      address: 'required',
      province: 'required',
      district: 'required',
      ward: 'required',
      title: 'required',
      request: 'required',
      price: { required: true, number: true },
      priceMin: { number: true },
    },
    messages: {
      name: 'Vui lòng nhập tên',
      address: 'Vui lòng nhập address',
      province: 'Vui lòng chọn tỉnh thành',
      district: 'Vui lòng chọn quận huyện',
      ward: 'Vui lòng chọn phường xã',
      title: 'Vui lòng nhập tiêu đề',
      request: 'Vui lòng chọn nhu cầu',
      price: 'Vui lòng nhập giá',
      tipriceMintle: 'Vui lòng nhập số',
    },
  })
  // validate data update
  $('#update-form').validate({
    rules: {
      name: 'required',
      address: 'required',
      province: 'required',
      district: 'required',
      ward: 'required',
      title: 'required',
      request: 'required',
      price: { required: true, number: true },
      priceMin: { number: true },
    },
    messages: {
      name: 'Vui lòng nhập tên',
      address: 'Vui lòng nhập address',
      province: 'Vui lòng chọn tỉnh thành',
      district: 'Vui lòng chọn quận huyện',
      ward: 'Vui lòng chọn phường xã',
      request: 'Vui lòng chọn nhu cầu',
      title: 'Vui lòng nhập tiêu đề',
      price: 'Vui lòng nhập giá',
      tipriceMintle: 'Vui lòng nhập số',
    },
  })

  $('#cancel-data').click(function (e) {
    e.preventDefault()
    resetDataForm()
    window.location.href = 'manageList.html'
    // console.log('cancel')
  })
})
