$(document).ready(function () {
  /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
  const gAPI_URL = 'http://localhost:8080/'

  // get param url
  let gUrlString = new URL(window.location.href)
  let gEstateId = gUrlString.searchParams.get('estateid')
  let gProjectOfEstate
  let gEstate = {}

  /*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */

  // chạy khi tải trang
  onPageLoading()
  // gán sự kiện cuộn trang navbar menu
  $(window).scroll(navbarTranform)
  // gán sự kiện nút submit
  $(document).on('click', '#submit-btn', function (e) {
    e.preventDefault()
    handleFormSubmit()
  })

  /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */

  function onPageLoading() {
    navbarTranform()
    getEstateById()
    getNewEstate()
  }

  // xử lý sự kiện submit
  function handleFormSubmit() {
    // get data
    let dataCustomer = {
      name: $('#inp-username').val(),
      mobile: $('#inp-phone').val(),
      email: $('#inp-email').val(),
      estateId: gEstateId,
    }
    // console.log(dataCustomer)
    if ($('#resquest-form').valid()) {
      Swal.fire({
        title: 'Vui lòng xác nhận?',
        position: 'top',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Vui lòng xác nhận',
      }).then((result) => {
        if (result.isConfirmed) {
          // call api register
          $.ajax({
            type: 'post',
            url: gAPI_URL + 'customer-request',
            data: JSON.stringify(dataCustomer),
            async: false,
            contentType: 'application/json; charset=utf-8',
            success: function (respond) {
              // console.log('customer', respond)
              resetform()
              handleSuccessRequest(gEstate)
            },
            error: function (error) {
              // console.log(error.responseText)
            },
          })
        }
      })
    }
  }

  /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/

  // hàm xử lý menu khi cuộn trang
  function navbarTranform() {
    if ($(document).scrollTop() > 30) {
      $('#navbar-main').addClass('transform')
      $('#navbar-main').addClass('navbar-dark bg-dark')
      $('#navbar-main').removeClass('navbar-light  bg-light')
    } else {
      $('#navbar-main').removeClass('transform')
      $('#navbar-main').removeClass('navbar-dark bg-dark')
      $('#navbar-main').addClass('navbar-light bg-light')
    }
  }

  // tải bđs theo id
  function getEstateById() {
    if (gEstateId) {
      $.ajax({
        type: 'get',
        url: gAPI_URL + 'real-estate/' + gEstateId,
        dataType: 'json',
        success: function (response) {
          addEstateToHtml(response)
          gEstate = response
        },
      })
    }
  }

  // reset form
  function resetform() {
    $('#inp-username').val('')
    $('#inp-phone').val('')
    $('#inp-email').val('')
  }

  // handle success request
  function handleSuccessRequest(paramObj) {
    $('#modal-success').modal('show')
    $('#modalbody-success').html(`
                <h3 class="my-2 text-center"> Quý khách sẽ nhận thông tin về căn hộ </h3> 
                <h2 class="my-2 text-info text-center"> ${paramObj.address} </h2> 
                <p class="my-2 text-center"> trong thời gian sớm nhất</p> 
                `)
  }

  // add Estate info to html
  function addEstateToHtml(paramEstate) {
    $('#detail-info').html('')
    if (paramEstate.projectId) {
      $.ajax({
        type: 'get',
        url: gAPI_URL + 'project/' + paramEstate.projectId,
        dataType: 'json',
        async: false,
        success: function (response) {
          // console.log(response)
          gProjectOfEstate = response
        },
      })
    }
    $('#detail-info').append(`      
    <h4 class="my-4">${paramEstate.title ? paramEstate.title : ' '}</h4>
    <div class="row  px-2">
      <div class="col-lg-12 p-1 py-2">Địa chỉ: <strong> ${paramEstate.address} </strong></div>
      <div class="col-lg-12 p-1 py-2">Thuộc dự án: <strong> ${
        gProjectOfEstate ? gProjectOfEstate.name : ''
      } </strong></div>
      <div class="col-lg-6 p-1 py-2">Danh mục: <strong> ${getTypeEstate(paramEstate.type)} </strong></div>
      <div class="col-lg-6 p-1 py-2">Giá: <strong> ${paramEstate.price ? paramEstate.price : ' '} </strong> tỷ</div>
      <div class="col-lg-6 p-1 py-2">Nhu cầu: <strong> ${getRequestEstate(paramEstate.request)}</strong></div>
      <div class="col-lg-6 p-1 py-2">Ngày đăng: <strong> ${
        paramEstate.dateCreate ? paramEstate.dateCreate : ' '
      }</strong></div>
      <div class="col-lg-6 p-1 py-2">Diện tích đất: <strong> ${
        paramEstate.acreage ? paramEstate.acreage : ' '
      }</strong> m2</div>
      <div class="col-lg-6 p-1 py-2">Phòng ngủ: <strong> ${
        paramEstate.bedroom ? paramEstate.bedroom : ' '
      }</strong></div>
      <div class="col-lg-6 p-1 py-2">Phòng tắm: <strong> ${paramEstate.bath ? paramEstate.bath : ' '}</strong></div>
      <div class="col-lg-6 p-1 py-2">Ban công: <strong> ${
        paramEstate.balcony ? paramEstate.balcony : ' '
      }</strong></div>
      <div class="col-lg-6 p-1 py-2">View: <strong> ${
        paramEstate.landscapeView ? paramEstate.landscapeView : ' '
      }</strong></div>
      <div class="col-lg-6 p-1 py-2">Nội thất: <strong> ${getFurniEstate(paramEstate.furnitureType)}</strong></div>
    </div>   
  `)
  }

  // get new relate estate new
  function getNewEstate() {
    $.ajax({
      type: 'get',
      url: gAPI_URL + 'real-estate/page/0/limit/5/sort/id',
      dataType: 'json',
      success: function (response) {
        // console.log(response)
        addToRealeteHtml(response)
      },
    })
  }

  // add to side new estate
  function addToRealeteHtml(paramProject) {
    $('#new-estate-list').html('')
    for (const project of paramProject) {
      $('#new-estate-list').append(`       
      <div class="card card-list my-4 bg">
      <a href="estate.html?estateid=${project.id}">
        <div class="row">
          <div class=" col-sm-12 col-md-5">
            <img
              class="card-img-top"
              style="width: 120px"
              src="./images/bds/${project.photo ? project.photo : '11.jpg'}"
              alt="image"
            />
          </div>

          <div class=" col-sm-12 col-md-6 ml-2">
            <h6 class="card-subtitle mt-2 text-muted">
              <i class="mdi mdi-home-map-marker"></i> ${project.address ? project.address : ' '}
            </h6>
            <p class="text-info mb-0 mt-2">
              <small>Diện tích: </small> <strong>${project.acreage ? project.acreage : ' '}</strong> m2 
              <br/>
              <small>Giá: </small> <strong>${project.price ? project.price : ' '}</strong>            
              tỷ
            </p>
          </div>
        </div>
      </a>
    </div>   
      `)
    }
  }

  // get type name
  function getTypeEstate(param) {
    switch (param) {
      case 0:
        return 'Đất'
        break
      case 1:
        return 'Nhà ở'
        break
      case 2:
        return 'Căn hộ/Chung cư'
        break
      case 3:
        return 'Văn phòng, Mặt bằng'
        break
      case 4:
        return 'Kinh doanh'
        break
      case 5:
        return 'Phòng trọ'
        break

      default:
        return ''
        break
    }
  }

  // get request name
  function getRequestEstate(param) {
    // console.log(param)
    switch (param) {
      case 0:
        return 'Cần bán'

      case 2:
        return 'Cần mua'

      case 3:
        return 'Cho thuê'

      case 4:
        return 'Cần thuê'

      default:
        return ''
    }
  }

  // get furniture name
  function getFurniEstate(param) {
    switch (param) {
      case 0:
        return 'cơ bản'
        break
      case 1:
        return 'đầy đủ'
        break
      case 3:
        return 'chưa biết'
        break

      default:
        return ''
        break
    }
  }

  // validate data
  $('#resquest-form').validate({
    rules: {
      username: 'required',
      phone: {
        required: true,
        number: true,
      },
      email: {
        required: true,
        email: true,
      },
    },
    messages: {
      username: 'Vui lòng nhập tên',
      phone: 'Vui lòng nhập số điện thoại',
      email: 'Vui lòng nhập email',
    },
  })
})
