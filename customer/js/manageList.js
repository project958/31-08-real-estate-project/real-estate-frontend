$(document).ready(function () {
  /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
  const gAPI_URL = 'http://localhost:8080/'

  const token = getCookie('token')
  let gEstate = []
  let gEstateFound = []
  // const numberOption = {
  //   currencySymbol: ' VNĐ',
  //   currencySymbolPlacement: 's',
  //   decimalCharacter: ',',
  //   decimalPlaces: 0,
  //   digitGroupSeparator: '.',
  // }

  // Khai báo xác thực ở headers
  var headers = {
    Authorization: 'Token ' + token,
  }

  /*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
  onPageLoading()
  // gán sự kiện menu khi cuộn trang
  $(window).scroll(navbarTranform)
  // gán sự kiện nút xóa
  $(document).on('click', '#delete-estate-btn', onDeleteBtn)
  // gán sự kiện nút search
  $(document).on('click', '#search-btn', onSearchBtn)
  // gán sự kiện chọn sắp xếp
  $(document).on('change', '#sort-estate', onSelectSortChange)

  /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */

  function onPageLoading() {
    if (token) {
      callApiGetUserIdByToken(token)
      addUserInfo()
    } else {
      window.location.href = 'login.html'
    }
    navbarTranform()
  }

  /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/

  // handle user logged
  function addUserInfo() {
    $('.list-inline.main-nav-right.my-2').html(` <li class="list-inline-item">
  <a id="btn-logout" class="btn btn-danger btn-sm" href="">Đăng xuất</a>
</li>`)
  }

  // get user id
  function callApiGetUserIdByToken(paramToken) {
    $.ajax({
      url: gAPI_URL + 'findUserId/Token',
      type: 'GET',
      dataType: 'json',
      contentType: 'application/json; charset=utf-8',
      headers: {
        Authorization: 'Token ' + paramToken,
      },
      success: function (responseObject) {
        // console.log('user id: ', responseObject)
        gUserId = responseObject
        getUserInfo(responseObject)
      },
      error: function (pAjaxContext) {
        //redirectToLogin();
      },
    })
  }

  // get user info to redirect
  function getUserInfo(paramId) {
    $.ajax({
      type: 'get',
      url: gAPI_URL + 'user/' + paramId,
      dataType: 'json',
      async: false,
      success: function (response) {
        // console.log(response)
        gUserRole = response.roles[0].roleKey
        // console.log(gUserRole)
        $('.info .d-block').html(response.username)
        handleUserAuth(gUserRole)
      },
    })
  }

  // handle user authorize
  function handleUserAuth(paramRole) {
    $.ajax({
      type: 'get',
      url: gAPI_URL + 'real-estate/createby/' + gUserId,
      headers: headers,
      dataType: 'json',
      success: function (response) {
        // console.log(response)
        // loadDataToList(response)
        let sortResult = response.sort((a, b) => b.id - a.id)
        addEstateToHtml(sortResult)
        pageEstate()
      },
    })
  }

  // get cookie
  function getCookie(cname) {
    var name = cname + '='
    var decodedCookie = decodeURIComponent(document.cookie)
    var ca = decodedCookie.split(';')
    for (var i = 0; i < ca.length; i++) {
      var c = ca[i]
      while (c.charAt(0) == ' ') {
        c = c.substring(1)
      }
      if (c.indexOf(name) == 0) {
        return c.substring(name.length, c.length)
      }
    }
    return ''
  }

  // xử lý menu khi cuộn trang
  function navbarTranform() {
    if ($(document).scrollTop() > 30) {
      $('#navbar-main').addClass('transform')
      $('#navbar-main').addClass('navbar-dark bg-dark')
      $('#navbar-main').removeClass('navbar-light  bg-light')
    } else {
      $('#navbar-main').removeClass('transform')
      $('#navbar-main').removeClass('navbar-dark bg-dark')
      $('#navbar-main').addClass('navbar-light bg-light')
    }
  }

  function loadDataToList() {
    $.ajax({
      type: 'get',
      url: gAPI_URL + 'real-estate/customer/' + gUserId,
      dataType: 'json',
      async: false,
      success: function (response) {
        // console.log(response)
        gEstate = response
        addEstateToHtml(response)
        pageEstate()
      },
    })
  }

  // delete article
  function onDeleteBtn(e) {
    $('#modal-delete-data').modal('show')
    let dataId = $(this).children().val()
    // console.log(dataId)

    // confirm delete data
    $(document).on('click', '#delete-data', function () {
      $.ajax({
        url: gAPI_URL + `real-estate/${dataId}`,
        headers: headers,
        method: 'DELETE',
        success: () => {
         Swal.fire({
          position: 'top',
          icon: 'success',
          title: 'Xóa thành công',
          showConfirmButton: false,
          timer: 1500,
        })
          $('#modal-delete-data').modal('hide')

          $.ajax({
            type: 'get',
            url: gAPI_URL + 'real-estate/createby/' + gUserId,
            headers: headers,
            dataType: 'json',
            success: function (response) {
              // console.log(response)
              // loadDataToList(response)
              let sortResult = response.sort((a, b) => b.id - a.id)
              addEstateToHtml(sortResult)
              pageEstate()
            },
          })
        },
        // error: (err) => console.log(err.responseText),
      })
    })
  }

  // add Estate to html
  function addEstateToHtml(paramProject) {
    $('.all-estate').html('')
    // console.log(paramProject)

    for (const estate of paramProject) {
      $('.all-estate').append(`      
      <div class="col-md-4 mt-4 all-estate-item"> 
        <div class="card card-list">        
          <a href="estate.html?estateid=${estate.id}">
            <img
              class="card-img-top"
              src="./images/bds/${estate.photo ? estate.photo : '11.jpg'}"
              alt="Card image cap"
            />
            <div class="card-body">
              <a href="estateEdit.html?estateid=${estate.id}" class="btn btn-warning float-right"> Sửa tin </a>
              <h5 class="card-title">${estate.title ? estate.title : 'Đang cập nhật'}</h5>
              <h6 class="card-subtitle mb-2 text-muted">
                <i class="mdi mdi-home-map-marker"></i> ${estate.address ? estate.address : ' '}
              </h6>
              <h2 class="text-success mt-3"><small>Giá: </small>${estate.price ? estate.price : ' '} <small>tỷ </small>
                <a class="btn btn-danger float-right" id="delete-estate-btn">
                  <input type="hidden" id="esate-id" name="esate-id"" value="${estate.id}" />
                   Xóa tin 
                </a>
              </h2>
              <h6>
                <small>Tổng diện tích: </small> ${estate.acreage ? estate.acreage : ' '} m2
              </h6>
             
            </div>
           
            <div class="card-footer">
              <span>
                <i class="mdi mdi-sofa"></i> PN: <strong>${estate.bedroom ? estate.bedroom : ' '}</strong>
              </span>
              <span>
                <i class="mdi mdi-scale-bathroom"></i> | PT: <strong>${estate.bath ? estate.bath : ' '}</strong>
              </span>
              <span>
                <i class="mdi mdi-move-resize-variant"></i> | Ban công: <strong>${
                  estate.balcony ? estate.balcony : ' '
                }</strong>
              </span>
            </div>
          </a>
        </div>     
      </div>     
    `)
    }
  }

  function onSearchBtn() {
    let objFind = {
      address: $('#searh-input').val(),
      type: $('#type-estate').val(),
      request: $('#request-estate').val(),
    }
    // console.log(objFind)
    gEstateFound = gEstate.filter(function (estate) {
      return (
        (removeAccents(estate.address.toLowerCase()).includes(removeAccents(objFind.address.toLowerCase())) ||
          objFind.address == '') &&
        (estate.type == objFind.type || objFind.type == '') &&
        (estate.request == objFind.request || objFind.request == '')
      )
    })
    // console.log(gEstateFound)
    addEstateToHtml(gEstateFound)
    pageEstate()
  }

  // sắp sếp khi chọn
   function onSelectSortChange(e) {
    // console.log(gEstateFound)
    if (gEstateFound.length > 0) {
      sortEstate(this.value, gEstateFound)
      addEstateToHtml(gEstateFound)
      pageEstate()
    } else {
      sortEstate(this.value, gEstate)
      addEstateToHtml(gEstate)
      pageEstate()
    }
  }

  // sắp xếp
  function sortEstate(paramValue, paramObj) {
    switch (paramValue) {
      case 'giatang':
        paramObj.sort(function (a, b) {
          return a.price - b.price
        })
        break
      case 'giagiam':
        paramObj.sort(function (a, b) {
          return b.price - a.price
        })
        break
      case 'moi':
        paramObj.sort(function (a, b) {
          return b.id - a.id
        })
        break
      case 'cu':
        paramObj.sort(function (a, b) {
          return a.id - b.id
        })
        break

      default:
        paramObj.sort(function (a, b) {
          return b.id - a.id
        })
        break
    }
  }

  // bỏ dâu tiếng việt
  function removeAccents(str) {
    var AccentsMap = [
      'aàảãáạăằẳẵắặâầẩẫấậ',
      'AÀẢÃÁẠĂẰẲẴẮẶÂẦẨẪẤẬ',
      'dđ',
      'DĐ',
      'eèẻẽéẹêềểễếệ',
      'EÈẺẼÉẸÊỀỂỄẾỆ',
      'iìỉĩíị',
      'IÌỈĨÍỊ',
      'oòỏõóọôồổỗốộơờởỡớợ',
      'OÒỎÕÓỌÔỒỔỖỐỘƠỜỞỠỚỢ',
      'uùủũúụưừửữứự',
      'UÙỦŨÚỤƯỪỬỮỨỰ',
      'yỳỷỹýỵ',
      'YỲỶỸÝỴ',
    ]
    for (var i = 0; i < AccentsMap.length; i++) {
      var re = new RegExp('[' + AccentsMap[i].substr(1) + ']', 'g')
      var char = AccentsMap[i][0]
      str = str.replace(re, char)
    }
    return str
  }

  // paging
  function pageEstate() {
    var items = $('.all-estate-item')
    var numItems = items.length
    var perPage = 9
    items.slice(perPage).hide()

    $('#pagination-container').pagination({
      items: numItems,
      itemsOnPage: perPage,
      hrefTextPrefix: '#',
      prevText: '&laquo;',
      edges: 1,
      displayedPages: 3,
      nextText: '&raquo;',
      onPageClick: function (pageNumber) {
        var showFrom = perPage * (pageNumber - 1)
        var showTo = showFrom + perPage
        items.hide().slice(showFrom, showTo).show()
      },
    })
    // $('#pagination-container').pagination(
    // 'selectPage',
    // window.location.hash.substring(1) ? window.location.hash.substring(1) : 1
    // )
  }
})
