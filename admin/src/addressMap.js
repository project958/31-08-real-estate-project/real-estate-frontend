'use strict'
$(document).ready(function () {
  /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
  const gAPI_URL = 'http://localhost:8080/'

  let gData = {
    address: '',
    lat: '',
    lng: '',
  }
  let gDataId = ''

  let gStt = 0
  let gDataTable = $('#data-table').DataTable({
    columns: [{ data: 'id' }, { data: 'address' }, { data: 'lat' }, { data: 'lng' }],
    columnDefs: [
      {
        targets: 0,
        render: renderStt,
      },
      {
        targets: 4,
        defaultContent: `<i id="btn-edit" class="fas fa-edit text-primary" title="Edit"></i> | <i id="btn-delete" class="fas fa-trash text-danger" title="Delete"></i>`,
      },
    ],
    // dom: 'Bfrtip',
    // buttons: ['copy', 'csv', 'excel', 'pdf', 'print', 'colvis'],
    lengthMenu: [
      [10, 25, 50, -1],
      [10, 25, 50, 'All'],
    ],
    autoWidth: false,
  })

  // authorize
  const token = getCookie('token')
  // console.log('token', token)
  // Khai báo xác thực ở headers
  var headers = {
    Authorization: 'Token ' + token,
  }

  /*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */

  onPageLoading()
  // gán sự kiện nút create
  $('#create-data').click(onCreateClick)
  // gán sự kiện nút edit
  $('#data-table').on('click', '#btn-edit', onEditBtnClick)
  // gán sự kiện nút save update
  $('#btn-save').click(onSaveUpdateClick)
  // gán sự kiện nút delete
  $('#data-table').on('click', '#btn-delete', onDeleteClick)
  // gán sự kiện nút confirm delete
  $(document).on('click', '#delete-data', onConfirmDeleteClick)
  // gán sự kiện cho nút logout
  $('#btn-logout').on('click', redirectToLogin)

  /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */

  function onPageLoading() {
    getAllDataToTable()
  }

  /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/

  // load data to table
  function getAllDataToTable() {
    $.ajax({
      type: 'get',
      url: gAPI_URL + 'address-maps',
      headers: headers,
      dataType: 'json',
      success: function (response) {
        loadToDataTable(response)
      },
    })
  }

  // load data to table
  function loadToDataTable(paramData) {
    gStt = 0
    // console.log(paramData)
    gDataTable.clear()
    gDataTable.rows.add(paramData)
    gDataTable.draw()
  }

  // render stauts
  function renderStt(params) {
    gStt++
    return gStt
  }

  // create new data
  function onCreateClick(e) {
    e.preventDefault()
    //get data
    getDataInput(gData)
    // console.log(gData)
    //validate data
    if ($('#create-form').valid()) {
      // if (confirm('Xác nhận tạo mới?')) {
      Swal.fire({
        title: 'Vui lòng xác nhận?',
        position: 'top',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Xác nhận',
      }).then((result) => {
        if (result.isConfirmed) {
          $.ajax({
            type: 'post',
            headers: headers,
            url: gAPI_URL + 'address-map',
            data: JSON.stringify(gData),
            contentType: 'application/json; charset=utf-8',
            success: function (respond) {
              // alert(`Tạo mới thành công`)
              Swal.fire({
                position: 'top',
                icon: 'success',
                title: 'Tạo mới thành công',
                showConfirmButton: false,
                timer: 1500,
              })

              // console.log('respond', respond)
              $('#modal-create').modal('hide')
              resetDataForm()
              getAllDataToTable()
            },
            error: function (error) {
              console.log(error)
              // alert(error.statusText)
            },
          })
        }
      })
    }
  }

  // update article
  function onEditBtnClick(e) {
    let vSelectedRow = $(this).parents('tr')
    let vSelectedData = gDataTable.row(vSelectedRow).data()
    gDataId = vSelectedData.id
    // console.log('gDataId', gDataId)
    $.ajax({
      type: 'get',
      headers: headers,
      async: false,
      url: gAPI_URL + `address-map/${gDataId}`,
      dataType: 'json',
      success: function (response) {
        loadDataToInputUpdate(response)
      },
    })
    $('#modal-update').modal('show')
  }

  // call api update
  function onSaveUpdateClick(e) {
    getDataInputUpdate(gData)
    if ($('#update-form').valid()) {
      // if (confirm('Xác nhận cập nhật?')) {
      Swal.fire({
        title: 'Vui lòng xác nhận?',
        position: 'top',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Xác nhận',
      }).then((result) => {
        if (result.isConfirmed) {
          // console.log('gData', gData)
          // call api update
          $.ajax({
            type: 'put',
            headers: headers,
            url: gAPI_URL + 'address-map/' + gDataId,
            data: JSON.stringify(gData),
            async: false,
            contentType: 'application/json; charset=utf-8',
            success: function (response) {
              // console.log(response)
              // alert('Cập nhật thành công')
              Swal.fire({
                position: 'top',
                icon: 'success',
                title: 'Cập nhật thành công',
                showConfirmButton: false,
                timer: 1500,
              })
              $('#modal-update').modal('hide')
              getAllDataToTable()
            },
            error: function (error) {
              // console.log(error.responseText)
            },
          })
        }
      })
    }
    // }
  }

  // delete article
  function onDeleteClick(e) {
    $('#modal-delete-data').modal('show')
    let vSelectedRow = $(this).parents('tr')
    let vSelectedData = gDataTable.row(vSelectedRow).data()
    gDataId = vSelectedData.id
  }

  // confirm delete data
  function onConfirmDeleteClick() {
    $.ajax({
      url: gAPI_URL + `address-map/${gDataId}`,
      method: 'DELETE',
      headers: headers,
      success: () => {
        // alert(`Xóa thành công`)
        Swal.fire({
          position: 'top',
          icon: 'success',
          title: 'Xóa thành công',
          showConfirmButton: false,
          timer: 1500,
        })
        $('#modal-delete-data').modal('hide')
        getAllDataToTable()
      },
      // error: (err) => console.log(err.responseText),
    })
  }

  // get data input
  function getDataInput(paramObj) {
    paramObj.address = $('#input-address-map').val().trim()
    paramObj.lat = $('#input-lat').val().trim()
    paramObj.lng = $('#input-lng').val().trim()
  }

  // get data input update
  function getDataInputUpdate(paramObj) {
    paramObj.address = $('#input-address-map-update').val().trim()
    paramObj.lat = $('#input-lat-update').val().trim()
    paramObj.lng = $('#input-lng-update').val().trim()
  }

  // load data input update
  function loadDataToInputUpdate(paramObj) {
    $('#input-address-map-update').val(paramObj.address)
    $('#input-lat-update').val(paramObj.lat)
    $('#input-lng-update').val(paramObj.lng)
  }

  // reset form
  function resetDataForm() {
    $('#input-address-map').val('')
    $('#input-lat').val('')
    $('#input-lng').val('')
  }

  // validate data
  $('#create-form').validate({
    rules: {
      address: 'required',
      lat: {
        number: true,
        required: true,
      },
      lng: {
        number: true,
        required: true,
      },
    },
    messages: {
      address: 'Vui lòng nhập địa chỉ',
      lat: {
        number: 'Vui lòng nhập số',
        required: 'Vui lòng nhập vĩ độ',
      },
      lng: {
        number: 'Vui lòng nhập số',
        required: 'Vui lòng nhập vĩ độ',
      },
    },
  })

  // validate data update
  $('#update-form').validate({
    rules: {
      address: 'required',
      lat: {
        number: true,
        required: true,
      },
      lng: {
        number: true,
        required: true,
      },
    },
    messages: {
      address: 'Vui lòng nhập địa chỉ',
      lat: {
        number: 'Vui lòng nhập số',
        required: 'Vui lòng nhập vĩ độ',
      },
      lng: {
        number: 'Vui lòng nhập số',
        required: 'Vui lòng nhập vĩ độ',
      },
    },
  })

  $('#cancel-data').click(function (e) {
    e.preventDefault()
    resetDataForm()
    // console.log('cancel')
  })

  $('#cancel-data-update').click(function (e) {
    e.preventDefault()
    $('#modal-update').modal('hide')
  })

  function redirectToLogin() {
    // Trước khi logout cần xóa token đã lưu trong cookie
    setCookie('token', '', 1)
    window.location.href = 'login.html'
  }

  //Hàm get Cookie đã giới thiệu ở bài trước
  function getCookie(cname) {
    var name = cname + '='
    var decodedCookie = decodeURIComponent(document.cookie)
    var ca = decodedCookie.split(';')
    for (var i = 0; i < ca.length; i++) {
      var c = ca[i]
      while (c.charAt(0) == ' ') {
        c = c.substring(1)
      }
      if (c.indexOf(name) == 0) {
        return c.substring(name.length, c.length)
      }
    }
    return ''
  }

  //Hàm setCookie đã giới thiệu ở bài trước
  function setCookie(cname, cvalue, exdays) {
    var d = new Date()
    d.setTime(d.getTime() + exdays * 24 * 60 * 60 * 1000)
    var expires = 'expires=' + d.toUTCString()
    document.cookie = cname + '=' + cvalue + ';' + expires + ';path=/'
  }
})
