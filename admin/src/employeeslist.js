'use strict'
$(document).ready(function () {
  /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
  const gAPI_URL = 'http://localhost:8080/'
  // Authorization
  const token = getCookie('token')
  // console.log('token', token)
  // Khai báo xác thực ở headers
  var headers = {
    Authorization: 'Token ' + token,
  }

  let gStt = 0

  let gDataTable = $('#data-table').DataTable({
    columns: [
      { data: 'id' },
      { data: 'lastName' },
      { data: 'firstName' },
      { data: 'title' },
      { data: 'titleOfCourtesy' },
      { data: 'birthDate' },
      { data: 'hireDate' },
      { data: 'address' },
      { data: 'city' },
      { data: 'region' },
      { data: 'postalCode' },
      { data: 'country' },
      { data: 'homePhone' },
      { data: 'extension' },
      { data: 'photo' },
      { data: 'reportsTo' },
      { data: 'username' },
      { data: 'email' },
      { data: 'activated' },
      { data: 'userLevel' },
    ],
    columnDefs: [
      {
        targets: 20,
        defaultContent: `<i id="btn-edit" class="fas fa-edit text-primary" title="Edit"></i> | <i id="btn-delete" class="fas fa-trash text-danger" title="Delete"></i>`,
      },
      // {
      //   targets: 0,
      //   render: renderStt,
      // },
      {
        targets: 18,
        render: renderCheckbox,
      },
      {
        targets: 19,
        render: renderUserLevel,
      },
    ],
    // dom: 'Bfrtip',
    // buttons: ['copy', 'csv', 'excel', 'pdf', 'print', 'colvis'],
    lengthMenu: [
      [10, 25, 50, -1],
      [10, 25, 50, 'All'],
    ],
    autoWidth: false,
    // scrollX: true,
  })

  // load data to table
  function loadToDataTable(paramData) {
    gStt = 0
    // console.log(paramData)
    gDataTable.clear()
    gDataTable.rows.add(paramData)
    gDataTable.draw()
  }

  // crud
  let gData = {
    lastName: '',
    firstName: '',
    title: '',
    titleOfCourtesy: '',
    birthDate: '',
    hireDate: '',
    address: '',
    city: '',
    region: '',
    postalCode: '',
    country: '',
    homePhone: '',
    extension: '',
    photo: '',
    reportsTo: '',
    username: '',
    password: '',
    email: '',
    activated: '',
    userLevel: '',
  }
  let gDataId = ''

  /*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */

  onPageLoading()
  // gán sự kiện nút create
  $('#create-data').click(onCreateClick)
  // gán sự kiện nút edit
  $('#data-table').on('click', '#btn-edit', onEditBtnClick)
  // gán sự kiện nút save update
  $('#btn-save').click(onSaveUpdateClick)
  // gán sự kiện nút delete
  $('#data-table').on('click', '#btn-delete', onDeleteClick)
  // gán sự kiện nút confirm delete
  $(document).on('click', '#delete-data', onConfirmDeleteClick)
  // gán sự kiện nút chi tiết
  $('#data-table').on('click', '#btn-detail', onBtnDetailClick)

  /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */

  function onPageLoading() {
    getAllDataToTable()
  }

  /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/

  //Hàm get Cookie đã giới thiệu ở bài trước
  function getCookie(cname) {
    var name = cname + '='
    var decodedCookie = decodeURIComponent(document.cookie)
    var ca = decodedCookie.split(';')
    for (var i = 0; i < ca.length; i++) {
      var c = ca[i]
      while (c.charAt(0) == ' ') {
        c = c.substring(1)
      }
      if (c.indexOf(name) == 0) {
        return c.substring(name.length, c.length)
      }
    }
    return ''
  }

  // get all data
  function getAllDataToTable() {
    $.ajax({
      type: 'get',
      url: gAPI_URL + 'employees',
      headers: headers,
      dataType: 'json',
      success: function (response) {
        loadToDataTable(response)
      },
    })
  }

  // render stauts
  function renderStt(params) {
    gStt++
    return gStt
  }

  // render checkbox
  function renderCheckbox(params) {
    switch (params) {
      case 'Y':
        return `<input type="checkbox" checked disabled/>`
        break
      case 'N':
        return `<input type="checkbox" disabled/>`
        break

      default:
        break
    }
  }

  // render user level
  function renderUserLevel(params) {
    switch (params) {
      case 1:
        return 'ADMIN'
        break
      case 2:
        return 'CUSTOMER'
        break
      case 3:
        return 'HOMESELLER'
        break

      default:
        break
    }
  }

  // handle btn detail article
  function onBtnDetailClick(e) {
    let vSelectedRow = $(this).parents('tr')
    let vSelectedData = gDataTable.row(vSelectedRow).data()
    gDataId = vSelectedData.id
    // console.log('vSelectedData', vSelectedData)
    // console.log('gDataId', gDataId)
    $.get(gAPI_URL + `employee/${gDataId}`, loadDataToInput)
    // $('#modal-detail').modal('show')
    // hiển thị ra console
    var vOrderUrl = './realestateList.html?' + 'id=' + gDataId
    window.location.href = vOrderUrl
  }

  // create
  function onCreateClick(e) {
    //get data
    e.preventDefault()
    getDataInput(gData)
    // console.log(gData)
    if ($('#create-form').valid()) {
      Swal.fire({
        title: 'Vui lòng xác nhận?',
        position: 'top',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Xác nhận',
      }).then((result) => {
        if (result.isConfirmed) {
        $.ajax({
          type: 'post',
          url: gAPI_URL + 'employee',
          headers: headers,
          data: JSON.stringify(gData),
          contentType: 'application/json; charset=utf-8',
          success: function (respond) {
            Swal.fire({
                position: 'top',
                icon: 'success',
                title: 'Tạo mới thành công',
                showConfirmButton: false,
                timer: 1500,
              })
            // console.log('respond', respond)
            $('#modal-create').modal('hide')
            resetDataForm()
            getAllDataToTable()
          },
          error: function (error) {
            // alert(error.statusText)
            // console.log(error.responseText)
            },
          })
        }
      })
    }
  }

  // update article
  function onEditBtnClick(e) {
    let vSelectedRow = $(this).parents('tr')
    let vSelectedData = gDataTable.row(vSelectedRow).data()
    gDataId = vSelectedData.id
    // console.log('vSelectedData', vSelectedData)
    // console.log('gDataId', gDataId)
    $.ajax({
      type: 'get',
      headers: headers,
      async: false,
      url: gAPI_URL + `employee/${gDataId}`,
      dataType: 'json',
      success: function (response) {
        loadDataToInput(response)
      },
    })
    $('#modal-update').modal('show')
  }

  // call api update
  function onSaveUpdateClick(e) {
    getDataInputUpdate(gData)
    // console.log(gData)
    if ($('#update-form').valid()) {
      Swal.fire({
        title: 'Vui lòng xác nhận?',
        position: 'top',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Xác nhận',
      }).then((result) => {
        if (result.isConfirmed) {
        // console.log('gData', gData)
        // call api update
        $.ajax({
          type: 'put',
          url: gAPI_URL + 'employee/' + gDataId,
          headers: headers,
          data: JSON.stringify(gData),
          async: false,
          contentType: 'application/json; charset=utf-8',
          success: function (response) {
            // console.log(response)
            Swal.fire({
                position: 'top',
                icon: 'success',
                title: 'Cập nhật thành công',
                showConfirmButton: false,
                timer: 1500,
              })
            $('#modal-update').modal('hide')
            getAllDataToTable()
          },
          error: function (error) {
            // console.log(error.responseText)
            },
          })
        }
      })
    }
  }

  // delete article
  function onDeleteClick(e) {
    $('#modal-delete-data').modal('show')
    let vSelectedRow = $(this).parents('tr')
    let vSelectedData = gDataTable.row(vSelectedRow).data()
    gDataId = vSelectedData.id
  }

  // confirm delete data
  function onConfirmDeleteClick() {
    $.ajax({
      url: gAPI_URL + `employee/${gDataId}`,
      method: 'DELETE',
      headers: headers,
      success: () => {
       Swal.fire({
          position: 'top',
          icon: 'success',
          title: 'Xóa thành công',
          showConfirmButton: false,
          timer: 1500,
        })
        $('#modal-delete-data').modal('hide')
        getAllDataToTable()
      },
      error: (err) => {
        console.log(err), alert(err.statusText)
      },
    })
  }

  function formatDate(param) {
    param = new Date()
    return param.getDate() + '-' + (param.getMonth() + 1) + '-' + param.getFullYear()
  }

  function formatDateUpdate(param) {
    if (param) {
      const [dayC, monthC, yearC] = param.split('-')
      return `${yearC}-${monthC}-${dayC}`
    }
  }

  // get data input
  function getDataInput(paramObj) {
    paramObj.lastName = $('#input-lastName').val().trim()
    paramObj.firstName = $('#input-firstName').val().trim()
    paramObj.title = $('#input-title').val().trim()
    paramObj.titleOfCourtesy = $('#input-titleOfCourtesy').val().trim()
    paramObj.titleOfCourtesy = $('#input-titleOfCourtesy').val().trim()
    paramObj.birthDate = formatDate($('#input-birthDate').val().trim())
    paramObj.hireDate = formatDate($('#input-hireDate').val().trim())
    paramObj.address = $('#input-address').val().trim()
    paramObj.city = $('#input-city').val().trim()
    paramObj.region = $('#input-region').val().trim()
    paramObj.postalCode = $('#input-postalCode').val().trim()
    paramObj.country = $('#input-country').val().trim()
    paramObj.homePhone = $('#input-homePhone').val().trim()
    paramObj.extension = $('#input-extension').val().trim()
    paramObj.photo = $('#input-photo').val().trim()
    paramObj.reportsTo = $('#input-reportsTo').val().trim()
    paramObj.username = $('#input-username').val().trim()
    paramObj.password = $('#input-password').val().trim()
    paramObj.email = $('#input-email').val().trim()
    let checkboxValue = $('#input-activated').is(':checked')
    if (checkboxValue) {
      paramObj.activated = 'Y'
    } else {
      paramObj.activated = 'N'
    }
    paramObj.userLevel = $('#select-userLevel').val()
  }

  // get data input update
  function getDataInputUpdate(paramObj) {
    paramObj.lastName = $('#input-lastName-update').val().trim()
    paramObj.firstName = $('#input-firstName-update').val().trim()
    paramObj.title = $('#input-title-update').val().trim()
    paramObj.titleOfCourtesy = $('#input-titleOfCourtesy-update').val().trim()
    paramObj.titleOfCourtesy = $('#input-titleOfCourtesy-update').val().trim()
    paramObj.birthDate = formatDateUpdate($('#input-birthDate-update').val().trim())
    paramObj.hireDate = formatDateUpdate($('#input-hireDate-update').val().trim())
    paramObj.address = $('#input-address-update').val().trim()
    paramObj.city = $('#input-city-update').val().trim()
    paramObj.region = $('#input-region-update').val().trim()
    paramObj.postalCode = $('#input-postalCode-update').val().trim()
    paramObj.country = $('#input-country-update').val().trim()
    paramObj.homePhone = $('#input-homePhone-update').val().trim()
    paramObj.extension = $('#input-extension-update').val().trim()
    paramObj.photo = $('#input-photo-update').val().trim()
    paramObj.reportsTo = $('#input-reportsTo-update').val().trim()
    paramObj.username = $('#input-username-update').val().trim()
    paramObj.password = $('#input-password-update').val().trim()
    paramObj.email = $('#input-email-update').val().trim()
    let checkboxValue = $('#input-activated-update').is(':checked')
    if (checkboxValue) {
      paramObj.activated = 'Y'
    } else {
      paramObj.activated = 'N'
    }
    paramObj.userLevel = $('#select-userLevel-update').val()
  }

  // load data input update
  function loadDataToInput(paramObj) {
    $('#input-lastName-update').val(paramObj.lastName)
    $('#input-firstName-update').val(paramObj.firstName)
    $('#input-title-update').val(paramObj.title)
    $('#input-titleOfCourtesy-update').val(paramObj.titleOfCourtesy)
    $('#input-titleOfCourtesy-update').val(paramObj.titleOfCourtesy)
    $('#input-birthDate-update').val(formatDateUpdate(paramObj.birthDate))
    $('#input-hireDate-update').val(formatDateUpdate(paramObj.hireDate))
    $('#input-address-update').val(paramObj.address)
    $('#input-city-update').val(paramObj.city)
    $('#input-region-update').val(paramObj.region)
    $('#input-postalCode-update').val(paramObj.postalCode)
    $('#input-country-update').val(paramObj.country)
    $('#input-homePhone-update').val(paramObj.homePhone)
    $('#input-extension-update').val(paramObj.extension)
    $('#input-photo-update').val(paramObj.photo)
    $('#input-reportsTo-update').val(paramObj.reportsTo)
    $('#input-username-update').val(paramObj.username)
    $('#input-password-update').val(paramObj.password)
    $('#input-email-update').val(paramObj.email)
    if (paramObj.activated == 'Y') {
      $('#input-activated-update').attr('checked', 'checked')
    } else {
      $('#input-activated-update').attr('checked', false)
    }
    $('#select-userLevel-update').val(paramObj.userLevel)
  }

  // reset form
  function resetDataForm() {
    $('#input-lastName').val('')
    $('#input-firstName').val('')
    $('#input-title').val('')
    $('#input-titleOfCourtesy').val('')
    $('#input-titleOfCourtesy').val('')
    $('#input-birthDate').val('')
    $('#input-hireDate').val('')
    $('#input-address').val('')
    $('#input-city').val('')
    $('#input-region').val('')
    $('#input-postalCode').val('')
    $('#input-country').val('')
    $('#input-homePhone').val('')
    $('#input-extension').val('')
    $('#input-photo').val('')
    $('#input-reportsTo').val('')
    $('#input-username').val('')
    $('#input-password').val('')
    $('#input-email').val('')
    $('#input-activated').val('')
    $('#select-userLevel').val('')
  }

  // validate data
  $('#create-form').validate({
    rules: {
      firstName: 'required',
      lastName: 'required',
      username: 'required',
      password: 'required',
      email: 'required',
      userlevel: 'required',
    },
    messages: {
      firstName: 'Vui lòng nhập firstName',
      lastName: 'Vui lòng nhập lastName',
      username: 'Vui lòng nhập username',
      password: 'Vui lòng nhập password',
      email: 'Vui lòng nhập email',
      userlevel: 'Vui lòng chọn level',
    },
  })
  // validate data update
  $('#update-form').validate({
    rules: {
      firstName: 'required',
      lastName: 'required',
      username: 'required',
      password: 'required',
      email: 'required',
      userlevel: 'required',
    },
    messages: {
      firstName: 'Vui lòng nhập firstName',
      lastName: 'Vui lòng nhập lastName',
      username: 'Vui lòng nhập username',
      password: 'Vui lòng nhập password',
      email: 'Vui lòng nhập email',
      userlevel: 'Vui lòng chọn level',
    },
  })

  $('#cancel-data').click(function (e) {
    e.preventDefault()
    resetDataForm()
    // console.log('cancel')
  })

  $('#cancel-data-update').click(function (e) {
    e.preventDefault()
    $('#modal-update').modal('hide')
  })
})
