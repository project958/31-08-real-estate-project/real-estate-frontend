'use strict'
$(document).ready(function () {
  /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
  const gAPI_URL = 'http://localhost:8080/'
  
  // Authorization
  const token = getCookie('token')
  // console.log('token', token)

  // Khai báo xác thực ở headers
  var headers = {
    Authorization: 'Token ' + token,
  }

  let gDataTable = $('#data-table').DataTable({
    columns: [{ data: 'id' }, { data: 'prefix' }, { data: 'name' }, { data: 'districtName' }, { data: 'province' }],
    columnDefs: [
      {
        targets: 5,
        defaultContent: `<i id="btn-edit" class="fas fa-edit text-primary" title="Edit"></i> | <i id="btn-delete" class="fas fa-trash text-danger" title="Delete"></i>`,
      },
      {
        targets: 0,
        render: renderStt,
      },
      {
        targets: 4,
        render: renderProvinceName,
      },
    ],
    // dom: 'Bfrtip',
    // buttons: ['copy', 'csv', 'excel', 'pdf', 'print', 'colvis'],
    lengthMenu: [
      [10, 25, 50, -1],
      [10, 25, 50, 'All'],
    ],
    autoWidth: false,
    // scrollX: true,
  })

  let gStt = 0
  let gDistrictId = ''
  let gProvinceId = ''
  let gProvince = []
  // crud
  let gData = {
    name: '',
    prefix: '',
    province: '',
  }
  let gDataId = ''

  /*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */

  onPageLoading()
  // gán sự kiện nút create
  $('#create-data').click(onCreateClick)
  // gán sự kiện nút edit
  $('#data-table').on('click', '#btn-edit', onEditBtnClick)
  // gán sự kiện nút save update
  $('#btn-save').click(onSaveUpdateClick)
  // gán sự kiện nút delete
  $('#data-table').on('click', '#btn-delete', onDeleteClick)
  // gán sự kiện nút confirm delete
  $(document).on('click', '#delete-data', onConfirmDeleteClick)

  /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */

  function onPageLoading() {
    getAllDataToTable()
    getAllProvince()
  }

  /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/

  // get all province
  function getAllProvince() {
    $.ajax({
      type: 'get',
      url: gAPI_URL + 'provinces',
      dataType: 'json',
      headers: headers,
      async: false,
      success: function (response) {
        gProvince = response
        for (const province of response) {
          $('#select-province').append(
            $('<option>', {
              value: province.id,
              text: province.name,
            })
          )
          $('#select-province-update').append(
            $('<option>', {
              value: province.id,
              text: province.name,
            })
          )
        }
        // console.log('gProvince', gProvince)
      },
    })
  }

  //Hàm get Cookie đã giới thiệu ở bài trước
  function getCookie(cname) {
    var name = cname + '='
    var decodedCookie = decodeURIComponent(document.cookie)
    var ca = decodedCookie.split(';')
    for (var i = 0; i < ca.length; i++) {
      var c = ca[i]
      while (c.charAt(0) == ' ') {
        c = c.substring(1)
      }
      if (c.indexOf(name) == 0) {
        return c.substring(name.length, c.length)
      }
    }
    return ''
  }

  // get all data
  function getAllDataToTable() {
    $.ajax({
      type: 'get',
      url: gAPI_URL + 'wards',
      headers: headers,
      dataType: 'json',
      success: function (response) {
        loadToDataTable(response)
      },
    })
  }

  // render stauts
  function renderStt(params) {
    gStt++
    return gStt
  }

  // render province name
  function renderProvinceName(paramId) {
    for (var bI = 0; bI < gProvince.length; bI++) {
      if (gProvince[bI].id == paramId) {
        return gProvince[bI].name
      }
    }
  }

  // handle province select change
  $('#select-province').change(function (e) {
    gProvinceId = $('#select-province').val()
    $('#select-district').html('')
    $('#select-district').append(
      $('<option>', {
        value: '',
        text: 'Quận huyện',
      })
    )
    if (gProvinceId) {
      $.ajax({
        type: 'get',
        url: gAPI_URL + 'province/' + gProvinceId + '/districts',
        headers: headers,
        dataType: 'json',
        success: function (response) {
          // console.log('district', response)
          for (const district of response) {
            $('#select-district').append(
              $('<option>', {
                value: district.id,
                text: district.prefix + ' ' + district.name,
              })
            )
          }
        },
      })
    }
  })

  // district change
  $('#select-district').change(function (e) {
    gDistrictId = $('#select-district').val()
  })

  // load data to table
  function loadToDataTable(paramData) {
    gStt = 0
    // console.log(paramData)
    gDataTable.clear()
    gDataTable.rows.add(paramData)
    gDataTable.draw()
  }

  // create
  function onCreateClick(e) {
    //get data
    e.preventDefault()
    getDataInput(gData)
    // console.log(gData)
    if ($('#create-form').valid()) {
      Swal.fire({
        title: 'Vui lòng xác nhận?',
        position: 'top',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Xác nhận',
      }).then((result) => {
        if (result.isConfirmed) {
        $.ajax({
          type: 'post',
          url: gAPI_URL + `district/${gDistrictId}/ward`,
          headers: headers,
          data: JSON.stringify(gData),
          contentType: 'application/json; charset=utf-8',
          success: function (respond) {
            Swal.fire({
                position: 'top',
                icon: 'success',
                title: 'Tạo mới thành công',
                showConfirmButton: false,
                timer: 1500,
              })
            // console.log('respond', respond)
            $('#modal-create').modal('hide')
            resetDataForm()
            getAllDataToTable()
          },
          error: function (error) {
            // alert(error.statusText)
            // console.log(error.responseText)
            },
          })
        }
      })
    }
  }

  // update article
  function onEditBtnClick(e) {
    let vSelectedRow = $(this).parents('tr')
    let vSelectedData = gDataTable.row(vSelectedRow).data()
    gDataId = vSelectedData.id
    // console.log('vSelectedData', vSelectedData)
    // console.log('gDataId', gDataId)
    $.ajax({
      type: 'get',
      headers: headers,
      async: false,
      url: gAPI_URL + `ward/${gDataId}`,
      dataType: 'json',
      success: function (response) {
        loadDataToInput(response)
      },
    })
    $('#modal-update').modal('show')
  }

  // call api update
  function onSaveUpdateClick(e) {
    getDataInputUpdate(gData)
    if ($('#update-form').valid()) {
      Swal.fire({
        title: 'Vui lòng xác nhận?',
        position: 'top',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Xác nhận',
      }).then((result) => {
        if (result.isConfirmed) {
        // console.log('gData', gData)
        // call api update
        $.ajax({
          type: 'put',
          url: gAPI_URL + 'ward/' + gDataId,
          data: JSON.stringify(gData),
          headers: headers,
          async: false,
          contentType: 'application/json; charset=utf-8',
          success: function (response) {
            // console.log(response)
            Swal.fire({
                position: 'top',
                icon: 'success',
                title: 'Cập nhật thành công',
                showConfirmButton: false,
                timer: 1500,
              })
            $('#modal-update').modal('hide')
            getAllDataToTable()
          },
          error: function (error) {
            // console.log(error.responseText)
            },
          })
        }
      })
    }
  }

  // delete article
  function onDeleteClick(e) {
    $('#modal-delete-data').modal('show')
    let vSelectedRow = $(this).parents('tr')
    let vSelectedData = gDataTable.row(vSelectedRow).data()
    gDataId = vSelectedData.id
  }

  // confirm delete data
  function onConfirmDeleteClick() {
    $.ajax({
      url: gAPI_URL + `ward/${gDataId}`,
      headers: headers,
      method: 'DELETE',
      success: () => {
       Swal.fire({
          position: 'top',
          icon: 'success',
          title: 'Xóa thành công',
          showConfirmButton: false,
          timer: 1500,
        })
        $('#modal-delete-data').modal('hide')
        $.get(gAPI_URL + `wards`, loadToDataTable)
      },
      // error: (err) => console.log(err.responseText),
    })
  }

  // get data input
  function getDataInput(paramObj) {
    paramObj.name = $('#input-name').val().trim()
    paramObj.prefix = $('#select-prefix').val().trim()
    paramObj.province = $('#select-province').val().trim()
  }
  // get data input update
  function getDataInputUpdate(paramObj) {
    paramObj.name = $('#input-name-update').val().trim()
    paramObj.prefix = $('#select-prefix-update').val().trim()
  }

  // load data input update
  function loadDataToInput(paramObj) {
    $('#input-name-update').val(paramObj.name)
    $('#select-prefix-update').val(paramObj.prefix)
  }

  // reset form
  function resetDataForm() {
    $('#input-name').val('')
    $('#select-prefix').val('')
    $('#select-province').val('')
  }

  // validate data
  $('#create-form').validate({
    rules: {
      name: 'required',
      district: 'required',
    },
    messages: {
      name: 'Vui lòng nhập tên',
      district: 'Vui lòng chọn quận huyện',
    },
  })
  // validate data update
  $('#update-form').validate({
    rules: {
      name: 'required',
      district: 'required',
    },
    messages: {
      name: 'Vui lòng nhập tên',
      district: 'Vui lòng chọn quận huyện',
    },
  })

  $('#cancel-data').click(function (e) {
    e.preventDefault()
    resetDataForm()
    // console.log('cancel')
  })

  $('#cancel-data-update').click(function (e) {
    e.preventDefault()
    $('#modal-update').modal('hide')
  })
})
