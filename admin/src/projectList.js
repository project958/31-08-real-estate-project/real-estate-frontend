'use strict'
$(document).ready(function () {
  /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
  const gAPI_URL = 'http://localhost:8080/'
  
  let gProvince = []
  let gDistrict = []
  let gProvinceById = []
  let gDistrictById = []

  let gStt = 0
  let gProvinceId = ''
  let gProvinceIdUpdate = ''

  // Authorization

  const token = getCookie('token')
  // console.log('token', token)

  // Khai báo xác thực ở headers
  var headers = {
    Authorization: 'Token ' + token,
  }

  let gDataTable = $('#data-table').DataTable({
    columns: [
      { data: 'id' },
      { data: 'name' },
      { data: 'provinceId' },
      { data: 'districtId' },
      { data: 'lat' },
      { data: 'lng' },
      { data: 'acreage' },
      { data: 'constructArea' },
      { data: 'numBlock' },
      { data: 'numApartment' },
      { data: 'investor' },
      { data: 'constructionContractor' },
      { data: 'designUnit' },
    ],
    columnDefs: [
      {
        targets: 13,
        defaultContent: `<i id="btn-edit" class="fas fa-edit text-primary" title="Edit"></i> | <i id="btn-delete" class="fas fa-trash text-danger" title="Delete"></i>`,
      },
      {
        targets: 0,
        render: renderStt,
      },
      {
        targets: 2,
        render: getProvinceNameById,
      },
      {
        targets: 3,
        render: getDistrictNameById,
      },
      {
        targets: 10,
        render: getConstructNameById,
      },
      {
        targets: 11,
        render: getInvestorNameById,
      },
      {
        targets: 12,
        render: getDesignNameById,
      },
    ],
    order: [[0, 'desc']],
    // dom: 'Bfrtip',
    // buttons: ['copy', 'csv', 'excel', 'pdf', 'print', 'colvis'],
    lengthMenu: [
      [10, 25, 50, -1],
      [10, 25, 50, 'All'],
    ],
    autoWidth: false,
    // scrollX: true,
  })

  let gUrlString = new URL(window.location.href)
  let gCustomerId = gUrlString.searchParams.get('customerId')
  let gProvinceUrlId = gUrlString.searchParams.get('provinceId')
  let gDistrictUrlId = gUrlString.searchParams.get('districtId')

  // crud
  let gData = {
    name: '',
    provinceId: '',
    districtId: '',
    wardId: null,
    streetId: null,
    address: '',
    slogan: '',
    description: null,
    acreage: '',
    constructArea: '',
    numBlock: '',
    numFloors: '',
    numApartment: '',
    apartmenttArea: '',
    investor: '',
    constructionContractor: null,
    designUnit: null,
    utilities: '',
    regionLink: '',
    photo: '',
    lat: '',
    lng: '',
  }
  let gDataId = ''

  /*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */

  onPageLoading()
  // gán sự kiện nút create
  $('#create-data').click(onCreateClick)
  // gán sự kiện nút edit
  $('#data-table').on('click', '#btn-edit', onEditBtnClick)
  // gán sự kiện nút save update
  $('#btn-save').click(onSaveUpdateClick)
  // gán sự kiện nút delete
  $('#data-table').on('click', '#btn-delete', onDeleteClick)
  // gán sự kiện nút confirm delete
  $(document).on('click', '#delete-data', onConfirmDeleteClick)
  // gán sự kiện chọn province
  $('#select-province').change(onChangeProvinceSelect)
  // gán sự kiện chọn province update
  $('#select-province-update').change(onChangeProvinceUpdate)
  // gán sự kiện nút detail
  $('#data-table').on('click', '#btn-detail', onBtnDetailClick)

  /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */

  function onPageLoading() {
    getAllDataToTable()
    getAllProvince()
    getAllDistrict()
    getAllInvestor()
    getAllConstructor()
    getAllDesign()
    loadToTableById()
  }

  /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/

  //Hàm get Cookie đã giới thiệu ở bài trước
  function getCookie(cname) {
    var name = cname + '='
    var decodedCookie = decodeURIComponent(document.cookie)
    var ca = decodedCookie.split(';')
    for (var i = 0; i < ca.length; i++) {
      var c = ca[i]
      while (c.charAt(0) == ' ') {
        c = c.substring(1)
      }
      if (c.indexOf(name) == 0) {
        return c.substring(name.length, c.length)
      }
    }
    return ''
  }

  function getAllDataToTable() {
    $.ajax({
      type: 'get',
      url: gAPI_URL + 'projects',
      headers: headers,
      dataType: 'json',
      success: function (response) {
        loadToDataTable(response)
      },
    })
  }

  // get all province
  function getAllProvince() {
    $.ajax({
      type: 'get',
      url: gAPI_URL + 'provinces',
      dataType: 'json',
      headers: headers,
      async: false,
      success: function (response) {
        gProvince = response
        for (const province of response) {
          $('#select-province').append(
            $('<option>', {
              value: province.id,
              text: province.name,
            })
          )
          $('#select-province-update').append(
            $('<option>', {
              value: province.id,
              text: province.name,
            })
          )
        }
        // console.log('gProvince', gProvince)
      },
    })
  }

  // get all district
  function getAllDistrict() {
    $.ajax({
      type: 'get',
      url: gAPI_URL + 'districts',
      dataType: 'json',
      headers: headers,
      async: false,
      success: function (response) {
        // console.log('project', response)
        gDistrict = response
        for (const district of response) {
          $('#select-district').append(
            $('<option>', {
              value: district.id,
              text: district.name,
            })
          )
          $('#select-district-update').append(
            $('<option>', {
              value: district.id,
              text: district.name,
            })
          )
        }
      },
    })
  }

  // get all investor
  function getAllInvestor() {
    $.ajax({
      type: 'get',
      url: gAPI_URL + 'investors',
      dataType: 'json',
      headers: headers,
      async: false,
      success: function (response) {
        // console.log("investor", response)
        for (const investor of response) {
          $('#select-investor').append(
            $('<option>', {
              value: investor.id,
              text: investor.name,
            })
          )
          $('#select-investor-update').append(
            $('<option>', {
              value: investor.id,
              text: investor.name,
            })
          )
        }
      },
    })
  }

  // get all constructionContractor
  function getAllConstructor() {
    $.ajax({
      type: 'get',
      url: gAPI_URL + 'construction-contractors',
      headers: headers,
      dataType: 'json',
      async: false,
      success: function (response) {
        // console.log("constructor", response)
        for (const constructor of response) {
          $('#select-constructionContractor').append(
            $('<option>', {
              value: constructor.id,
              text: constructor.name,
            })
          )
          $('#select-constructionContractor-update').append(
            $('<option>', {
              value: constructor.id,
              text: constructor.name,
            })
          )
        }
      },
    })
  }

  // get all designUnit
  function getAllDesign() {
    $.ajax({
      type: 'get',
      url: gAPI_URL + 'design-units',
      dataType: 'json',
      headers: headers,
      async: false,
      success: function (response) {
        // console.log("design", response)
        for (const design of response) {
          $('#select-designUnit').append(
            $('<option>', {
              value: design.id,
              text: design.name,
            })
          )
          $('#select-designUnit-update').append(
            $('<option>', {
              value: design.id,
              text: design.name,
            })
          )
        }
      },
    })
  }

  // load page by url param id
  function loadToTableById() {
    // load to table by customer id
    if (gCustomerId) {
      $.ajax({
        type: 'get',
        url: gAPI_URL + 'projects',
        headers: headers,
        dataType: 'json',
        async: false,
        success: function (response) {
          for (const data of response) {
            if (data.customerId == gCustomerId) {
              gCustomerById.push(data)
            }
          }
          loadToDataTable(gCustomerById)
          loadCustomerDetailToForm(gCustomerId)
          $('#customer-detail').show()
        },
      })
      // load to table by province id
    } else if (gProvinceUrlId) {
      $.ajax({
        type: 'get',
        url: gAPI_URL + 'projects',
        dataType: 'json',
        headers: headers,
        async: false,
        success: function (response) {
          for (const data of response) {
            if (data.provinceId == gProvinceUrlId) {
              gProvinceById.push(data)
            }
          }
          loadToDataTable(gProvinceById)
          loadProvinceToForm(gProvinceUrlId)
          $('#province-detail').show()
        },
      })
      // load to table by district id
    } else if (gDistrictUrlId) {
      $.ajax({
        type: 'get',
        url: gAPI_URL + 'projects',
        headers: headers,
        dataType: 'json',
        async: false,
        success: function (response) {
          for (const data of response) {
            if (data.districtId == gDistrictUrlId) {
              gDistrictById.push(data)
            }
          }
          loadToDataTable(gDistrictById)
          loadDistrictToForm(gDistrictUrlId)
          $('#district-detail').show()
        },
      })
      // load all data to table
    } else {
      getAllDataToTable()
    }
  }

  // load province detail to form
  function loadProvinceToForm(paramId) {
    $.ajax({
      type: 'get',
      url: gAPI_URL + 'province/' + paramId,
      headers: headers,
      dataType: 'json',
      async: false,
      success: function (response) {
        // console.log('province', response)
        loadToProvinceFormDetail(response)
      },
    })
  }

  // load district detail to form
  function loadDistrictToForm(paramId) {
    $.ajax({
      type: 'get',
      url: gAPI_URL + 'district/' + paramId,
      headers: headers,
      dataType: 'json',
      async: false,
      success: function (response) {
        // console.log('district', response)
        loadToDistrictFormDetail(response)
      },
    })
  }

  function loadToProvinceFormDetail(paramCustomer) {
    $('#span-id-province').html(paramCustomer.id)
    $('#span-name-province').html(paramCustomer.name)
    $('#span-code-province').html(paramCustomer.code)
    $('#span-number-province').html(gProvinceById.length)
  }

  function loadToDistrictFormDetail(paramCustomer) {
    $('#span-id-district').html(paramCustomer.id)
    $('#span-prefix-district').html(paramCustomer.prefix)
    $('#span-name-district').html(paramCustomer.name)
    $('#span-number-district').html(gDistrictById.length)
  }

  // province change
  function onChangeProvinceSelect(e) {
    gProvinceId = $('#select-province').val()
    $('#select-district').html('')
    $('#select-district').append(
      $('<option>', {
        value: '',
        text: 'Quận huyện',
      })
    )
    if (gProvinceId) {
      $.ajax({
        type: 'get',
        url: gAPI_URL + 'province/' + gProvinceId + '/districts',
        headers: headers,
        dataType: 'json',
        success: function (response) {
          // console.log('province', response)
          for (const district of response) {
            $('#select-district').append(
              $('<option>', {
                value: district.id,
                text: district.prefix + ' ' + district.name,
              })
            )
          }
        },
      })
    }
  }

  // province change update
  function onChangeProvinceUpdate(e) {
    gProvinceIdUpdate = $('#select-province-update').val()
    $('#select-district-update').html('')
    $('#select-district-update').append(
      $('<option>', {
        value: '',
        text: 'Quận huyện',
      })
    )
    if (gProvinceIdUpdate) {
      $.ajax({
        type: 'get',
        url: gAPI_URL + 'province/' + gProvinceIdUpdate + '/districts',
        headers: headers,
        dataType: 'json',
        success: function (response) {
          // console.log('district', response)
          for (const district of response) {
            $('#select-district-update').append(
              $('<option>', {
                value: district.id,
                text: district.prefix + ' ' + district.name,
              })
            )
          }
        },
      })
    }
  }

  // render stauts
  function renderStt(params) {
    gStt++
    return gStt
  }

  // render province name by id
  function getProvinceNameById(paramId) {
    for (var bI = 0; bI < gProvince.length; bI++) {
      if (gProvince[bI].id == paramId) {
        return gProvince[bI].name
      }
    }
    return null
  }
  // render district name by id
  function getDistrictNameById(paramId) {
    for (var bI = 0; bI < gDistrict.length; bI++) {
      if (gDistrict[bI].id == paramId) {
        return gDistrict[bI].name
      }
    }
    return null
  }

  // render constructor name by id
  function getConstructNameById(paramId) {
    if (paramId) {
      let name
      $.ajax({
        type: 'get',
        url: gAPI_URL + 'construction-contractor/' + paramId,
        dataType: 'json',
        async: false,
        headers: headers,
        success: function (response) {
          name = response.name
        },
      })
      return name
    } else {
      return null
    }
  }
  // render district name by id
  function getInvestorNameById(paramId) {
    if (paramId) {
      let name
      $.ajax({
        type: 'get',
        url: gAPI_URL + 'investor/' + paramId,
        dataType: 'json',
        async: false,
        headers: headers,
        success: function (response) {
          name = response.name
        },
      })
      return name
    } else {
      return null
    }
  }

  // render district name by id
  function getDesignNameById(paramId) {
    if (paramId) {
      let name
      $.ajax({
        type: 'get',
        url: gAPI_URL + 'design-unit/' + paramId,
        dataType: 'json',
        async: false,
        headers: headers,
        success: function (response) {
          name = response.name
        },
      })
      return name
    } else {
      return null
    }
  }

  // load data to table
  function loadToDataTable(paramData) {
    gStt = 0
    // console.log(paramData)
    gDataTable.clear()
    gDataTable.rows.add(paramData)
    gDataTable.draw()
  }

  

  // create
  function onCreateClick(e) {
    //get data
    e.preventDefault()
    getDataInput(gData)
    // console.log(gData)
    if ($('#create-form').valid()) {
      Swal.fire({
        title: 'Vui lòng xác nhận?',
        position: 'top',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Xác nhận',
      }).then((result) => {
        if (result.isConfirmed) {
        $.ajax({
          headers: headers,
          type: 'post',
          url: gAPI_URL + 'project',
          data: JSON.stringify(gData),
          contentType: 'application/json; charset=utf-8',
          success: function (respond) {
            Swal.fire({
                position: 'top',
                icon: 'success',
                title: 'Tạo mới thành công',
                showConfirmButton: false,
                timer: 1500,
              })
            // console.log('respond', respond)
            $('#modal-create').modal('hide')
            resetDataForm()
            getAllDataToTable()
          },
          error: function (error) {
            // alert(error.statusText)
            // console.log(error.responseText)
            },
          })
        }
      })
    }
  }

  // update article
  function onEditBtnClick(e) {
    let vSelectedRow = $(this).parents('tr')
    let vSelectedData = gDataTable.row(vSelectedRow).data()
    gDataId = vSelectedData.id
    // console.log('vSelectedData', vSelectedData)
    // console.log('gDataId', gDataId)
    $.ajax({
      type: 'get',
      headers: headers,
      async: false,
      url: gAPI_URL + `project/${gDataId}`,
      dataType: 'json',
      success: function (response) {
        loadDataToInput(response)
      },
    })
    $('#modal-update').modal('show')
  }

  // detail project
   function onBtnDetailClick(e) {
    let vSelectedRow = $(this).parents('tr')
    let vSelectedData = gDataTable.row(vSelectedRow).data()
    gDataId = vSelectedData.id
    // console.log('vSelectedData', vSelectedData)
    // console.log('gDataId', gDataId)
    $.ajax({
      type: 'get',
      headers: headers,
      async: false,
      url: gAPI_URL + `project/${gDataId}`,
      dataType: 'json',
      success: function (response) {
        loadDataToInput(response)
      },
    })
    $('#modal-detail').modal('show')
  }

  // call api update
  function onSaveUpdateClick(e) {
    getDataInputUpdate(gData)
    if ($('#update-form').valid()) {
      Swal.fire({
        title: 'Vui lòng xác nhận?',
        position: 'top',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Xác nhận',
      }).then((result) => {
        if (result.isConfirmed) {
        // console.log('gData', gData)
        // call api update
        $.ajax({
          type: 'put',
          headers: headers,
          url: gAPI_URL + 'project/' + gDataId,
          data: JSON.stringify(gData),
          async: false,
          contentType: 'application/json; charset=utf-8',
          success: function (response) {
            // console.log(response)
            Swal.fire({
                position: 'top',
                icon: 'success',
                title: 'Cập nhật thành công',
                showConfirmButton: false,
                timer: 1500,
              })
            $('#modal-update').modal('hide')
            getAllDataToTable()
          },
          error: function (error) {
            // console.log(error.responseText)
            },
          })
        }
      })
    }
  }

  // delete article
  function onDeleteClick(e) {
    $('#modal-delete-data').modal('show')
    let vSelectedRow = $(this).parents('tr')
    let vSelectedData = gDataTable.row(vSelectedRow).data()
    gDataId = vSelectedData.id
  }

  // confirm delete data
  function onConfirmDeleteClick() {
    $.ajax({
      url: gAPI_URL + `project/${gDataId}`,
      headers: headers,
      method: 'DELETE',
      success: () => {
       Swal.fire({
          position: 'top',
          icon: 'success',
          title: 'Xóa thành công',
          showConfirmButton: false,
          timer: 1500,
        })
        $('#modal-delete-data').modal('hide')
        getAllDataToTable()
      },
      // error: (err) => console.log(err.responseText),
    })
  }

  // handle checkbox value
  function getCheckboxVal(paramCheckbox) {
    let values = ''
    $(paramCheckbox).each(function () {
      values += $(this).val() + ','
    })
    // console.log(values)
    return values.substring(0, values.length - 1)
  }

  // get data input
  function getDataInput(paramObj) {
    paramObj.name = $('#input-name').val().trim()
    paramObj.provinceId = $('#select-province').val()
    paramObj.districtId = $('#select-district').val()
    paramObj.address = $('#input-address').val().trim()
    paramObj.slogan = $('#input-slogan').val().trim()
    paramObj.description = $('#input-description').val().trim()
    paramObj.acreage = $('#input-acreage').val().trim()
    paramObj.constructArea = $('#input-constructArea').val().trim()
    paramObj.numBlock = $('#input-numBlock').val().trim()
    paramObj.numFloors = $('#input-numFloors').val().trim()
    paramObj.apartmenttArea = $('#input-apartmenttArea').val().trim()
    paramObj.numApartment = $('#input-numApartment').val().trim()
    paramObj.investor = $('#select-investor').val()
    paramObj.constructionContractor = $('#select-constructionContractor').val()
    paramObj.designUnit = $('#select-designUnit').val()
    paramObj.utilities = getCheckboxVal($('[name=utilities]:checked'))
    paramObj.regionLink = getCheckboxVal($('[name=regionlink]:checked'))
    paramObj.photo = $('#input-photo').val().trim()
    paramObj.lat = $('#input-lat').val().trim()
    paramObj.lng = $('#input-lng').val().trim()
  }
  // get data input update
  function getDataInputUpdate(paramObj) {
    paramObj.name = $('#input-name-update').val().trim()
    paramObj.provinceId = $('#select-province-update').val()
    paramObj.districtId = $('#select-district-update').val()
    paramObj.address = $('#input-address-update').val().trim()
    paramObj.slogan = $('#input-slogan-update').val().trim()
    paramObj.description = $('#input-description-update').val().trim()
    paramObj.acreage = $('#input-acreage-update').val().trim()
    paramObj.constructArea = $('#input-constructArea-update').val().trim()
    paramObj.numBlock = $('#input-numBlock-update').val().trim()
    paramObj.numFloors = $('#input-numFloors-update').val().trim()
    paramObj.apartmenttArea = $('#input-apartmenttArea-update').val().trim()
    paramObj.numApartment = $('#input-numApartment-update').val().trim()
    paramObj.investor = $('#select-investor-update').val()
    paramObj.constructionContractor = $('#select-constructionContractor-update').val().trim()
    paramObj.designUnit = $('#select-designUnit-update').val().trim()
    paramObj.utilities = getCheckboxVal($('[name=utilities-update]:checked'))
    paramObj.regionLink = getCheckboxVal($('[name=regionlink-update]:checked'))
    paramObj.photo = $('#input-photo-update').val().trim()
    paramObj.lat = $('#input-lat-update').val().trim()
    paramObj.lng = $('#input-lng-update').val().trim()
  }

  // load data input update
  function loadDataToInput(paramObj) {
    $('#input-name-update').val(paramObj.name)
    $('#select-province-update').val(paramObj.provinceId)
    $('#select-district-update').val(paramObj.districtId)
    $('#input-address-update').val(paramObj.address)
    $('#input-slogan-update').val(paramObj.slogan)
    $('#input-description-update').val(paramObj.description)
    $('#input-acreage-update').val(paramObj.acreage)
    $('#input-constructArea-update').val(paramObj.constructArea)
    $('#input-numBlock-update').val(paramObj.numBlock)
    $('#input-numFloors-update').val(paramObj.numFloors)
    $('#input-apartmenttArea-update').val(paramObj.apartmenttArea)
    $('#input-numApartment-update').val(paramObj.numApartment)
    $('#select-investor-update').val(paramObj.investor)
    $('#select-constructionContractor-update').val(paramObj.constructionContractor)
    $('#select-update-designUnit').val(paramObj.designUnit)
    $('#input-utilities-update').val(paramObj.utilities)
    $('#input-regionLink-update').val(paramObj.regionLink)
    $('#input-photo-update').val(paramObj.photo)
    $('#input-lat-update').val(paramObj.lat)
    $('#input-lng-update').val(paramObj.lng)
  }

  // reset form
  function resetDataForm() {
    $('#input-name').val('')
    $('#select-province').val('')
    $('#select-district').val('')
    $('#input-address').val('')
    $('#input-slogan').val('')
    $('#input-description').val('')
    $('#input-acreage').val('')
    $('#input-constructArea').val('')
    $('#input-numBlock').val('')
    $('#input-numFloors').val('')
    $('#input-apartmenttArea').val('')
    $('#input-numApartment').val('')
    $('#select-investor').val('')
    $('#select-constructionContractor').val('')
    $('#select-designUnit').val('')
    $('#input-utilities').val('')
    $('#input-regionLink').val('')
    $('#input-photo').val('')
    $('#input-lat').val('')
    $('#input-lng').val('')
  }

  // validate data
  $('#create-form').validate({
    rules: {
      name: 'required',
      numApartment: 'required',
      investor: 'required',
    },
    messages: {
      name: 'Vui lòng nhập tên',
      numApartment: 'Vui lòng nhập số căn hộ',
      investor: 'Vui lòng chọn chủ đầu tư',
    },
  })
  // validate data update
  $('#update-form').validate({
    rules: {
      name: 'required',
      numApartment: 'required',
      investor: 'required',
    },
    messages: {
      name: 'Vui lòng nhập tên',
      numApartment: 'Vui lòng nhập số căn hộ',
      investor: 'Vui lòng chọn chủ đầu tư',
    },
  })

  $('#cancel-data').click(function (e) {
    e.preventDefault()
    resetDataForm()
    // console.log('cancel')
  })

  $('#cancel-data-update').click(function (e) {
    e.preventDefault()
    $('#modal-update').modal('hide')
  })
})
