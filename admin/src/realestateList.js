'use strict'
$(document).ready(function () {
  /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
  const gAPI_URL = 'http://localhost:8080/'

  // Authorization
  const token = getCookie('token')
  // console.log('token', token)

  // Khai báo xác thực ở headers
  var headers = {
    Authorization: 'Token ' + token,
  }

  let gDataTable = $('#data-table').DataTable({
    columns: [
      { data: 'id' },
      { data: 'provinceId' },
      { data: 'districtId' },
      { data: 'wardsId' },
      { data: 'streetId' },
      { data: 'type' },
      { data: 'request' },
      { data: 'customerId' },
      { data: 'price' },
      { data: 'dateCreate' },
      { data: 'acreage' },
      { data: 'createBy' },
      { data: 'numberFloors' },
      { data: 'widthY' },
      { data: 'longX' },
      { data: 'photo' },
      { data: 'priceTime' },
      { data: 'status' },
    ],
    columnDefs: [
      {
        targets: 18,
        defaultContent: `<i id="btn-edit" class="fas fa-edit text-primary" title="Edit" style="font-size: 24px;"></i> | <i id="btn-delete" class="fas fa-trash text-danger" title="Delete" style="font-size: 24px;"></i>`,
      },
      {
        targets: 17,
        // defaultContent: `
        // <input type="checkbox" class="form-check-input" name="status" id="status-check" value="checkedValue" >`,
        render: renderStatus,
      },
      {
        targets: 0,
        render: renderStt,
      },
      {
        targets: 1,
        render: getProvinceNameById,
      },
      {
        targets: 2,
        render: getDistrictNameById,
      },
      {
        targets: 3,
        render: getWardNameById,
      },
      {
        targets: 4,
        render: getStreetNameById,
      },
      {
        targets: 5,
        render: getTypeNameById,
      },
      {
        targets: 6,
        render: getRequetsNameById,
      },
      {
        targets: 7,
        render: getCustomerNameById,
      },
      {
        targets: 11,
        render: getCreatByNameById,
      },
      {
        targets: 16,
        render: getBuyTimeNameById,
      },
    ],
    dom: 'Bfrtip',
    buttons: ['copy', 'csv', 'excel', 'pdf', 'print', 'colvis'],
    lengthMenu: [
      [10, 25, 50, -1],
      [10, 25, 50, 'All'],
    ],
    order: [[0, 'desc']],
    autoWidth: true,
    scrollX: true,
  })

  let gUrlString = new URL(window.location.href)
  let gCustomerId = gUrlString.searchParams.get('customerId')
  let gProvinceUrlId = gUrlString.searchParams.get('provinceId')
  let gDistrictUrlId = gUrlString.searchParams.get('districtId')

  let gProvince = []
  let gDistrict = []
  let gWard = []
  let gStreet = []
  let gCustomer = []
  let gCustomerById = []
  let gProvinceById = []
  let gDistrictById = []
  let gStt = 0
  let gDistrictId = ''
  let gProvinceId = ''
  let gWardId = ''
  let gProvinceIdUpdate = ''
  let gUserId
  let gUserRole
  let gEstate

  // crud
  let gData = {
    title: '',
    type: '',
    request: '',
    provinceId: '',
    districtId: '',
    wardsId: '',
    streetId: '',
    projectId: '',
    address: '',
    customerId: '',
    price: '',
    priceMin: '',
    priceTime: '',
    // dateCreate: "",
    acreage: '',
    direction: '',
    totalFloors: '',
    numberFloors: '',
    bath: '',
    apartCode: "'",
    wallArea: '',
    bedroom: '',
    balcony: '',
    landscapeView: '',
    apartLoca: '',
    apartType: '',
    furnitureType: '',
    priceRent: '',
    returnRate: '',
    legalDoc: '',
    description: '',
    widthY: '',
    streetHouse: 0,
    viewNum: '',
    createBy: '',
    updateBy: '',
    shape: '',
    distance2facade: '',
    adjacentFacadeNum: '',
    adjacentRoad: '',
    alleyMinWidth: '',
    adjacentAlleyMinWidth: '',
    factor: '',
    structure: '',
    photo: '',
    lat: '',
    lng: '',
    ctxdprice: '',
    ctxdvalue: '',
    dtsxd: '',
    clcl: '',
    fsbo: 0,
    longX: '',
  }
  let gDataId = ''
  /*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */

  onPageLoading()
  // gán sự kiện nút create
  $('#create-data').click(onCreateClick)
  // gán sự kiện nút edit
  $('#data-table').on('click', '#btn-edit', onEditBtnClick)
  // gán sự kiện nút save update
  $('#btn-save').click(onSaveUpdateClick)
  // gán sự kiện nút delete
  $('#data-table').on('click', '#btn-delete', onDeleteClick)
  // gán sự kiện nút confirm delete
  $(document).on('click', '#delete-data', onConfirmDeleteClick)
  // gán sự kiện nút hiện new estate
  $('#showpending-btn').click(onBtnShowPendingBtn)
  // gán sự kiện change province
  $('#select-province').change(onProvinceChangeSelect)
  // gán sự kiện change province update
  $('#select-province-update').change(onProvinceChangeUpdateSelect)
  // gán sự kiện change district
  $('#select-district').change(onDistrictChangeSelect)
  // gán sự kiện change district
  $('#select-district-update').change(onDistrictChangeUpdateSelect)
  // gán sự kiện change ward
  $('#select-ward').change(onWardChangeSelect)
  // gán sự kiện change ward
  $('#select-ward-update').change(onWardChangeUpdateSelect)

  /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */

  function onPageLoading() {
    if (token) {
      callApiGetUserIdByToken(token)
    } else {
      window.location.href = 'index.html'
    }

    getPendingList()
    getAllProvince()
    getAllDistrict()
    getAllWard()
    getAllStreet()
    getAllCustomer()
    loadProjectToSelect()
  }

  /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/

  // get user id
  function callApiGetUserIdByToken(paramToken) {
    $.ajax({
      url: gAPI_URL + 'findUserId/Token',
      type: 'GET',
      dataType: 'json',
      contentType: 'application/json; charset=utf-8',
      headers: {
        Authorization: 'Token ' + paramToken,
      },
      success: function (responseObject) {
        // console.log('user id: ', responseObject)
        gUserId = responseObject
        getUserInfo(responseObject)
      },
      error: function (pAjaxContext) {
        //redirectToLogin();
      },
    })
  }

  // get user info to redirect
  function getUserInfo(paramId) {
    $.ajax({
      type: 'get',
      url: gAPI_URL + 'user/' + paramId,
      dataType: 'json',
      async: false,
      success: function (response) {
        // console.log(response)
        gUserRole = response.roles[0].roleKey
        // console.log(gUserRole)
        $('.info .d-block').html(response.username)
        handleUserAuth(gUserRole)
      },
    })
  }

  // load table by user role
  function handleUserAuth(paramRole) {
    switch (paramRole) {
      case 'ROLE_HOMESELLER':
        $.ajax({
          type: 'get',
          url: gAPI_URL + 'real-estate/createby/' + gUserId,
          headers: headers,
          dataType: 'json',
          success: function (response) {
            loadToDataTable(response)
          },
        })
        break
      default:
        getAllDataToTableById()
        break
    }
  }

  // get all list estate
  $('#showall-btn').click(function (e) {
    e.preventDefault()
    handleUserAuth(gUserRole)
  })

  // close detail form
  $('.close-detail').click(function (e) {
    e.preventDefault()
    window.location.href = 'realestateList.html'
  })

  //Hàm get Cookie
  function getCookie(cname) {
    var name = cname + '='
    var decodedCookie = decodeURIComponent(document.cookie)
    var ca = decodedCookie.split(';')
    for (var i = 0; i < ca.length; i++) {
      var c = ca[i]
      while (c.charAt(0) == ' ') {
        c = c.substring(1)
      }
      if (c.indexOf(name) == 0) {
        return c.substring(name.length, c.length)
      }
    }
    return ''
  }

  // get all estate load to table
  function getAllDataToTable() {
    $.ajax({
      type: 'get',
      url: gAPI_URL + 'real-estatesAll',
      headers: headers,
      async: false,
      dataType: 'json',
      success: function (response) {
        // console.log(response)
        gEstate = response
        loadToDataTable(response)
      },
    })
  }

  // get pending list to approve
  function getPendingList() {
    $.ajax({
      type: 'get',
      url: gAPI_URL + 'real-estate/status/0',
      dataType: 'json',
      success: function (response) {
        // console.log('pending', response)
        $('#pending-list-badge').html(response.length)
        // loadToDataTable(response)
      },
    })
  }

  function onBtnShowPendingBtn(e) {
    e.preventDefault()
    $('#district-detail').hide()
    $('#customer-detail').hide()
    $('#province-detail').hide()
    // window.location.href = "realestateListnew.html"
    $.ajax({
      type: 'get',
      url: gAPI_URL + 'real-estate/status/0',
      dataType: 'json',
      success: function (response) {
        // console.log('pending', response)
        $('#pending-list-badge').html(response.length)
        loadToDataTable(response)
      },
    })
  }

  // get all province
  function getAllProvince() {
    $.ajax({
      type: 'get',
      url: gAPI_URL + 'provinces',
      headers: headers,
      dataType: 'json',
      async: false,
      success: function (response) {
        gProvince = response
        for (const province of response) {
          $('#select-province').append(
            $('<option>', {
              value: province.id,
              text: province.name,
            })
          )
          $('#select-province-update').append(
            $('<option>', {
              value: province.id,
              text: province.name,
            })
          )
        }
        // console.log('gProvince', gProvince)
      },
    })
  }

  // get all district to update form
  function getAllDistrict() {
    $.ajax({
      type: 'get',
      url: gAPI_URL + 'districts',
      dataType: 'json',
      headers: headers,
      async: false,
      success: function (response) {
        // console.log('project', response)
        gDistrict = response
        for (const district of response) {
          $('#select-district-update').append(
            $('<option>', {
              value: district.id,
              text: district.name,
            })
          )
        }
      },
    })
  }

  // get all ward to update form
  function getAllWard() {
    $.ajax({
      type: 'get',
      url: gAPI_URL + 'wards',
      headers: headers,
      dataType: 'json',
      async: false,
      success: function (response) {
        gWard = response
        for (const ward of response) {
          $('#select-ward-update').append(
            $('<option>', {
              value: ward.id,
              text: ward.name,
            })
          )
        }
      },
    })
  }

  // get all street to update form
  function getAllStreet() {
    $.ajax({
      type: 'get',
      url: gAPI_URL + 'streets',
      headers: headers,
      dataType: 'json',
      async: false,
      success: function (response) {
        gStreet = response
        for (const ward of response) {
          $('#select-street-update').append(
            $('<option>', {
              value: ward.id,
              text: ward.name,
            })
          )
        }
      },
    })
  }

  // get all customer
  function getAllCustomer() {
    $.ajax({
      type: 'get',
      url: gAPI_URL + 'customersAll',
      headers: headers,
      async: false,
      dataType: 'json',
      async: false,
      success: function (response) {
        // console.log('gCustomer', response)
        gCustomer = response
        loadCustomerToSelect(response)
      },
    })
  }

  // load table by param url id
  function getAllDataToTableById() {
    // load table by customer id
    if (gCustomerId) {
      $.ajax({
        type: 'get',
        url: gAPI_URL + 'real-estate/customer/' + gCustomerId,
        headers: headers,
        dataType: 'json',
        async: false,
        success: function (response) {
          loadToDataTable(response)
          gCustomerById = response
          loadCustomerDetailToForm(gCustomerId)
          $('#customer-detail').show()
        },
      })
      // load table by province id
    } else if (gProvinceUrlId) {
      $.ajax({
        type: 'get',
        url: gAPI_URL + 'real-estatesAll',
        headers: headers,
        dataType: 'json',
        async: false,
        success: function (response) {
          for (const data of response) {
            if (data.provinceId == gProvinceUrlId) {
              gProvinceById.push(data)
            }
          }
          loadToDataTable(gProvinceById)
          loadProvinceToForm(gProvinceUrlId)
          $('#province-detail').show()
        },
      })
      // load table by district id
    } else if (gDistrictUrlId) {
      $.ajax({
        type: 'get',
        url: gAPI_URL + 'real-estates',
        headers: headers,
        dataType: 'json',
        async: false,
        success: function (response) {
          for (const data of response) {
            if (data.districtId == gDistrictUrlId) {
              gDistrictById.push(data)
            }
          }
          loadToDataTable(gDistrictById)
          loadDistrictToForm(gDistrictUrlId)
          $('#district-detail').show()
        },
      })
      // load all if no param
    } else {
      getAllDataToTable()
    }
  }

  // province change
  function onProvinceChangeSelect(e) {
    gProvinceId = $('#select-province').val()
    $('#select-district').html('')
    $('#select-district').append(
      $('<option>', {
        value: '',
        text: 'Quận huyện',
      })
    )

    $('#select-ward').html('')
    $('#select-ward').append(
      $('<option>', {
        value: '',
        text: 'Phường xã',
      })
    )

    $('#select-street').html('')
    $('#select-street').append(
      $('<option>', {
        value: '',
        text: 'Đường phố',
      })
    )
    if (gProvinceId) {
      $.ajax({
        headers: headers,
        type: 'get',
        url: gAPI_URL + 'province/' + gProvinceId + '/districts',
        dataType: 'json',
        success: function (response) {
          // console.log('district', response)
          for (const district of response) {
            $('#select-district').append(
              $('<option>', {
                value: district.id,
                text: district.prefix + ' ' + district.name,
              })
            )
          }
        },
      })
    }
  }

  // province change update
  function onProvinceChangeUpdateSelect(e) {
    gProvinceIdUpdate = $('#select-province-update').val()
    $('#select-district-update').html('')
    $('#select-district-update').attr('disabled', false)
    $('#select-district-update').append(
      $('<option>', {
        value: '',
        text: 'Quận huyện',
      })
    )

    $('#select-ward-update').html('')
    $('#select-ward-update').append(
      $('<option>', {
        value: '',
        text: 'Phường xã',
      })
    )

    $('#select-street-update').html('')
    $('#select-street-update').append(
      $('<option>', {
        value: '',
        text: 'Đường phố',
      })
    )
    if (gProvinceIdUpdate) {
      $.ajax({
        type: 'get',
        url: gAPI_URL + 'province/' + gProvinceIdUpdate + '/districts',
        headers: headers,
        dataType: 'json',
        success: function (response) {
          // console.log('district', response)
          for (const district of response) {
            $('#select-district-update').append(
              $('<option>', {
                value: district.id,
                text: district.prefix + ' ' + district.name,
              })
            )
          }
        },
      })
    }
  }

  // district change
  function onDistrictChangeSelect(e) {
    gDistrictId = $('#select-district').val()
    $('#select-ward').html('')
    $('#select-ward').append(
      $('<option>', {
        value: '',
        text: 'Phường xã',
      })
    )

    $('#select-street').html('')
    $('#select-street').append(
      $('<option>', {
        value: '',
        text: 'Đường phố',
      })
    )
    if (gDistrictId) {
      $.ajax({
        type: 'get',
        url: gAPI_URL + 'district/' + gDistrictId + '/wards',
        headers: headers,
        dataType: 'json',
        success: function (response) {
          // console.log('ward', response)
          for (const ward of response) {
            $('#select-ward').append(
              $('<option>', {
                value: ward.id,
                text: ward.prefix + ' ' + ward.name,
              })
            )
          }
        },
      })
    }
  }

  // district change update
  function onDistrictChangeUpdateSelect(e) {
    gDistrictId = $('#select-district-update').val()
    $('#select-ward-update').attr('disabled', false)
    $('#select-ward').html('')
    $('#select-ward').append(
      $('<option>', {
        value: '',
        text: 'Phường xã',
      })
    )
    $('#select-street').html('')
    $('#select-street').append(
      $('<option>', {
        value: '',
        text: 'Đường phố',
      })
    )
    if (gDistrictId) {
      $.ajax({
        type: 'get',
        url: gAPI_URL + 'district/' + gDistrictId + '/wards',
        headers: headers,
        dataType: 'json',
        success: function (response) {
          // console.log('ward', response)
          for (const ward of response) {
            $('#select-ward-update').append(
              $('<option>', {
                value: ward.id,
                text: ward.prefix + ' ' + ward.name,
              })
            )
          }
        },
      })
    }
  }

  // ward change
  function onWardChangeSelect(e) {
    gWardId = $('#select-ward').val()
    // $('#select-street-update').attr('disabled', false)
    $('#select-street').html('')
    $('#select-street').append(
      $('<option>', {
        value: '',
        text: 'Đường phố',
      })
    )
    if (gDistrictId) {
      // console.log(gDistrictId)
      $.ajax({
        type: 'get',
        headers: headers,
        url: gAPI_URL + `district/${gDistrictId}/street`,
        dataType: 'json',
        success: function (response) {
          // console.log('ward', response)
          for (const street of response) {
            $('#select-street').append(
              $('<option>', {
                value: street.id,
                text: street.prefix + ' ' + street.name,
              })
            )
          }
        },
      })
    }
  }

  // ward update change
  function onWardChangeUpdateSelect(e) {
    gWardId = $('#select-ward-update').val()
    $('#select-street-update').attr('disabled', false)
    $('#select-street-update').html('')
    $('#select-street-update').append(
      $('<option>', {
        value: '',
        text: 'Đường phố',
      })
    )
    if (gDistrictId) {
      // console.log(gDistrictId)
      $.ajax({
        type: 'get',
        headers: headers,
        url: gAPI_URL + `district/${gDistrictId}/street`,
        dataType: 'json',
        success: function (response) {
          // console.log('ward', response)
          for (const street of response) {
            $('#select-street-update').append(
              $('<option>', {
                value: street.id,
                text: street.prefix + ' ' + street.name,
              })
            )
          }
        },
      })
    }
  }

  // load project to select
  function loadProjectToSelect() {
    $.ajax({
      type: 'get',
      url: gAPI_URL + 'projectsAll',
      dataType: 'json',
      headers: headers,
      async: false,
      success: function (response) {
        // console.log('project', response)
        $('#select-project').html('')
        $('#select-project').append(
          $('<option>', {
            value: '',
            text: 'Chọn dự án',
          })
        )
        $('#select-project-update').html('')
        $('#select-project-update').append(
          $('<option>', {
            value: '',
            text: 'Chọn dự án',
          })
        )
        for (const data of response) {
          $('#select-project').append(
            $('<option>', {
              value: data.id,
              text: data.name,
            })
          )
          $('#select-project-update').append(
            $('<option>', {
              value: data.id,
              text: data.name,
            })
          )
        }
      },
    })
  }

  // load customer to select
  function loadCustomerToSelect(paramData) {
    $('#select-customer').html('')
    $('#select-customer').append(
      $('<option>', {
        value: '',
        text: 'Chọn khách hàng',
      })
    )
    for (const data of paramData) {
      $('#select-customer').append(
        $('<option>', {
          value: data.id,
          text: data.contactName,
        })
      )
    }
    $('#select-customer-update').html('')
    $('#select-customer-update').append(
      $('<option>', {
        value: '',
        text: 'Chọn khách hàng',
      })
    )
    for (const data of paramData) {
      $('#select-customer-update').append(
        $('<option>', {
          value: data.id,
          text: data.contactName,
        })
      )
    }
  }

  // render stauts
  function renderStt(params) {
    gStt++
    return gStt
  }

  // render stauts
  function renderStatus(param) {
    if (param == 0) {
      return `        
      <input type="checkbox" class="form-check-input" name="status" id="status-check" style="width:20px; height:20px" >`
    } else {
      return `        
      <input type="checkbox" class="form-check-input" name="status" id="status-check"
      checked style="width:20px; height:20px" disabled>`
    }
  }

  // render province name by id
  function getProvinceNameById(paramId) {
    if (paramId) {
      for (var bI = 0; bI < gProvince.length; bI++) {
        if (gProvince[bI].id == paramId) {
          return gProvince[bI].name
        }
      }
    } else {
      return null
    }
  }

  // render district name by id
  function getDistrictNameById(paramId) {
    if (paramId) {
      for (var bI = 0; bI < gDistrict.length; bI++) {
        if (gDistrict[bI].id == paramId) {
          return gDistrict[bI].name
        }
      }
    } else {
      return null
    }
  }
  // render ward name by id
  function getWardNameById(paramId) {
    if (paramId) {
      for (var bI = 0; bI < gWard.length; bI++) {
        if (gWard[bI].id == paramId) {
          return gWard[bI].prefix + ' ' + gWard[bI].name
        }
      }
    } else {
      return null
    }
  }

  // render street name by id
  function getStreetNameById(paramId) {
    if (paramId) {
      for (var bI = 0; bI < gStreet.length; bI++) {
        if (gStreet[bI].id == paramId) {
          return gStreet[bI].prefix + ' ' + gStreet[bI].name
        }
      }
    } else {
      return null
    }
  }

  // render customer name by id
  function getCustomerNameById(paramId) {
    for (const customer of gCustomer) {
      if (paramId == customer.id) {
        return customer.contactName + ' ' + customer.mobile
      }
    }
    return null
  }

  // render createby name by id
  function getCreatByNameById(paramId) {
    if (paramId) {
      // console.log(paramId)
      let username = ''
      $.ajax({
        type: 'get',
        url: gAPI_URL + 'user/' + paramId,
        headers: headers,
        async: false,
        dataType: 'json',
        success: function (response) {
          username = response.username
        },
      })
      return username
    } else {
      return null
    }
  }

  // render createby name by id
  function getBuyTimeNameById(paramId) {
    if (paramId) {
      switch (paramId) {
        case 0:
          return 'Bán nhanh 24h'
          break
        case 1:
          return 'Bán nhanh 72h'
          break
        case 2:
          return 'Bán nhanh 1 tuần'
          break
      }
    } else {
      return null
    }
  }

  // render type name by id
  function getTypeNameById(paramId) {
    if (paramId || paramId == 0) {
      switch (paramId) {
        case 0:
          return 'Đất'
          break
        case 1:
          return 'Nhà ở'
          break
        case 2:
          return 'Căn hộ/chung cư'
          break
        case 3:
          return 'Văn phòng, mặt bằng'
          break
        case 4:
          return 'Kinh doanh'
          break
        case 5:
          return 'Phòng trọ'
          break
      }
    } else {
      return null
    }
  }

  // render request name by id
  function getRequetsNameById(paramId) {
    switch (paramId) {
      case 0:
        return 'Cần bán'
        break
      case 2:
        return 'Cần mua'
        break
      case 3:
        return 'Cho thuê'
        break
      case 4:
        return 'Cần thuê'
        break
      default:
        return null
        break
    }
  }

  // check status change
  $('#data-table').on('click', 'input[name=status]', function (e) {
    e.stopPropagation()
    let vSelectedRow = $(this).parents('tr')
    let vSelectedData = gDataTable.row(vSelectedRow).data()
    let dataId = vSelectedData.id
    vSelectedData.status = e.target.checked ? 1 : 0

    Swal.fire({
        title: 'Vui lòng xác nhận?',
        position: 'top',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Xác nhận',
      }).then((result) => {
        if (result.isConfirmed) {
      // call api update
      $.ajax({
        type: 'put',
        url: gAPI_URL + 'real-estate/' + dataId,
        headers: headers,
        data: JSON.stringify(vSelectedData),
        async: false,
        contentType: 'application/json; charset=utf-8',
        success: function (response) {
          // console.log(response)
          Swal.fire({
                position: 'top',
                icon: 'success',
                title: 'Cập nhật thành công',
                showConfirmButton: false,
                timer: 1500,
              })
          $.ajax({
            type: 'get',
            url: gAPI_URL + 'real-estate/status/0',
            dataType: 'json',
            success: function (response) {
              $('#pending-list-badge').html(response.length)
              loadToDataTable(response)
            },
          })
        },
        error: function (error) {
          // console.log(error.responseText)
        },
      })
    } else {
      $(this).prop('checked', true ? false : true)
    }
  })

  // load customer detail to form
  function loadCustomerDetailToForm(paramId) {
    $.ajax({
      type: 'get',
      url: gAPI_URL + 'customer/' + paramId,
      headers: headers,
      dataType: 'json',
      async: false,
      success: function (response) {
        // console.log('customer', response)
        loadCustomerToForm(response)
      },
    })
  }

  // load province detail to form
  function loadProvinceToForm(paramId) {
    $.ajax({
      type: 'get',
      url: gAPI_URL + 'province/' + paramId,
      headers: headers,
      dataType: 'json',
      async: false,
      success: function (response) {
        // console.log('province', response)
        loadToProvinceFormDetail(response)
      },
    })
  }
  // load district detail to form
  function loadDistrictToForm(paramId) {
    $.ajax({
      type: 'get',
      url: gAPI_URL + 'district/' + paramId,
      headers: headers,
      dataType: 'json',
      async: false,
      success: function (response) {
        // console.log('district', response)
        loadToDistrictFormDetail(response)
      },
    })
  }

  function loadCustomerToForm(paramCustomer) {
    $('#span-id').html(paramCustomer.id)
    $('#span-title').html(paramCustomer.contactTitle)
    $('#span-name').html(paramCustomer.contactName)
    $('#span-address').html(paramCustomer.address)
    $('#span-phone').html(paramCustomer.mobile)
    $('#span-email').html(paramCustomer.email)
    $('#span-note').html(paramCustomer.note)
    $('#span-createby').html(paramCustomer.createBy)
    $('#span-createdate').html(paramCustomer.createDate)
    $('#span-updateby').html(paramCustomer.updateBy)
    $('#span-updatedate').html(paramCustomer.updateDate)
    $('#span-numberproduct').html(gCustomerById.length)
  }

  function loadToProvinceFormDetail(paramCustomer) {
    $('#span-id-province').html(paramCustomer.id)
    $('#span-name-province').html(paramCustomer.name)
    $('#span-code-province').html(paramCustomer.code)
    $('#span-number-province').html(gProvinceById.length)
  }

  function loadToDistrictFormDetail(paramCustomer) {
    $('#span-id-district').html(paramCustomer.id)
    $('#span-prefix-district').html(paramCustomer.prefix)
    $('#span-name-district').html(paramCustomer.name)
    $('#span-number-district').html(gDistrictById.length)
  }

  function loadEstateDetailToForm(paramObj) {
    $('#span-estate-id').html(paramObj.id)
    $('#span-estate-title').html(paramObj.title)
    $('#span-estate-address').html(paramObj.address)
    $('#span-estate-price').html(paramObj.price)
    $('#span-estate-apartCode').html(paramObj.apartCode)
    $('#span-estate-bedroom').html(paramObj.bedroom)
    $('#span-estate-bath').html(paramObj.bath)
    $('#span-estate-dateCreate').html(paramObj.dateCreate)
  }

  // preview estae
  $(document).on('click', '#data-table tr', function (e) {
    e.stopPropagation()
    let vSelectedRow = gDataTable.row(this).data()
    $('#estate-detail').modal('show')
    // console.log(vSelectedRow)
    loadEstateDetailToForm(vSelectedRow)
  })

  // load data to table
  function loadToDataTable(paramData) {
    gStt = 0
    // console.log(paramData)
    gDataTable.clear()
    gDataTable.rows.add(paramData)
    gDataTable.draw()
  }

  // create
  function onCreateClick(e) {
    //get data
    e.preventDefault()
    getDataInput(gData)
    // console.log(gData)
    if ($('#create-form').valid()) {
      Swal.fire({
        title: 'Vui lòng xác nhận?',
        position: 'top',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Xác nhận',
      }).then((result) => {
        if (result.isConfirmed) {
        $.ajax({
          type: 'post',
          headers: headers,
          url: gAPI_URL + 'real-estate',
          data: JSON.stringify(gData),
          contentType: 'application/json; charset=utf-8',
          success: function (respond) {
            Swal.fire({
                position: 'top',
                icon: 'success',
                title: 'Tạo mới thành công',
                showConfirmButton: false,
                timer: 1500,
              })
            $('#modal-create').modal('hide')
            // console.log('respond', respond)
            $('#modal-create').modal('hide')
            resetDataForm()
            handleUserAuth(gUserRole)
            getPendingList()
          },
          error: function (error) {
            // alert(error.statusText)
            // console.log(error.responseText)
            },
          })
        }
      })
    }
  }

  // update article
  function onEditBtnClick(e) {
    e.stopPropagation()
    let vSelectedRow = $(this).parents('tr')
    let vSelectedData = gDataTable.row(vSelectedRow).data()
    gDataId = vSelectedData.id
    // console.log('vSelectedData', vSelectedData)
    // console.log('gDataId', gDataId)
    $.ajax({
      type: 'get',
      headers: headers,
      async: false,
      url: gAPI_URL + `real-estate/${gDataId}`,
      dataType: 'json',
      success: function (response) {
        loadDataToInput(response)
      },
    })
    $('#modal-update').modal('show')
  }

  // call api update
  function onSaveUpdateClick(e) {
    e.stopPropagation()
    getDataInputUpdate(gData)
    if ($('#update-form').valid()) {
      Swal.fire({
        title: 'Vui lòng xác nhận?',
        position: 'top',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Xác nhận',
      }).then((result) => {
        if (result.isConfirmed) {
        // console.log('gData', gData)
        // call api update
        $.ajax({
          type: 'put',
          url: gAPI_URL + 'real-estate/' + gDataId,
          headers: headers,
          data: JSON.stringify(gData),
          async: false,
          contentType: 'application/json; charset=utf-8',
          success: function (response) {
            // console.log(response)
            Swal.fire({
                position: 'top',
                icon: 'success',
                title: 'Cập nhật thành công',
                showConfirmButton: false,
                timer: 1500,
              })
            $('#modal-update').modal('hide')
            handleUserAuth(gUserRole)
            getPendingList()
          },
          error: function (error) {
            // console.log(error.responseText)
            },
          })
        }
      })
    }
  }

  // delete article
  function onDeleteClick(e) {
    e.stopPropagation()
    $('#modal-delete-data').modal('show')
    let vSelectedRow = $(this).parents('tr')
    let vSelectedData = gDataTable.row(vSelectedRow).data()
    gDataId = vSelectedData.id
  }

  // confirm delete data
  function onConfirmDeleteClick() {
    $.ajax({
      url: gAPI_URL + `real-estate/${gDataId}`,
      headers: headers,
      method: 'DELETE',
      success: () => {
       Swal.fire({
          position: 'top',
          icon: 'success',
          title: 'Xóa thành công',
          showConfirmButton: false,
          timer: 1500,
        })
        $('#modal-delete-data').modal('hide')
        handleUserAuth(gUserRole)
        getPendingList()
      },
      // // error: (err) => console.log(err.responseText),
    })
  }

  // get data input
  function getDataInput(paramObj) {
    paramObj.title = $('#input-title').val().trim()
    paramObj.type = $('#select-type').val().trim()
    paramObj.request = $('#select-request').val().trim()
    paramObj.provinceId = $('#select-province').val()
    paramObj.districtId = $('#select-district').val()
    paramObj.streetId = $('#select-street').val()
    paramObj.wardsId = $('#select-ward').val()
    paramObj.projectId = $('#select-project').val()
    paramObj.address = $('#input-address').val().trim()
    paramObj.customerId = $('#select-customer').val()
    paramObj.createBy = gUserId
    paramObj.price = $('#input-price').val().trim()
    paramObj.priceMin = $('#input-priceMin').val().trim()
    paramObj.priceTime = $('#select-priceTime').val()
    paramObj.acreage = $('#input-acreage').val().trim()
    paramObj.direction = $('#input-direction').val().trim()
    paramObj.totalFloors = $('#input-totalFloors').val().trim()
    paramObj.numberFloors = $('#input-numberFloors').val().trim()
    paramObj.bath = $('#input-bath').val().trim()
    paramObj.apartCode = $('#input-apartCode').val().trim()
    paramObj.wallArea = $('#input-wallArea').val().trim()
    paramObj.bedroom = $('#input-bedroom').val().trim()
    paramObj.balcony = $('#select-balcony').val().trim()
    paramObj.landscapeView = $('#input-landscapeView').val()
    paramObj.apartLoca = $('#select-apartLoca').val().trim()
    paramObj.apartType = $('#select-apartType').val()
    paramObj.furnitureType = $('#select-furnitureType').val()
    paramObj.priceRent = $('#input-priceRent').val().trim()
    paramObj.returnRate = $('#input-returnRate').val().trim()
    paramObj.legalDoc = $('#input-legalDoc').val().trim()
    paramObj.description = $('#input-description').val().trim()
    paramObj.widthY = $('#input-widthY').val().trim()
    paramObj.streetHouse = $("input[name='streetHouse']").is(':checked') ? 1 : 0
    paramObj.viewNum = $('#input-viewNum').val().trim()
    paramObj.shape = $('#input-shape').val().trim()
    paramObj.distance2facade = $('#input-distance2facade').val().trim()
    paramObj.adjacentFacadeNum = $('#input-adjacentFacadeNum').val().trim()
    paramObj.adjacentRoad = $('#input-adjacentRoad').val().trim()
    paramObj.alleyMinWidth = $('#input-alleyMinWidth').val().trim()
    paramObj.adjacentAlleyMinWidth = $('#input-adjacentAlleyMinWidth').val().trim()
    paramObj.factor = $('#input-factor').val().trim()
    paramObj.structure = $('#input-structure').val().trim()
    paramObj.photo = $('#input-photo').val()
    paramObj.lat = $('#input-lat').val().trim()
    paramObj.lng = $('#input-lng').val().trim()
    paramObj.ctxdprice = $('#input-ctxdprice').val().trim()
    paramObj.ctxdvalue = $('#input-ctxdvalue').val().trim()
    paramObj.dtsxd = $('#input-dtsxd').val().trim()
    paramObj.clcl = $('#input-clcl').val().trim()
    paramObj.fsbo = $("input[name='fsbo']").is(':checked') ? 1 : 0
    paramObj.longX = $('#input-longX').val().trim()
    paramObj.status = $('#select-status').val()
  }
  // get data input update
  function getDataInputUpdate(paramObj) {
    paramObj.title = $('#input-title-update').val().trim()
    paramObj.type = $('#select-type-update').val().trim()
    paramObj.request = $('#select-request-update').val().trim()
    paramObj.provinceId = $('#select-province-update').val()
    paramObj.districtId = $('#select-district-update').val()
    paramObj.streetId = $('#select-street-update').val()
    paramObj.wardsId = $('#select-ward-update').val()
    paramObj.projectId = $('#select-project-update').val()
    paramObj.address = $('#input-address-update').val().trim()
    paramObj.customerId = $('#select-customer-update').val()
    paramObj.updateBy = gUserId
    paramObj.price = $('#input-price-update').val().trim()
    paramObj.priceMin = $('#input-priceMin-update').val().trim()
    paramObj.priceTime = $('#select-priceTime-update').val()
    paramObj.acreage = $('#input-acreage-update').val().trim()
    paramObj.direction = $('#input-direction-update').val().trim()
    paramObj.totalFloors = $('#input-totalFloors-update').val().trim()
    paramObj.numberFloors = $('#input-numberFloors-update').val().trim()
    paramObj.bath = $('#input-bath-update').val().trim()
    paramObj.apartCode = $('#input-apartCode-update').val().trim()
    paramObj.wallArea = $('#input-wallArea-update').val().trim()
    paramObj.bedroom = $('#input-bedroom-update').val().trim()
    paramObj.balcony = $('#select-balcony-update').val().trim()
    paramObj.landscapeView = $('#input-landscapeView-update').val()
    paramObj.apartLoca = $('#select-apartLoca-update').val().trim()
    paramObj.apartType = $('#select-apartType-update').val()
    paramObj.furnitureType = $('#select-furnitureType-update').val()
    paramObj.priceRent = $('#input-priceRent-update').val().trim()
    paramObj.returnRate = $('#input-returnRate-update').val().trim()
    paramObj.legalDoc = $('#input-legalDoc-update').val().trim()
    paramObj.description = $('#input-description-update').val().trim()
    paramObj.widthY = $('#input-widthY-update').val().trim()
    paramObj.streetHouse = $("input[name='streetHouse']").is(':checked') ? 1 : 0
    paramObj.viewNum = $('#input-viewNum-update').val().trim()
    paramObj.shape = $('#input-shape-update').val().trim()
    paramObj.distance2facade = $('#input-distance2facade-update').val().trim()
    paramObj.adjacentFacadeNum = $('#input-adjacentFacadeNum-update').val().trim()
    paramObj.adjacentRoad = $('#input-adjacentRoad-update').val().trim()
    paramObj.alleyMinWidth = $('#input-alleyMinWidth-update').val().trim()
    paramObj.adjacentAlleyMinWidth = $('#input-adjacentAlleyMinWidth-update').val().trim()
    paramObj.factor = $('#input-factor-update').val().trim()
    paramObj.structure = $('#input-structure-update').val().trim()
    paramObj.photo = $('#input-photo-update').val()
    paramObj.lat = $('#input-lat-update').val().trim()
    paramObj.lng = $('#input-lng-update').val().trim()
    paramObj.ctxdprice = $('#input-ctxdprice-update').val().trim()
    paramObj.ctxdvalue = $('#input-ctxdvalue-update').val().trim()
    paramObj.dtsxd = $('#input-dtsxd-update').val().trim()
    paramObj.clcl = $('#input-clcl-update').val().trim()
    paramObj.fsbo = $("input[name='fsbo']").is(':checked') ? 1 : 0
    paramObj.longX = $('#input-longX-update').val().trim()
    paramObj.status = $('#select-status-update').val()
  }

  // load data input update
  function loadDataToInput(paramObj) {
    $('#input-title-update').val(paramObj.title)
    $('#select-type-update').val(paramObj.type)
    $('#select-request-update').val(paramObj.request)
    $('#select-province-update').val(paramObj.provinceId)
    $('#select-district-update').val(paramObj.districtId)
    $('#select-street-update').val(paramObj.streetId)
    $('#select-ward-update').val(paramObj.wardsId)
    $('#select-project-update').val(paramObj.projectId)
    $('#input-address-update').val(paramObj.address)
    $('#select-customer-update').val(paramObj.customerId)
    $('#input-price-update').val(paramObj.price)
    $('#input-priceMin-update').val(paramObj.priceMin)
    $('#select-priceTime-update').val(paramObj.priceTime)
    $('#input-acreage-update').val(paramObj.acreage)
    $('#input-direction-update').val(paramObj.direction)
    $('#input-totalFloors-update').val(paramObj.totalFloors)
    $('#input-numberFloors-update').val(paramObj.numberFloors)
    $('#input-bath-update').val(paramObj.bath)
    $('#input-apartCode-update').val(paramObj.apartCode)
    $('#input-wallArea-update').val(paramObj.wallArea)
    $('#input-bedroom-update').val(paramObj.bedroom)
    $('#select-balcony-update').val(paramObj.balcony)
    $('#input-landscapeView-update').val(paramObj.landscapeView)
    $('#select-apartLoca-update').val(paramObj.apartLoca)
    $('#select-apartType-update').val(paramObj.apartType)
    $('#select-furnitureType-update').val(paramObj.furnitureType)
    $('#input-priceRent-update').val(paramObj.priceRent)
    $('#input-returnRate-update').val(paramObj.returnRate)
    $('#input-legalDoc-update').val(paramObj.legalDoc)
    $('#input-description-update').val(paramObj.description)
    $('#input-widthY-update').val(paramObj.widthY)
    1 ? $('#input-streetHouse-update').prop('checked', true) : $('#input-streetHouse-update').prop('checked', false)
    $('#input-viewNum-update').val(paramObj.viewNum)
    $('#input-shape-update').val(paramObj.shape)
    $('#input-distance2facade-update').val(paramObj.distance2facade)
    $('#input-adjacentFacadeNum-update').val(paramObj.adjacentFacadeNum)
    $('#input-adjacentRoad-update').val(paramObj.adjacentRoad)
    $('#input-alleyMinWidth-update').val(paramObj.alleyMinWidth)
    $('#input-adjacentAlleyMinWidth-update').val(paramObj.adjacentAlleyMinWidth)
    $('#input-factor-update').val(paramObj.factor)
    $('#input-structure-update').val(paramObj.structure)
    $('#input-photo-update').val(paramObj.photo)
    $('#input-lat-update').val(paramObj.lat)
    $('#input-lng-update').val(paramObj.lng)
    $('#input-ctxdprice-update').val(paramObj.ctxdprice)
    $('#input-ctxdvalue-update').val(paramObj.ctxdvalue)
    $('#input-dtsxd-update').val(paramObj.dtsxd)
    $('#input-clcl-update').val(paramObj.clcl)
    1 ? $('#input-fsbo-update').prop('checked', true) : $('#input-fsbo-update').prop('checked', false)
    $('#input-longX-update').val(paramObj.longX)
    $('#select-status-update').val(paramObj.status)
  }

  // reset form
  function resetDataForm() {
    $('#input-title').val('')
    $('#select-type').val('')
    $('#select-request').val('')
    $('#select-province').val('')
    $('#select-district').val('')
    $('#select-street').val('')
    $('#select-ward').val('')
    $('#select-project').val('')
    $('#input-address').val('')
    $('#select-customer').val('')
    $('#input-price').val('')
    $('#input-priceMin').val('')
    $('#select-priceTime').val('')
    $('#input-acreage').val('')
    $('#input-direction').val('')
    $('#input-totalFloors').val('')
    $('#input-numberFloors').val('')
    $('#input-bath').val('')
    $('#input-apartCode').val('')
    $('#input-wallArea').val('')
    $('#input-bedroom').val('')
    $('#select-balcony').val('')
    $('#input-landscapeView').val('')
    $('#select-apartLoca').val('')
    $('#select-apartType').val('')
    $('#select-furnitureType').val('')
    $('#input-priceRent').val('')
    $('#input-returnRate').val('')
    $('#input-legalDoc').val('')
    $('#input-description').val('')
    $('#input-widthY').val('')
    $('#input-streetHouse').val('')
    $('#input-viewNum').val('')
    $('#input-shape').val('')
    $('#input-distance2facade').val('')
    $('#input-adjacentFacadeNum').val('')
    $('#input-adjacentRoad').val('')
    $('#input-alleyMinWidth').val('')
    $('#input-adjacentAlleyMinWidth').val('')
    $('#input-factor').val('')
    $('#input-structure').val('')
    $('#input-photo').val('')
    $('#input-lat').val('')
    $('#input-lng').val('')
    $('#input-ctxdprice').val('')
    $('#input-ctxdvalue').val('')
    $('#input-dtsxd').val('')
    $('#input-clcl').val('')
    $('#input-fsbo').val('')
    $('#input-longX').val('')
  }

  // validate data
  $('#create-form').validate({
    rules: {
      name: 'required',
      address: 'required',
      province: 'required',
      district: 'required',
      ward: 'required',
      title: 'required',
      price: { required: true, number: true },
      priceMin: { number: true },
    },
    messages: {
      name: 'Vui lòng nhập tên',
      address: 'Vui lòng nhập address',
      province: 'Vui lòng chọn tỉnh thành',
      district: 'Vui lòng chọn quận huyện',
      ward: 'Vui lòng chọn phường xã',
      title: 'Vui lòng nhập tiêu đề',
      price: 'Vui lòng nhập giá',
      tipriceMintle: 'Vui lòng nhập số',
    },
  })
  // validate data update
  $('#update-form').validate({
    rules: {
      name: 'required',
      address: 'required',
      province: 'required',
      district: 'required',
      ward: 'required',
      title: 'required',
      price: { required: true, number: true },
      priceMin: { number: true },
    },
    messages: {
      name: 'Vui lòng nhập tên',
      address: 'Vui lòng nhập address',
      province: 'Vui lòng chọn tỉnh thành',
      district: 'Vui lòng chọn quận huyện',
      ward: 'Vui lòng chọn phường xã',
      title: 'Vui lòng nhập tiêu đề',
      price: 'Vui lòng nhập giá',
      tipriceMintle: 'Vui lòng nhập số',
    },
  })

  $('#cancel-data').click(function (e) {
    e.preventDefault()
    resetDataForm()
    // console.log('cancel')
  })

  $('#cancel-data-update').click(function (e) {
    e.preventDefault()
    $('#modal-update').modal('hide')
  })
})
