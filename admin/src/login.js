$(document).ready(function () {
  const gAPI_URL = 'http://localhost:8080/'
  
  //Kiểm tra token nếu có token tức người dùng đã đăng nhập
  const token = getCookie('token')

  if (token) {
    callApiGetUserIdByToken(token)
    // window.location.href = 'addressMap.html'
    // console.log(window.location.href)
  }
  //----Phần này làm sau khi đã làm trang info.js

  //Sự kiện bấm nút login
  $('#login-btn').on('click', function () {
    // console.log('submit')
    var user = {
      username: $('#input-username').val().trim(),
      password: $('#input-password').val().trim(),
    }
    if (validate(user)) {
      signin(user)
    }
  })

  function signin(user) {
    var loginUrl = gAPI_URL + 'login'
    //API theo đặc tả sẽ call theo data-form chứ không phải kiểu json nên cần khai báo formdata
    $.ajax({
      type: 'POST',
      url: loginUrl,
      data: JSON.stringify(user),
      contentType: 'application/json; charset=utf-8',
      //   dataType: 'json',
      success: function (responseObject) {
        // console.log(responseObject)

        responseHandler(responseObject)
      },
      error: function (xhr) {
        // Lấy error message
        // alert(xhr.responseText)
        // console.log(xhr)
      },
    })
  }

  //Xử lý object trả về khi login thành công
  function responseHandler(data) {
    //Lưu token vào cookie trong 1 ngày
    setCookie('token', data, 1)
    // console.log(document.cookie)
    callApiGetUserIdByToken(data)
    // window.location.href = 'addressMap.html'
  }

  function callApiGetUserIdByToken(paramToken) {
    $.ajax({
      url: gAPI_URL + 'findUserId/Token',
      type: 'GET',
      dataType: 'json',
      contentType: 'application/json; charset=utf-8',
      headers: {
        Authorization: 'Token ' + paramToken,
      },
      success: function (responseObject) {
        // console.log('user id: ', responseObject)
        getUserInfo(responseObject)
      },
      error: function (pAjaxContext) {
        //redirectToLogin();
      },
    })
  }

  // get user info to redirect
  function getUserInfo(paramId) {
    $.ajax({
      type: 'get',
      url: gAPI_URL + 'user/' + paramId,
      dataType: 'json',
      async: false,
      success: function (response) {
        // console.log(response)
        // console.log(response.roles[0].roleKey)
        let userRole = response.roles[0].roleKey
        handleUserAuth(userRole)
      },
    })
  }
  // xử lý redirect trang theo user role
  function handleUserAuth(paramRole) {
    switch (paramRole) {
      case "ROLE_ADMIN":
        window.location.href = "addressMap.html"
        break;
    
      case "ROLE_HOMESELLER":
        window.location.href = "customersList.html"
        break;
    
      default:
        window.location.href = "index.html"
        break;
    }
  }

  //Hàm setCookie đã giới thiệu ở bài trước
  function setCookie(cname, cvalue, exdays) {
    var d = new Date()
    d.setTime(d.getTime() + exdays * 24 * 60 * 60 * 1000)
    var expires = 'expires=' + d.toUTCString()
    document.cookie = cname + '=' + cvalue + ';' + expires + ';path=/'

    // console.log(cname)
    // console.log(cvalue)
    // console.log(exdays)
  }

  //Hiển thị lỗi lên form
  function showError(message) {
    var errorElement = $('#error')

    errorElement.html(message)
    errorElement.addClass('d-block')
    errorElement.addClass('d-none')
  }

  //Validate dữ liệu từ form
  function validateForm(username, password) {
    if (username == '') {
      // alert('Username không phù hợp')
      return false
    }

    if (password === '') {
      // alert('Password không phù hợp')
      return false
    }

    return true
  }

  //Validate dữ liệu từ form
  function validate(user) {
    if (user.username == '') {
      // alert('Username không phù hợp')
      return false
    }

    if (user.password === '') {
      // alert('Password không phù hợp')
      return false
    }

    return true
  }

  // Hàm validate email bằng regex
  // function validateEmail(email) {
  //     const regex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  //     return regex.test(String(email).toLowerCase());
  // }

  //Hàm get Cookie đã giới thiệu ở bài trước
  function getCookie(cname) {
    var name = cname + '='
    var decodedCookie = decodeURIComponent(document.cookie)
    var ca = decodedCookie.split(';')
    for (var i = 0; i < ca.length; i++) {
      var c = ca[i]
      while (c.charAt(0) == ' ') {
        c = c.substring(1)
      }
      if (c.indexOf(name) == 0) {
        return c.substring(name.length, c.length)
      }
    }
    return ''
  }
})
