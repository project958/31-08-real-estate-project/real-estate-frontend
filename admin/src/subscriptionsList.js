'use strict'
$(document).ready(function () {
  /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
  const gAPI_URL = 'http://localhost:8080/'

  let gStt = 0
  let gEstate = []

  let gDataTable = $('#data-table').DataTable({
    columns: [
      { data: 'id' },
      { data: 'name' },
      { data: 'email' },
      { data: 'mobile' },
      { data: 'estateId' },
      { data: 'status' },
    ],
    columnDefs: [
      {
        targets: 0,
        render: renderStt,
      },
      {
        targets: 5,
        render: renderStatus,
      },
      {
        targets: 4,
        render: getNameById,
      },

      {
        targets: 6,
        defaultContent: `<i id="btn-delete" class="fas fa-trash text-danger" title="Delete"></i>`,
      },
    ],
    order: [[0, 'desc']],
    // dom: 'Bfrtip',
    // buttons: ['copy', 'csv', 'excel', 'pdf', 'print', 'colvis'],
    lengthMenu: [
      [10, 25, 50, -1],
      [10, 25, 50, 'All'],
    ],
    autoWidth: false,
  })

  // Authorization
  const token = getCookie('token')
  // console.log('token', token)

  // Khai báo xác thực ở headers
  var headers = {
    Authorization: 'Token ' + token,
  }

  // crud
  let gData = {
    user: '',
    endpoint: '',
    publickey: '',
    authenticationtoken: '',
    contentencoding: '',
  }
  let gDataId = ''

  /*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */

  onPageLoading()
  // gán sự kiện nút create
  $('#create-data').click(onCreateClick)
  // gán sự kiện nút edit
  $('#data-table').on('click', '#btn-edit', onEditBtnClick)
  // gán sự kiện nút save update
  $('#btn-save').click(onSaveUpdateClick)
  // gán sự kiện nút delete
  $('#data-table').on('click', '#btn-delete', onDeleteClick)
  // gán sự kiện nút confirm delete
  $(document).on('click', '#delete-data', onConfirmDeleteClick)
  // gán sự kiện thay đổi status
  $('#data-table').on('click', 'input[name=status]', onStatusCheckboxChange)

  /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */

  function onPageLoading() {
    getAllDataToTable()
    getAllEstate()
  }

  /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/

  //Hàm get Cookie đã giới thiệu ở bài trước
  function getCookie(cname) {
    var name = cname + '='
    var decodedCookie = decodeURIComponent(document.cookie)
    var ca = decodedCookie.split(';')
    for (var i = 0; i < ca.length; i++) {
      var c = ca[i]
      while (c.charAt(0) == ' ') {
        c = c.substring(1)
      }
      if (c.indexOf(name) == 0) {
        return c.substring(name.length, c.length)
      }
    }
    return ''
  }

  // get all data
  function getAllDataToTable() {
    $.ajax({
      type: 'get',
      url: gAPI_URL + 'customer-requests',
      headers: headers,
      dataType: 'json',
      success: function (response) {
        loadToDataTable(response)
      },
    })
  }

  // get all estate
  function getAllEstate() {
    $.ajax({
      type: 'get',
      url: gAPI_URL + 'real-estatesAll',
      dataType: 'json',
      headers: headers,
      async: false,
      success: function (response) {
        // console.log(response)
        gEstate = response
      },
    })
  }

  // load data to table
  function loadToDataTable(paramData) {
    gStt = 0
    // console.log(paramData)
    gDataTable.clear()
    gDataTable.rows.add(paramData)
    gDataTable.draw()
  }

  // render  name by id
  function getNameById(paramId) {
    for (var bI = 0; bI < gEstate.length; bI++) {
      if (gEstate[bI].id == paramId) {
        return gEstate[bI].address
      }
    }
  }
  // render status
  function renderStatus(paramId) {
    if (paramId == 0) {
      return `        
      <input type="checkbox" class="form-check-input" name="status" id="status-check" style="width:20px; height:20px" >`
    } else {
      return `        
      <input type="checkbox" class="form-check-input" name="status" id="status-check"
      checked style="width:20px; height:20px" disabled>`
    }
  }

  // check status change
  function onStatusCheckboxChange(e) {
    e.stopPropagation()
    let vSelectedRow = $(this).parents('tr')
    let vSelectedData = gDataTable.row(vSelectedRow).data()
    let dataId = vSelectedData.id
    // console.log('dataId', dataId)
    vSelectedData.status = e.target.checked ? 1 : 0
    // console.log(vSelectedData)

    Swal.fire({
      title: 'Vui lòng xác nhận?',
      position: 'top',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Xác nhận',
    }).then((result) => {
      if (result.isConfirmed) {
        // call api update
        $.ajax({
          type: 'put',
          url: gAPI_URL + 'customer-request/' + dataId,
          headers: headers,
          data: JSON.stringify(vSelectedData),
          async: false,
          contentType: 'application/json; charset=utf-8',
          success: function (response) {
            // console.log(response)
            Swal.fire({
              position: 'top',
              icon: 'success',
              title: 'Cập nhật thành công',
              showConfirmButton: false,
              timer: 1500,
            })
            $.get(gAPI_URL + 'customer-requests', loadToDataTable)
          },
          error: function (error) {
            // console.log(error)
          },
        })
      } else {
        $(this).prop('checked', true ? false : true)
      }
    })
  }

  // render stauts
  function renderStt(params) {
    gStt++
    return gStt
  }

  // create
  function onCreateClick(e) {
    //get data
    getDataInput(gData)
    // console.log(gData)
    if ($('#create-form').valid()) {
      Swal.fire({
        title: 'Vui lòng xác nhận?',
        position: 'top',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Xác nhận',
      }).then((result) => {
        if (result.isConfirmed) {
          $.ajax({
            type: 'post',
            url: gAPI_URL + 'customer-request',
            headers: headers,
            data: JSON.stringify(gData),
            contentType: 'application/json; charset=utf-8',
            success: function (respond) {
              Swal.fire({
                position: 'top',
                icon: 'success',
                title: 'Tạo mới thành công',
                showConfirmButton: false,
                timer: 1500,
              })
              // console.log('respond', respond)
              resetDataForm()
              $('#modal-create').modal('hide')
              getAllDataToTable()
            },
            error: function (error) {
              // alert(error.statusText)
              // console.log(error.responseText)
            },
          })
        }
      })
    }
  }

  // update article
  function onEditBtnClick(e) {
    let vSelectedRow = $(this).parents('tr')
    let vSelectedData = gDataTable.row(vSelectedRow).data()
    gDataId = vSelectedData.id
    // console.log('gDataId', gDataId)
    // console.log('gData', vSelectedData)
    $.ajax({
      type: 'get',
      headers: headers,
      async: false,
      url: gAPI_URL + `customer-request/${gDataId}`,
      dataType: 'json',
      success: function (response) {
        loadDataToInput(response)
      },
    })
    $('#modal-update').modal('show')
  }

  // call api update
  function onSaveUpdateClick(e) {
    getDataInputUpdate(gData)
    if ($('#update-form').valid()) {
      Swal.fire({
        title: 'Vui lòng xác nhận?',
        position: 'top',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Xác nhận',
      }).then((result) => {
        if (result.isConfirmed) {
          // console.log('gData', gData)
          // call api update
          $.ajax({
            type: 'put',
            url: gAPI_URL + 'customer-request/' + gDataId,
            headers: headers,
            data: JSON.stringify(gData),
            async: false,
            contentType: 'application/json; charset=utf-8',
            success: function (response) {
              // console.log(response)
              Swal.fire({
                position: 'top',
                icon: 'success',
                title: 'Cập nhật thành công',
                showConfirmButton: false,
                timer: 1500,
              })
              $('#modal-update').modal('hide')
              getAllDataToTable()
            },
            error: function (error) {
              // console.log(error.responseText)
            },
          })
        }
      })
    }
  }

  // delete article
  function onDeleteClick(e) {
    $('#modal-delete-data').modal('show')
    let vSelectedRow = $(this).parents('tr')
    let vSelectedData = gDataTable.row(vSelectedRow).data()
    gDataId = vSelectedData.id
  }

  // confirm delete data
  function onConfirmDeleteClick() {
    $.ajax({
      url: gAPI_URL + `customer-request/${gDataId}`,
      headers: headers,
      method: 'DELETE',
      success: () => {
        Swal.fire({
          position: 'top',
          icon: 'success',
          title: 'Xóa thành công',
          showConfirmButton: false,
          timer: 1500,
        })
        $('#modal-delete-data').modal('hide')
        getAllDataToTable()
      },
      // error: (err) => console.log(err.responseText),
    })
  }

  // get data input
  function getDataInput(paramObj) {
    paramObj.user = $('#select-user').val()
    paramObj.endpoint = $('#input-endpoint').val().trim()
    paramObj.publickey = $('#input-publickey').val().trim()
    paramObj.authenticationtoken = $('#input-authenticationtoken').val().trim()
    paramObj.contentencoding = $('#input-contentencoding').val().trim()
  }
  // get data input update
  function getDataInputUpdate(paramObj) {
    paramObj.user = $('#select-user-update').val()
    paramObj.endpoint = $('#input-endpoint-update').val().trim()
    paramObj.publickey = $('#input-publickey-update').val().trim()
    paramObj.authenticationtoken = $('#input-authenticationtoken-update').val().trim()
    paramObj.contentencoding = $('#input-contentencoding-update').val().trim()
  }

  // load data input
  function loadDataToInput(paramObj) {
    $('#select-user-update').val(paramObj.user)
    $('#input-endpoint-update').val(paramObj.endpoint)
    $('#input-publickey-update').val(paramObj.publickey)
    $('#input-authenticationtoken-update').val(paramObj.authenticationtoken)
    $('#input-contentencoding-update').val(paramObj.contentencoding)
  }
  // reset form
  function resetDataForm() {
    $('#select-user').val('')
    $('#input-endpoint').val('')
    $('#input-publickey').val('')
    $('#input-authenticationtoken').val('')
    $('#input-contentencoding').val('')
  }

  // validate data
  $('#create-form').validate({
    rules: {
      user: {
        required: true,
      },
      endpoint: 'required',
      publickey: 'required',
      authenticationtoken: 'required',
      contentencoding: 'required',
    },
    messages: {
      user: 'Vui lòng chọn user',
      endpoint: 'Vui lòng nhập endpoint',
      publickey: 'Vui lòng nhập publickey',
      authenticationtoken: 'Vui lòng nhập authenticationtoken',
      contentencoding: 'Vui lòng nhập contentencoding',
    },
  })
  // validate data update
  $('#update-form').validate({
    rules: {
      user: {
        required: true,
      },
      endpoint: 'required',
      publickey: 'required',
      authenticationtoken: 'required',
      contentencoding: 'required',
    },
    messages: {
      user: 'Vui lòng chọn user',
      endpoint: 'Vui lòng nhập endpoint',
      publickey: 'Vui lòng nhập publickey',
      authenticationtoken: 'Vui lòng nhập authenticationtoken',
      contentencoding: 'Vui lòng nhập contentencoding',
    },
  })

  $('#cancel-data').click(function (e) {
    e.preventDefault()
    resetDataForm()
    // console.log('cancel')
  })

  $('#cancel-data-update').click(function (e) {
    e.preventDefault()
    $('#modal-update').modal('hide')
  })
})
