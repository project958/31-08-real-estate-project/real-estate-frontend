$(document).ready(function () {
    
    //Set sự kiện cho nút logout
    $("#btn-logout").on("click", function() {
        redirectToLogin();
    });

    function redirectToLogin() {
        // Trước khi logout cần xóa token đã lưu trong cookie
        setCookie("token", "", 1);
        window.location.href = "index.html";
    }    

    //Hàm setCookie đã giới thiệu ở bài trước
    function setCookie(cname, cvalue, exdays) {
        var d = new Date();
        d.setTime(d.getTime() + (exdays*24*60*60*1000));
        var expires = "expires="+ d.toUTCString();
        document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
    }
})