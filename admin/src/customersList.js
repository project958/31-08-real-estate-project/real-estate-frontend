'use strict'
$(document).ready(function () {
  /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
  const gAPI_URL = 'http://localhost:8080/'
  
  let gProject = []
  let gStt = 0
  let gUserId
  let gUserRole

  let gDataTable = $('#data-table').DataTable({
    columns: [
      { data: 'id' },
      { data: 'contactTitle' },
      { data: 'contactName' },
      { data: 'address' },
      { data: 'mobile' },
      { data: 'email' },
      { data: 'createBy' },
      { data: 'updateBy' },
      { data: 'createDate' },
      { data: 'updateDate' },
    ],
    columnDefs: [
      {
        targets: 10,
        defaultContent: `<a id="btn-detail" class="btn btn-info btn-sm"> Chi tiết BĐS </a> | <i id="btn-edit" class="fas fa-edit text-primary" title="Edit"></i> | <i id="btn-delete" class="fas fa-trash text-danger" title="Delete"></i>`,
      },
      {
        targets: 0,
        render: renderStt,
      },
      {
        targets: 6,
        render: renderUsernameById,
      },
      {
        targets: 7,
        render: renderUsernameById,
      },
    ],
    order: [[0, 'desc']],
    // dom: 'Bfrtip',
    // buttons: ['copy', 'csv', 'excel', 'pdf', 'print', 'colvis'],
    lengthMenu: [
      [10, 25, 50, -1],
      [10, 25, 50, 'All'],
    ],
    autoWidth: false,
    // scrollX: true,
  })

  // Authorization

  const token = getCookie('token')
  // console.log('token', token)

  // Khai báo xác thực ở headers
  var headers = {
    Authorization: 'Token ' + token,
  }

  if (token) {
    callApiGetUserIdByToken(token)
  } else {
    window.location.href = 'index.html'
  }

  /*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */

  onPageLoading()
  // gán sự kiện nút create
  $('#create-data').click(onCreateClick)
  // gán sự kiện nút edit
  $('#data-table').on('click', '#btn-edit', onEditBtnClick)
  // gán sự kiện nút save update
  $('#btn-save').click(onSaveUpdateClick)
  // gán sự kiện nút delete
  $('#data-table').on('click', '#btn-delete', onDeleteClick)
  // gán sự kiện nút confirm delete
  $(document).on('click', '#delete-data', onConfirmDeleteClick)
  // gán sự kiện nút detail
  $('#data-table').on('click', '#btn-detail', onBtnDetailClick)

  /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */

  function onPageLoading() {
    getAllDataToTable()
  }

  /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/

  // get user id
  function callApiGetUserIdByToken(paramToken) {
    $.ajax({
      url: gAPI_URL + 'findUserId/Token',
      type: 'GET',
      dataType: 'json',
      contentType: 'application/json; charset=utf-8',
      headers: {
        Authorization: 'Token ' + paramToken,
      },
      success: function (responseObject) {
        // console.log('user id: ', responseObject)
        gUserId = responseObject
        getUserInfo(responseObject)
      },
      error: function (pAjaxContext) {
        //redirectToLogin();
      },
    })
  }

  // get user info to redirect
  function getUserInfo(paramId) {
    $.ajax({
      type: 'get',
      url: gAPI_URL + 'user/' + paramId,
      dataType: 'json',
      async: false,
      success: function (response) {
        // console.log(response)
        gUserRole = response.roles[0].roleKey
        // console.log(gUserRole)
        $('.info .d-block').html(response.username)
        handleUserAuth(gUserRole)
      },
    })
  }

  // handle if user authorize
  function handleUserAuth(paramRole) {
    switch (paramRole) {
      case 'ROLE_HOMESELLER':
        $.ajax({
          type: 'get',
          url: gAPI_URL + 'customer/createby/' + gUserId,
          headers: headers,
          dataType: 'json',
          success: function (response) {
            loadToDataTable(response)
          },
        })
        break
      default:
        getAllDataToTable()
        break
    }
  }

  //Hàm get Cookie
  function getCookie(cname) {
    var name = cname + '='
    var decodedCookie = decodeURIComponent(document.cookie)
    var ca = decodedCookie.split(';')
    for (var i = 0; i < ca.length; i++) {
      var c = ca[i]
      while (c.charAt(0) == ' ') {
        c = c.substring(1)
      }
      if (c.indexOf(name) == 0) {
        return c.substring(name.length, c.length)
      }
    }
    return ''
  }

  // get all data
  function getAllDataToTable() {
    $.ajax({
      type: 'get',
      url: gAPI_URL + 'customers',
      headers: headers,
      dataType: 'json',
      success: function (response) {
        loadToDataTable(response)
      },
    })
  }

  // render stauts
  function renderStt(params) {
    gStt++
    return gStt
  }
  // render username
  function renderUsernameById(paramId) {
    if (paramId) {
      let name = ''
      $.ajax({
        type: 'get',
        url: gAPI_URL + 'user/' + paramId,
        dataType: 'json',
        async: false,
        success: function (response) {
          name = response.username
        },
      })
      return name
    } else {
      return null
    }
  }

  // load data to table
  function loadToDataTable(paramData) {
    gStt = 0
    // console.log(paramData)
    gDataTable.clear()
    gDataTable.rows.add(paramData)
    gDataTable.draw()
  }

  // crud
  let gData = {
    contactTitle: '',
    contactName: '',
    address: '',
    mobile: '',
    email: '',
    createBy: '',
    updateBy: '',
    username: '',
    password: '',
  }
  let gDataId = ''

  // create
  function onCreateClick(e) {
    //get data
    e.preventDefault()
    getDataInput(gData)
    // console.log(gData)
    if ($('#create-form').valid()) {
      Swal.fire({
        title: 'Vui lòng xác nhận?',
        position: 'top',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Xác nhận',
      }).then((result) => {
        if (result.isConfirmed) {
        $.ajax({
          type: 'post',
          headers: headers,
          url: gAPI_URL + 'customer',
          data: JSON.stringify(gData),
          contentType: 'application/json; charset=utf-8',
          success: function (respond) {
            Swal.fire({
                position: 'top',
                icon: 'success',
                title: 'Tạo mới thành công',
                showConfirmButton: false,
                timer: 1500,
              })
            // console.log('respond', respond)
            $('#modal-create').modal('hide')
            resetDataForm()
            handleUserAuth(gUserRole)
          },
          error: function (error) {
            // console.log(error.responseText)
            // alert(error.statusText)
            },
          })
        }
      })
    }
  }

  // update article
  function onEditBtnClick(e) {
    let vSelectedRow = $(this).parents('tr')
    let vSelectedData = gDataTable.row(vSelectedRow).data()
    gDataId = vSelectedData.id
    // console.log('vSelectedData', vSelectedData)
    // console.log('gDataId', gDataId)
    $.ajax({
      type: 'get',
      headers: headers,
      async: false,
      url: gAPI_URL + `customer/${gDataId}`,
      dataType: 'json',
      success: function (response) {
        loadDataToInput(response)
      },
    })
    $('#modal-update').modal('show')
  }

  // btn detail estate
  function onBtnDetailClick(e) {
    let vSelectedRow = $(this).parents('tr')
    let vSelectedData = gDataTable.row(vSelectedRow).data()
    gDataId = vSelectedData.id
    // console.log('vSelectedData', vSelectedData)
    // console.log('gDataId', gDataId)
    $.ajax({
      type: 'get',
      headers: headers,
      async: false,
      url: gAPI_URL + `customer/${gDataId}`,
      dataType: 'json',
      success: function (response) {
        loadDataToInput(response)
      },
    })
    // $('#modal-detail').modal('show')
    // hiển thị ra console
    var vOrderUrl = './realestateList.html?' + 'customerId=' + gDataId
    window.location.href = vOrderUrl
  }

  // call api update
  function onSaveUpdateClick(e) {
    getDataInputUpdate(gData)
    if ($('#update-form').valid()) {
      Swal.fire({
        title: 'Vui lòng xác nhận?',
        position: 'top',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Xác nhận',
      }).then((result) => {
        if (result.isConfirmed) {
        // console.log('gData', gData)
        // call api update
        $.ajax({
          type: 'put',
          headers: headers,
          url: gAPI_URL + 'customer/' + gDataId,
          data: JSON.stringify(gData),
          async: false,
          contentType: 'application/json; charset=utf-8',
          success: function (response) {
            // console.log(response)
            Swal.fire({
                position: 'top',
                icon: 'success',
                title: 'Cập nhật thành công',
                showConfirmButton: false,
                timer: 1500,
              })
            $('#modal-update').modal('hide')
            handleUserAuth(gUserRole)
          },
          error: function (error) {
            // console.log(error.responseText)
            },
          })
        }
      })
    }
  }

  // delete article
  function onDeleteClick(e) {
    $('#modal-delete-data').modal('show')
    let vSelectedRow = $(this).parents('tr')
    let vSelectedData = gDataTable.row(vSelectedRow).data()
    gDataId = vSelectedData.id
  }

  // confirm delete data
  function onConfirmDeleteClick() {
    $.ajax({
      url: gAPI_URL + `customer/${gDataId}`,
      method: 'DELETE',
      headers: headers,
      success: () => {
       Swal.fire({
          position: 'top',
          icon: 'success',
          title: 'Xóa thành công',
          showConfirmButton: false,
          timer: 1500,
        })
        $('#modal-delete-data').modal('hide')
        handleUserAuth(gUserRole)
      },
      // error: (err) => console.log(err.responseText),
    })
  }

  // get data input
  function getDataInput(paramObj) {
    paramObj.contactTitle = $('#input-title').val().trim()
    paramObj.contactName = $('#input-name').val().trim()
    paramObj.mobile = $('#input-phone').val().trim()
    paramObj.email = $('#input-email').val().trim()
    paramObj.username = $('#input-username').val().trim()
    paramObj.password = $('#input-password').val().trim()
    paramObj.address = $('#input-address').val().trim()
    paramObj.note = $('#input-note').val().trim()
    paramObj.createBy = gUserId
  }

  // get data input update
  function getDataInputUpdate(paramObj) {
    paramObj.contactTitle = $('#input-title-update').val().trim()
    paramObj.contactName = $('#input-name-update').val().trim()
    paramObj.mobile = $('#input-phone-update').val().trim()
    paramObj.email = $('#input-email-update').val().trim()
    paramObj.username = $('#input-username-update').val().trim()
    paramObj.password = $('#input-password-update').val().trim()
    paramObj.address = $('#input-address-update').val().trim()
    paramObj.note = $('#input-note-update').val().trim()
    paramObj.updateBy = gUserId
  }

  // load data input update
  function loadDataToInput(paramObj) {
    $('#input-title-update').val(paramObj.contactTitle)
    $('#input-name-update').val(paramObj.contactName)
    $('#input-phone-update').val(paramObj.mobile)
    $('#input-email-update').val(paramObj.email)
    $('#input-password-update').val(paramObj.password)
    $('#input-username-update').val(paramObj.username)
    $('#input-address-update').val(paramObj.address)
    $('#input-note-update').val(paramObj.note)
  }

  // reset form
  function resetDataForm() {
    $('#input-title').val('')
    $('#input-name').val('')
    $('#input-phone').val('')
    $('#input-email').val('')
    $('#input-password').val('')
    $('#input-username').val('')
    $('#input-address').val('')
    $('#input-note').val('')
  }

  // validate data
  $('#create-form').validate({
    rules: {
      name: 'required',
      username: 'required',
      password: 'required',
      email: {
        required: true,
        email: true,
      },
      phone: { number: true },
      title: 'required',
    },
    messages: {
      name: 'Vui lòng nhập tên',
      phone: { number: 'Vui lòng nhập số' },
      username: 'Vui lòng nhập username',
      password: 'Vui lòng nhập password',
      email: 'Vui lòng nhập email',
      title: 'Vui lòng nhập tên hiển thị',
    },
  })
  // validate data update
  $('#update-form').validate({
    rules: {
      name: 'required',
      username: 'required',
      password: 'required',
      email: {
        required: true,
        email: true,
      },
      phone: { number: true },
      title: 'required',
    },
    messages: {
      name: 'Vui lòng nhập tên',
      phone: { number: 'Vui lòng nhập số' },
      username: 'Vui lòng nhập username',
      password: 'Vui lòng nhập password',
      email: 'Vui lòng nhập email',
      title: 'Vui lòng nhập tên hiển thị',
    },
  })

  $('#cancel-data').click(function (e) {
    e.preventDefault()
    resetDataForm()
    // console.log('cancel')
  })

  $('#cancel-data-update').click(function (e) {
    e.preventDefault()
    $('#modal-update').modal('hide')
  })
})
