'use strict'
$(document).ready(function () {
  const gAPI_URL = 'http://localhost:8080/'
  
  let gStt = 0
  let gDataTable = $('#data-table').DataTable({
    columns: [{ data: 'id' }, { data: 'address' }, { data: 'lat' }, { data: 'lng' }],
    columnDefs: [
      {
        targets: 0,
        render: renderStt,
      },
    ],
    // dom: 'Bfrtip',
    // buttons: ['copy', 'csv', 'excel', 'pdf', 'print', 'colvis'],
    lengthMenu: [
      [10, 25, 50, -1],
      [10, 25, 50, 'All'],
    ],
    autoWidth: false,
  })
  // gDataTable.buttons().container().appendTo('#data-table_wrapper .col-md-6:eq(0)')

  // $.get(gAPI_URL + 'address-maps', loadToDataTable)
  $.ajax({
    type: 'get',
    url: gAPI_URL + 'address-maps',
    dataType: 'json',
    success: function (response) {
      loadToDataTable(response)
    },
  })

  // load data to table
  function loadToDataTable(paramData) {
    gStt = 0
    // console.log(paramData)
    gDataTable.clear()
    gDataTable.rows.add(paramData)
    gDataTable.draw()
  }

  // render stauts
  function renderStt(params) {
    gStt++
    return gStt
  }

  // get cookie
  const token = getCookie('token')

  if (token) {
    callApiGetUserIdByToken(token)
    // window.location.href = 'addressMap.html'
    // console.log(window.location.href)
  }

  function callApiGetUserIdByToken(paramToken) {
    $.ajax({
      url: gAPI_URL + 'findUserId/Token',
      type: 'GET',
      dataType: 'json',
      contentType: 'application/json; charset=utf-8',
      headers: {
        Authorization: 'Token ' + paramToken,
      },
      success: function (responseObject) {
        // console.log('user id: ', responseObject)
        getUserInfo(responseObject)
      },
      error: function (pAjaxContext) {
        //redirectToLogin();
      },
    })
  }

  // get user info to redirect
  function getUserInfo(paramId) {
    $.ajax({
      type: 'get',
      url: gAPI_URL + 'user/' + paramId,
      dataType: 'json',
      async: false,
      success: function (response) {
        // console.log(response)
        // console.log(response.roles[0].roleKey)
        let userRole = response.roles[0].roleKey
        handleUserAuth(userRole)
      },
    })
  }
  // xử lý redirect trang theo user role
  function handleUserAuth(paramRole) {
    switch (paramRole) {
      case 'ROLE_ADMIN':
        window.location.href = 'addressMap.html'
        break

      case 'ROLE_CUSTOMER':
        window.location.href = 'realestateList.html'
        break

      case 'ROLE_HOMESELLER':
        window.location.href = 'realestateList.html'
        break

      default:
        window.location.href = 'addressMap.html'
        break
    }
  }

  //Hàm setCookie đã giới thiệu ở bài trước
  function setCookie(cname, cvalue, exdays) {
    var d = new Date()
    d.setTime(d.getTime() + exdays * 24 * 60 * 60 * 1000)
    var expires = 'expires=' + d.toUTCString()
    document.cookie = cname + '=' + cvalue + ';' + expires + ';path=/'

    // console.log(cname)
    // console.log(cvalue)
    // console.log(exdays)
  }

  //Set sự kiện cho nút login
  $('#btn-login').on('click', function () {
    redirectToLogin()
  })

  function redirectToLogin() {
    // Trước khi logout cần xóa token đã lưu trong cookie
    setCookie('token', '', 1)
    window.location.href = 'login.html'
  }

  // //Hiển thị thông tin người dùng
  // function displayUser(data) {
  //     $("#firstname").val(data.firstname);
  //     $("#email").val(data.email);
  //     $("#lastname").val(data.lastname);
  // };

  //Hàm get Cookie đã giới thiệu ở bài trước
  function getCookie(cname) {
    var name = cname + '='
    var decodedCookie = decodeURIComponent(document.cookie)
    var ca = decodedCookie.split(';')
    for (var i = 0; i < ca.length; i++) {
      var c = ca[i]
      while (c.charAt(0) == ' ') {
        c = c.substring(1)
      }
      if (c.indexOf(name) == 0) {
        return c.substring(name.length, c.length)
      }
    }
    return ''
  }

  //Hàm setCookie đã giới thiệu ở bài trước
  function setCookie(cname, cvalue, exdays) {
    var d = new Date()
    d.setTime(d.getTime() + exdays * 24 * 60 * 60 * 1000)
    var expires = 'expires=' + d.toUTCString()
    document.cookie = cname + '=' + cvalue + ';' + expires + ';path=/'
  }
})
