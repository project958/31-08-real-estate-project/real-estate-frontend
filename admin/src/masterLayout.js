'use strict'
$(document).ready(function () {
  /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
  const gAPI_URL = 'http://localhost:8080/'
  
  let gProject = []
  let gStt = 0

  // Authorization
  const token = getCookie('token')
  // console.log('token', token)

  // Khai báo xác thực ở headers
  var headers = {
    Authorization: 'Token ' + token,
  }

  let gDataTable = $('#data-table').DataTable({
    columns: [
      { data: 'id' },
      { data: 'name' },
      { data: 'project' },
      { data: 'acreage' },
      { data: 'dateCreate' },
      { data: 'dateUpdate' },
    ],
    columnDefs: [
      {
        targets: 6,
        defaultContent: `<i id="btn-edit" class="fas fa-edit text-primary" title="Edit"></i> | <i id="btn-delete" class="fas fa-trash text-danger" title="Delete"></i>`,
      },
      {
        targets: 0,
        render: renderStt,
      },
      {
        targets: 2,
        // className: 'text-secondary',
        render: getProjectNameById,
      },
    ],
    // dom: 'Bfrtip',
    // buttons: ['copy', 'csv', 'excel', 'pdf', 'print', 'colvis'],
    lengthMenu: [
      [10, 25, 50, -1],
      [10, 25, 50, 'All'],
    ],
    autoWidth: false,
  })

  // crud
  let gData = {
    acreage: '',
    apartmentList: '',
    description: '',
    name: '',
    photo: '',
    project: '',
  }
  let gDataId = ''

  /*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */

  onPageLoading()
  // gán sự kiện nút create
  $('#create-data').click(onCreateClick)
  // gán sự kiện nút edit
  $('#data-table').on('click', '#btn-edit', onEditBtnClick)
  // gán sự kiện nút save update
  $('#btn-save').click(onSaveUpdateClick)
  // gán sự kiện nút delete
  $('#data-table').on('click', '#btn-delete', onDeleteClick)
  // gán sự kiện nút confirm delete
  $(document).on('click', '#delete-data', onConfirmDeleteClick)

  /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */

  function onPageLoading() {
    getAllDataToTable()
    getAllProject()
  }

  /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/

  //Hàm get Cookie đã giới thiệu ở bài trước
  function getCookie(cname) {
    var name = cname + '='
    var decodedCookie = decodeURIComponent(document.cookie)
    var ca = decodedCookie.split(';')
    for (var i = 0; i < ca.length; i++) {
      var c = ca[i]
      while (c.charAt(0) == ' ') {
        c = c.substring(1)
      }
      if (c.indexOf(name) == 0) {
        return c.substring(name.length, c.length)
      }
    }
    return ''
  }

  // get all data
  function getAllDataToTable() {
    $.ajax({
      type: 'get',
      url: gAPI_URL + 'master-layouts',
      headers: headers,
      dataType: 'json',
      success: function (response) {
        loadToDataTable(response)
      },
    })
  }

  // get project
  function getAllProject() {
    $.ajax({
      type: 'get',
      url: gAPI_URL + 'projects',
      headers: headers,
      dataType: 'json',
      async: false,
      success: function (response) {
        // console.log('project', response)
        for (const project of response) {
          $('#select-project').append(
            $('<option>', {
              value: project.id,
              text: project.name,
            })
          )
          $('#select-project-update').append(
            $('<option>', {
              value: project.id,
              text: project.name,
            })
          )
        }
        gProject = response
      },
    })
  }
  // // change project select
  // $('#select-project').change(function (e) {
  //   gProjectId = $('#select-project').val()
  //   if (gProjectId) {
  //     $.ajax({
  //       type: 'get',
  //       url: gAPI_URL + 'project/' + gProjectId,
  //       dataType: 'json',
  //       success: function (response) {
  //         // console.log('project', response)
  //         gData.project = response.id
  //       },
  //     })
  //   }
  // })

  // render project name by id
  function getProjectNameById(paramId) {
    for (var bI = 0; bI < gProject.length; bI++) {
      if (gProject[bI].id == paramId) {
        return gProject[bI].name
      }
    }
  }

  // render stauts
  function renderStt(params) {
    gStt++
    return gStt
  }

  // load data to table
  function loadToDataTable(paramData) {
    gStt = 0
    // console.log(paramData)
    gDataTable.clear()
    gDataTable.rows.add(paramData)
    gDataTable.draw()
  }

  

  // create
  function onCreateClick(e) {
    //get data
    e.preventDefault()
    getDataInput(gData)
    // console.log(gData)
    if ($('#create-form').valid()) {
      Swal.fire({
        title: 'Vui lòng xác nhận?',
        position: 'top',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Xác nhận',
      }).then((result) => {
        if (result.isConfirmed) {
        $.ajax({
          type: 'post',
          headers: headers,
          url: gAPI_URL + 'master-layout',
          data: JSON.stringify(gData),
          contentType: 'application/json; charset=utf-8',
          success: function (respond) {
            Swal.fire({
                position: 'top',
                icon: 'success',
                title: 'Tạo mới thành công',
                showConfirmButton: false,
                timer: 1500,
              })
            // console.log('respond', respond)
            $('#modal-create').modal('hide')
            resetDataForm()
            getAllDataToTable()
          },
          error: function (error) {
            // alert(error.statusText)
            // console.log(error.responseText)
            },
          })
        }
      })
    }
  }

  // update article
  function onEditBtnClick(e) {
    let vSelectedRow = $(this).parents('tr')
    let vSelectedData = gDataTable.row(vSelectedRow).data()
    gDataId = vSelectedData.id
    // console.log('vSelectedData', vSelectedData)
    // console.log('gDataId', gDataId)
    $.ajax({
      type: 'get',
      headers: headers,
      async: false,
      url: gAPI_URL + `master-layout/${gDataId}`,
      dataType: 'json',
      success: function (response) {
        loadDataToInput(response)
      },
    })
    $('#modal-update').modal('show')
  }

  // call api update
  function onSaveUpdateClick(e) {
    getDataInputUpdate(gData)
    if ($('#update-form').valid()) {
      Swal.fire({
        title: 'Vui lòng xác nhận?',
        position: 'top',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Xác nhận',
      }).then((result) => {
        if (result.isConfirmed) {
        // console.log('gData', gData)
        // call api update
        $.ajax({
          type: 'put',
          url: gAPI_URL + 'master-layout/' + gDataId,
          headers: headers,
          data: JSON.stringify(gData),
          async: false,
          contentType: 'application/json; charset=utf-8',
          success: function (response) {
            // console.log(response)
            Swal.fire({
                position: 'top',
                icon: 'success',
                title: 'Cập nhật thành công',
                showConfirmButton: false,
                timer: 1500,
              })
            $('#modal-update').modal('hide')
            getAllDataToTable()
          },
          error: function (error) {
            // console.log(error.responseText)
            },
          })
        }
      })
    }
  }

  // delete article
  function onDeleteClick(e) {
    $('#modal-delete-data').modal('show')
    let vSelectedRow = $(this).parents('tr')
    let vSelectedData = gDataTable.row(vSelectedRow).data()
    gDataId = vSelectedData.id
  }

  // confirm delete data
  function onConfirmDeleteClick() {
    $.ajax({
      url: gAPI_URL + `master-layout/${gDataId}`,
      headers: headers,
      method: 'DELETE',
      success: () => {
       Swal.fire({
          position: 'top',
          icon: 'success',
          title: 'Xóa thành công',
          showConfirmButton: false,
          timer: 1500,
        })
        $('#modal-delete-data').modal('hide')
        getAllDataToTable()
      },
      // error: (err) => console.log(err.responseText),
    })
  }

  // get data input
  function getDataInput(paramObj) {
    paramObj.name = $('#input-name').val().trim()
    paramObj.acreage = $('#input-acreage').val().trim()
    paramObj.apartmentList = $('#input-apartmentList').val().trim()
    paramObj.description = $('#input-description').val().trim()
    paramObj.photo = $('#input-photo').val().trim()
    paramObj.project = $('#select-project').val()
  }
  // get data input update
  function getDataInputUpdate(paramObj) {
    paramObj.name = $('#input-name-update').val().trim()
    paramObj.acreage = $('#input-acreage-update').val().trim()
    paramObj.apartmentList = $('#input-apartmentList-update').val().trim()
    paramObj.description = $('#input-description-update').val().trim()
    paramObj.photo = $('#input-photo-update').val().trim()
    paramObj.project = $('#select-project-update').val().trim()
  }

  // load data input update
  function loadDataToInput(paramObj) {
    $('#input-name-update').val(paramObj.name)
    $('#input-acreage-update').val(paramObj.acreage)
    $('#input-apartmentList-update').val(paramObj.apartmentList)
    $('#input-description-update').val(paramObj.description)
    $('#input-photo-update').val(paramObj.photo)
    $('#select-project-update').val(paramObj.project)
  }

  // reset form
  function resetDataForm() {
    $('#input-name').val('')
    $('#input-acreage').val('')
    $('#input-apartmentList').val('')
    $('#input-description').val('')
    $('#input-photo').val('')
    $('#select-project').val('')
  }

  // validate data
  $('#create-form').validate({
    rules: {
      name: 'required',
      project: {
        required: true,
      },
    },
    messages: {
      name: 'Vui lòng nhập tên',
      project: {
        required: 'Vui lòng chọn dự án',
      },
    },
  })
  // validate data update
  $('#update-form').validate({
    rules: {
      name: 'required',
      project: {
        required: true,
      },
    },
    messages: {
      name: 'Vui lòng nhập tên',
      project: {
        required: 'Vui lòng chọn dự án',
      },
    },
  })

  $('#cancel-data').click(function (e) {
    e.preventDefault()
    resetDataForm()
    // console.log('cancel')
  })

  $('#cancel-data-update').click(function (e) {
    e.preventDefault()
    $('#modal-update').modal('hide')
  })
})
