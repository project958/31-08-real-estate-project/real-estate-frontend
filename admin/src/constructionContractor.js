'use strict'
$(document).ready(function () {
  /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
  const gAPI_URL = 'http://localhost:8080/'
  
  // Authorization
  const token = getCookie('token')
  // console.log('token', token)
  // Khai báo xác thực ở headers
  var headers = {
    Authorization: 'Token ' + token,
  }

  let gStt = 0

  // crud
  let gData = {
    name: '',
    address: '',
    phone: '',
    phone2: '',
    fax: '',
    email: '',
    description: '',
    project: '',
    website: '',
    note: '',
  }
  let gDataId = ''

  let gDataTable = $('#data-table').DataTable({
    columns: [
      { data: 'id' },
      { data: 'name' },
      { data: 'address' },
      { data: 'phone' },
      { data: 'phone2' },
      { data: 'fax' },
      { data: 'email' },
    ],
    columnDefs: [
      {
        targets: 7,
        defaultContent: `<i id="btn-edit" class="fas fa-edit text-primary" title="Edit"></i> | <i id="btn-delete" class="fas fa-trash text-danger" title="Delete"></i>`,
      },
      {
        targets: 0,
        render: renderStt,
      },
    ],
    // dom: 'Bfrtip',
    // buttons: ['copy', 'csv', 'excel', 'pdf', 'print', 'colvis'],
    lengthMenu: [
      [10, 25, 50, -1],
      [10, 25, 50, 'All'],
    ],
    autoWidth: false,
  })

  /*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */

  onPageLoading()
  // gán sự kiện nút create
  $('#create-data').click(onCreateClick)
  // gán sự kiện nút edit
  $('#data-table').on('click', '#btn-edit', onEditBtnClick)
  // gán sự kiện nút save update
  $('#btn-save').click(onSaveUpdateClick)
  // gán sự kiện nút delete
  $('#data-table').on('click', '#btn-delete', onDeleteClick)
  // gán sự kiện nút confirm delete
  $(document).on('click', '#delete-data', onConfirmDeleteClick)

  /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */

  function onPageLoading() {
    getAllDataToTable()
  }

  /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/

  //Hàm get Cookie 
  function getCookie(cname) {
    var name = cname + '='
    var decodedCookie = decodeURIComponent(document.cookie)
    var ca = decodedCookie.split(';')
    for (var i = 0; i < ca.length; i++) {
      var c = ca[i]
      while (c.charAt(0) == ' ') {
        c = c.substring(1)
      }
      if (c.indexOf(name) == 0) {
        return c.substring(name.length, c.length)
      }
    }
    return ''
  }

  // get all data
  function getAllDataToTable() {
    $.ajax({
      type: 'get',
      url: gAPI_URL + 'construction-contractors',
      headers: headers,
      dataType: 'json',
      success: function (response) {
        loadToDataTable(response)
      },
    })
  }

  // render stauts
  function renderStt(params) {
    gStt++
    return gStt
  }

  // load data to table
  function loadToDataTable(paramData) {
    gStt = 0
    // console.log(paramData)
    gDataTable.clear()
    gDataTable.rows.add(paramData)
    gDataTable.draw()
  }

  // create
  function onCreateClick(e) {
    //get data
    e.preventDefault()
    getDataInput(gData)
    // console.log(gData)
    //validate data
    if ($('#create-form').valid()) {
      Swal.fire({
        title: 'Vui lòng xác nhận?',
        position: 'top',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Xác nhận',
      }).then((result) => {
        if (result.isConfirmed) {
        $.ajax({
          type: 'post',
          headers: headers,
          url: gAPI_URL + 'construction-contractor',
          data: JSON.stringify(gData),
          contentType: 'application/json; charset=utf-8',
          success: function (respond) {
            Swal.fire({
                position: 'top',
                icon: 'success',
                title: 'Tạo mới thành công',
                showConfirmButton: false,
                timer: 1500,
              })
            // console.log('respond', respond)
            $('#modal-create').modal('hide')
            resetDataForm()
            getAllDataToTable()
          },
          error: function (error) {
            // console.log(error.responseText)
            // alert(error.statusText)
            },
          })
        }
      })
    }
  }

  // update article
  function onEditBtnClick(e) {
    let vSelectedRow = $(this).parents('tr')
    let vSelectedData = gDataTable.row(vSelectedRow).data()
    gDataId = vSelectedData.id
    // console.log('vSelectedData', vSelectedData)
    // console.log('gDataId', gDataId)
    $.ajax({
      type: 'get',
      headers: headers,
      async: false,
      url: gAPI_URL + `construction-contractor/${gDataId}`,
      dataType: 'json',
      success: function (response) {
        loadDataToInputUpdate(response)
      },
    })
    $('#modal-update').modal('show')
  }

  // call api update
  function onSaveUpdateClick(e) {
    getDataInputUpdate(gData)
    if ($('#update-form').valid()) {
      Swal.fire({
        title: 'Vui lòng xác nhận?',
        position: 'top',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Xác nhận',
      }).then((result) => {
        if (result.isConfirmed) {
        // console.log('gData', gData)
        // call api update
        $.ajax({
          type: 'put',
          headers: headers,
          url: gAPI_URL + 'construction-contractor/' + gDataId,
          data: JSON.stringify(gData),
          async: false,
          contentType: 'application/json; charset=utf-8',
          success: function (response) {
            // console.log(response)
            Swal.fire({
                position: 'top',
                icon: 'success',
                title: 'Cập nhật thành công',
                showConfirmButton: false,
                timer: 1500,
              })
            $('#modal-update').modal('hide')
            getAllDataToTable()
          },
          error: function (error) {
            // console.log(error.responseText)
            },
          })
        }
      })
    }
  }

  // delete article
  function onDeleteClick(e) {
    $('#modal-delete-data').modal('show')
    let vSelectedRow = $(this).parents('tr')
    let vSelectedData = gDataTable.row(vSelectedRow).data()
    gDataId = vSelectedData.id
  }

  // confirm delete data
  function onConfirmDeleteClick() {
    $.ajax({
      url: gAPI_URL + `construction-contractor/${gDataId}`,
      method: 'DELETE',
      headers: headers,
      success: () => {
       Swal.fire({
          position: 'top',
          icon: 'success',
          title: 'Xóa thành công',
          showConfirmButton: false,
          timer: 1500,
        })
        $('#modal-delete-data').modal('hide')
        getAllDataToTable()
      },
      // error: (err) => console.log(err.responseText),
    })
  }

  // get data input
  function getDataInput(paramObj) {
    paramObj.name = $('#input-name').val().trim()
    paramObj.address = $('#input-address').val().trim()
    paramObj.phone = $('#input-phone').val().trim()
    paramObj.phone2 = $('#input-phone2').val().trim()
    paramObj.fax = $('#input-fax').val().trim()
    paramObj.email = $('#input-email').val().trim()
    paramObj.description = $('#input-description').val().trim()
    paramObj.project = $('#input-project').val().trim()
    paramObj.website = $('#input-website').val().trim()
    paramObj.note = $('#input-note').val().trim()
  }
  // get data input update
  function getDataInputUpdate(paramObj) {
    paramObj.name = $('#input-name-update').val().trim()
    paramObj.address = $('#input-address-update').val().trim()
    paramObj.phone = $('#input-phone-update').val().trim()
    paramObj.phone2 = $('#input-phone2-update').val().trim()
    paramObj.fax = $('#input-fax-update').val().trim()
    paramObj.email = $('#input-email-update').val().trim()
    paramObj.description = $('#input-description-update').val().trim()
    paramObj.project = $('#input-project-update').val().trim()
    paramObj.website = $('#input-website-update').val().trim()
    paramObj.note = $('#input-note-update').val().trim()
  }

  // load data input update
  function loadDataToInputUpdate(paramObj) {
    $('#input-name-update').val(paramObj.name)
    $('#input-address-update').val(paramObj.address)
    $('#input-phone-update').val(paramObj.phone)
    $('#input-phone2-update').val(paramObj.phone2)
    $('#input-fax-update').val(paramObj.fax)
    $('#input-email-update').val(paramObj.email)
    $('#input-description-update').val(paramObj.description)
    $('#input-project-update').val(paramObj.project)
    $('#input-website-update').val(paramObj.website)
    $('#input-note-update').val(paramObj.note)
  }

  // reset form
  function resetDataForm() {
    $('#input-name').val('')
    $('#input-address').val('')
    $('#input-phone').val('')
    $('#input-phone2').val('')
    $('#input-fax').val('')
    $('#input-email').val('')
    $('#input-description').val('')
    $('#input-project').val('')
    $('#input-website').val('')
    $('#input-note').val('')
  }

  // validate data
  $('#create-form').validate({
    rules: {
      name: 'required',
      phone: { number: true },
      phone2: { number: true },
    },
    messages: {
      name: 'Vui lòng nhập tên nhà thầu',
      phone: { number: 'Vui lòng nhập số' },
      phone2: { number: 'Vui lòng nhập số' },
    },
  })
  // validate data update
  $('#update-form').validate({
    rules: {
      name: 'required',
      phone: { number: true },
      phone2: { number: true },
    },
    messages: {
      name: 'Vui lòng nhập tên nhà thầu',
      phone: { number: 'Vui lòng nhập số' },
      phone2: { number: 'Vui lòng nhập số' },
    },
  })

  $('#cancel-data').click(function (e) {
    e.preventDefault()
    resetDataForm()
  })

  $('#cancel-data-update').click(function (e) {
    e.preventDefault()
    $('#modal-update').modal('hide')
  })
})
